<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:30:"template/manage/gift/edit.html";i:1596466104;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
	<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">修改礼物</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">




          <div class="layui-form-item">
            <label class="layui-form-label">礼物名称</label>
            <div class="layui-input-block">
              <input type="text" name="title" id="title"   placeholder="礼物名称" class="layui-input" value="<?php echo $info['gift_title']; ?>">
            </div>
          </div>
          
          <div class="layui-form-item">
                <label class="layui-form-label">礼物图片</label>
                <div class="layui-input-block">
                  <input name="avatar"  class="layui-input" id="avatar" placeholder="礼物图片" value="<?php echo $info['gift_img']; ?>">
                </div>
             </div>
		  
          <div class="layui-form-item">
                <div class="layui-input-block">
                 <input type="file" multiple id="filelogo" name="filelogo"  onChange="upfile();" >
                </div>
             </div>
		  
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">礼物单价</label>
            <div class="layui-input-block">
              <input type="text" name="money" id="money"   placeholder="礼物单价" class="layui-input" value="<?php echo $info['gift_money']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">礼物备注</label>
            <div class="layui-input-block">
              <script id="editor" type="text/plain" style="width:100%;height:300px;"></script>
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">礼物库存</label>
            <div class="layui-input-block">
              <input type="text" name="count" id="count"   placeholder="礼物库存（默认99999999）" class="layui-input" value="<?php echo $info['gift_count']; ?>"> 
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">礼物单位</label>
            <div class="layui-input-block">
              <input type="text" name="unit" id="unit"   placeholder="礼物单位（默认个）" class="layui-input" value="<?php echo $info['gift_unit']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">礼物重量</label>
            <div class="layui-input-block">
              <input type="text" name="heft" id="heft"   placeholder="礼物重量（默认0.02）kg" class="layui-input" value="<?php echo $info['gift_heft']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">礼物排序</label>
            <div class="layui-input-block">
              <input type="text" name="sort" id="sort"   placeholder="平台排序,默认999" class="layui-input" value="<?php echo $info['gift_sort']; ?>">
            </div>
          </div>


          <div class="layui-form-item">
            <label class="layui-form-label">礼物状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" <?php if($info['gift_status']==1): ?>checked=""<?php endif; ?>>
              <input type="radio" name="status" value="2" title="禁用" <?php if($info['gift_status']==2): ?>checked=""<?php endif; ?>>
            </div>
          </div> 

		<div class="layui-form-item">
            <label class="layui-form-label">所属仓库</label>
            <div class="layui-input-block">
				<?php if(is_array($warehouse) || $warehouse instanceof \think\Collection || $warehouse instanceof \think\Paginator): $i = 0; $__LIST__ = $warehouse;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
              <input type="checkbox" name="w_id" value="<?php echo $vo['w_id']; ?>" title="<?php echo $vo['w_title']; ?>" <?php if(in_array(($vo['w_id']), is_array($info['w_id'])?$info['w_id']:explode(',',$info['w_id']))): ?>checked<?php endif; ?>>
             <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
          </div> 


          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>


var ue = UE.getEditor('editor');
$(document).ready(function () {
    ue.ready(function () {
        ue.setContent('<?php echo $info['gift_content']; ?>');
    });
});
function upfile(){
	//alert("上传中");
	var timestamp  = Date.parse(new Date());
	var upload_url = "<?php echo url('upload/upload'); ?>";
	var files      = document.getElementById("files");
	var formData = new FormData();
	formData.append("filename", document.getElementById("filelogo").files[0]);
	//var f = 1
	var file = document.getElementById("filelogo").files[0];
	if (file) {
		var fileSize = 0;
		if (file.size > 1024 * 1024){
			fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
		}else{
			fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
		}      

		//alert("上传中:"+fileSize);
		var xhr = new XMLHttpRequest();
	
		/* 事件监听 */
		xhr.upload.addEventListener("progress", uploadProgress, false);
		xhr.addEventListener("load", uploadComplete, false);
		xhr.addEventListener("error", uploadFailed, false);
		xhr.addEventListener("abort", uploadCanceled, false);
		/* 下面的url一定要改成你要发送文件的服务器url */
		xhr.open("POST",upload_url);
		xhr.send(formData);
		
		function uploadProgress(evt) {
			if (evt.lengthComputable) {
				//if(f==1){
		 		//	f=2;
					//uploadload();
		 		//}
				var percentComplete = Math.round(evt.loaded * 100 / evt.total);
				//document.getElementById('upload_text').innerHTML = percentComplete.toString() + '%';
			}else {
				//document.getElementById('upload_text').innerHTML = '无法计算';
			}
		}
	
		/*function uploadload(evt){
			files.innerHTML = files.innerHTML + "<li class=\"weui-uploader__file weui-uploader__file_status\" id=\"file_"+timestamp+"\"><div class=\"weui-uploader__file-content\" id=\"upload_text\">1</div></li>";
		}*/
	
		function uploadComplete(evt) {
			/* 当服务器响应后，这个事件就会被触发 */
			//alert(evt.target.responseText);
			//ErrorAlert(evt.target.responseText);
			
			var obj = JSON.parse(evt.target.responseText);
			if(obj.success=="ok"){
				$("#avatar").val(obj.fileurl);
				//$("#aa_img"+id).attr("src", obj.fileurl);
			}else{
				show_error(obj.msg);
			}
		}
	
		function uploadFailed(evt) {
			show_error("上传文件发生了错误尝试");
		}
	
		function uploadCanceled(evt) {
			show_error("上传被用户取消或者浏览器断开连接");
		}  
	}	

} 








$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var title        = $("#title").val();
	var money        = $("#money").val();
	var content        = UE.getEditor('editor').getContent();
	var count        = $("#count").val();
	var unit        = $("#unit").val();
	var heft        = $("#heft").val();
	var sort      = $("#sort").val();
	var status       = $("input[name='status']:checked").val();
	var w_id = "";
	var avatar        = $("#avatar").val();
	
	if(title == ""){
		show_error("礼物名称不能为空");
		return false
	}
	
	if(money == ""){
		show_error("礼物单价不能为空");
		return false
	}
	
	if(avatar == ""){
		show_error("礼物图片不能为空");
		return false
	}
	
	if(count == ""){
		count = 99999999;
	}
	
	if(unit == ""){
		unit = "个";
	}
	
	if(heft == ""){
		heft = "0.02";
	}
	
	if(sort == ""){
		sort = 999;
	}
	
	$.each($('input:checkbox'),function(){
		if(this.checked){
			w_id += $(this).val()+",";
		}
	});
	
	$.ajax({
		type:"POST",
		url:"<?php echo url('gift/edit'); ?>",
		dataType:"json",
		data:{
			id:<?php echo $info['gift_id']; ?>,
			sort:sort,
			avatar:avatar,
			money:money,
			content:content,
			title:title,
			status:status,
			w_id:w_id,
			count:count,
			unit:unit,
			heft:heft,
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('gift/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
