<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:32:"template/manage/model/index.html";i:1595850140;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
		  <div style="padding-bottom: 10px;">
          <?php if($ps['add']['power']): ?><button class="layui-btn layuiadmin-btn-useradmin" onclick="callurl('<?php echo url('model/add'); ?>')" data-type="add">添加</button><?php endif; ?>
        </div>
            <table class="layui-table" lay-even="" lay-skin="nob">

            <thead>
              <tr>
							<th>#序号</th>
							<th>排序</th>
							<th>模块名称</th>
							<th>模块缩写</th>
							<th>控制器/方法</th>
							<th class="text-center">显示</th>
							<!--<th>导航位置</th>-->
							<?php if($ps['del']['power'] or  $ps['edit']['power']): ?>
							<th>管理</th>
							<?php endif; ?>
						</tr>
            </thead>
            <tbody>
             <?php if(is_array($model) || $model instanceof \think\Collection || $model instanceof \think\Paginator): $i = 0; $__LIST__ = $model;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
						<tr id="tr_<?php echo $vo['mo_id']; ?>">
							<td><?php echo $vo['mo_id']; ?></td>
							<td><?php echo $vo['mo_sort']; ?></td>
							<td class="text-primary"><?php echo $vo['mo_title']; ?></td>
							<td><?php echo $vo['mo_shorttitle']; ?></td>
							<td><?php echo $vo['mo_controller']; ?>/<?php echo $vo['mo_action']; ?></td>
							<td class="text-center"><?php if($vo['mo_show']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
							<!--<td><?php if($vo['mo_navplace']==1): ?>顶部导航<?php endif; ?></td>-->
                            <?php if($ps['del']['power'] or  $ps['edit']['power']): ?>
							<td>
							
							 <div class="layui-btn-group">
								<?php if($ps['del']['power']): ?><button class="layui-btn layui-btn-sm" onclick="calldel('<?php echo url('model/del',array('id'=>$vo['mo_id'])); ?>','tr_<?php echo $vo['mo_id']; ?>')"><i class="layui-icon">&#xe640;</i></button><?php endif; if($ps['edit']['power']): ?><button class="layui-btn layui-btn-sm" onclick="callurl('<?php echo url('model/edit',array('id'=>$vo['mo_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button><?php endif; ?>
							  </div>
							</td>
                            <?php endif; ?>
						</tr>
							<?php if(is_array($vo['offline']) || $vo['offline'] instanceof \think\Collection || $vo['offline'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['offline'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?>
							<tr id="tr_<?php echo $vo1['mo_id']; ?>">
								<td><?php echo $vo1['mo_id']; ?></td>
								<td><?php echo $vo1['mo_sort']; ?></td>
								<td class="text-primary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $vo1['mo_title']; ?></td>
								<td><?php echo $vo1['mo_shorttitle']; ?></td>
								<td><?php echo $vo1['mo_controller']; ?>/<?php echo $vo1['mo_action']; ?></td>
								<td class="text-center"><?php if($vo1['mo_show']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
								<!--<td><?php if($vo['mo_navplace']==1): ?>顶部导航<?php endif; ?></td>-->
                                <?php if($ps['del']['power'] or  $ps['edit']['power']): ?>
								<td>
								<div class="layui-btn-group">
								<?php if($ps['del']['power']): ?><button class="layui-btn layui-btn-sm" onclick="calldel('<?php echo url('model/del',array('id'=>$vo1['mo_id'])); ?>','tr_<?php echo $vo1['mo_id']; ?>')"><i class="layui-icon">&#xe640;</i></button><?php endif; if($ps['edit']['power']): ?><button class="layui-btn layui-btn-sm" onclick="callurl('<?php echo url('model/edit',array('id'=>$vo1['mo_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button><?php endif; ?>
							  </div>
								</td>
                                <?php endif; ?>
							</tr>
							<?php endforeach; endif; else: echo "" ;endif; endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>