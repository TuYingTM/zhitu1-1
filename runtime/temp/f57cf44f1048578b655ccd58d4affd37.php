<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:31:"template/homes/goods/index.html";i:1596125762;s:46:"/www/wwwroot/zhitu1/template/homes/header.html";i:1620288014;s:54:"/www/wwwroot/zhitu1/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title>礼品中心-<?php echo $web_config['web_title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
    </head>

    <body>
				<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="../assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
                        <li><a href="http://lipin.maer.pro" target="_blank" >2.0演示</a></li>
                        <li><a href="http://demo.maer.pro" target="_blank">3.0演示</a></li>
                        <li><a href="http://zhitu1.maer.pro/manage.php" target="_blank">后台管理</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="content">
            <section class="addon-section">	
		<div class="container">
			<div class="text-line">
				<h5><span>精选商品</span></h5>
				<div class="subtitle">全国多仓发货</div>
			 </div>
				<div class="addon-filter">
					<ul class="nav nav-pills hidden-xs tab">
						<li role="presentation" class="active"><a href="<?php echo url("goods/index"); ?>">全部</a></li>
						<?php if(is_array($warehouse) || $warehouse instanceof \think\Collection || $warehouse instanceof \think\Paginator): $i = 0; $__LIST__ = $warehouse;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
							<li role="presentation"><a href="<?php echo url('goods/lists',array('wid'=>$vo['w_id'])); ?>" rel="nofollow"><?php echo $vo['w_title']; ?></a></li>
						<?php endforeach; endif; else: echo "" ;endif; ?>					 
					</ul>
				</div>
				<div class="addon-list">
					<div class="row">
						<?php if(is_array($gift) || $gift instanceof \think\Collection || $gift instanceof \think\Paginator): $i = 0; $__LIST__ = $gift;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
						<div class="col-lg-3 col-md-4 col-xs-6 grid-item">
							<div class="addon-item"><div class="tags tags-recommend"></div>
								<div class="addon-img">
									<a href="<?php echo url('goods/content',array('id'=>$vo['gift_id'])); ?>" title="<?php echo $vo['gift_title']; ?>">
										<img src="<?php echo $vo['gift_img']; ?>" alt="<?php echo $vo['gift_title']; ?>" class="img-responsive" width="300" height="200">
									</a>
								</div>
								<div class="addon-info">
									<div class="title">
										<a href="<?php echo url('goods/content',array('id'=>$vo['gift_id'])); ?>" target="_blank" title="<?php echo $vo['gift_title']; ?>"><?php echo $vo['gift_title']; ?></a>
									</div>
									<div class="metas clearfix"> 
										<span class="text-danger"><i class="fa fa-rmb"></i><?php echo $vo['gift_money']; ?></span>  
										<span class="addon-price"><a href="<?php echo url('order/buy',array('id'=>$vo['gift_id'])); ?>" class="btn btn-primary btn-sm active" role="button">立即购买</a></span> 
									</div>
								</div>
							</div>
						</div>
					 	<?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
				</div>
			</div>
		</section>	
        </main>
         
				<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
    </body>
</html>