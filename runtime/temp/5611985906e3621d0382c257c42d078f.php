<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:37:"template/homes/user/recharge_pay.html";i:1598536344;s:46:"/www/wwwroot/zhitu1/template/homes/header.html";i:1596423938;s:49:"/www/wwwroot/zhitu1/template/homes/user_left.html";i:1599139314;s:54:"/www/wwwroot/zhitu1/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title>会员中心-<?php echo $web_config['web_title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
		<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="/assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="content">
            <style>
    .panel-recharge h3 {
        margin-bottom: 15px;
        margin-top: 10px;
        color: #444;
        font-size: 16px;
    }

    .row-recharge > div {
        margin-bottom: 10px;
    }

    .row-recharge > div > label {
        width: 100%;
        height: 40px;
        display: block;
        font-size: 14px;
        line-height: 40px;
        color: #999;
        background: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        cursor: pointer;
        text-align: center;
        border: 1px solid #ddd;
        margin-bottom: 20px;
        font-weight: 400;
    }

    .row-recharge > div > label.active {
        border-color: #0d95e8;
        color: #0d95e8;
    }

    .row-recharge > div > label:hover {
        z-index: 4;
        border-color: #27b0d6;
        color: #27b0d6;
    }

    .panel-recharge .custommoney {
        border: none;
        height: 100%;
        width: 100%;
        display:inherit;
        line-height: 100%;
    }

    .row-recharge > div {
        height: 40px;
        line-height: 40px;
    }

    .row-recharge > div input.form-control {
        borer: none;
    }

    .row-paytype div input {
        display: none;
    }

    .row-paytype img {
        height: 22px;
        margin: 8px;
        vertical-align: inherit;
    }

    .btn-recharge {
        height: 40px;
        line-height: 40px;
        font-size: 14px;
        padding: 0;
    }

</style>
<div id="content-container" class="container">
    <div class="row">
        <div class="col-md-2">
	<div class="sidenav">
		<ul class="list-group">
			<li class="list-group-heading">单号管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/index'); ?>"><i class="fa fa-user-circle fa-fw"></i> 会员首页</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "buy"): ?>active<?php endif; ?>">
				<a href="<?php echo url('goods/index'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 购买礼品</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>">
				<a href="<?php echo url('order/index'); ?>"><i class="fa fa-list fa-fw"></i> 订单管理</a>
			</li>
			<!--<li class="list-group-item "> <a href="/express/ulist.html"><i class="fa fa-list fa-fw"></i>导入记录</a> </li>
			<li class="list-group-item "> <a href="/goods/favorite.html"><i class="fa fa-star fa-fw"></i>我的收藏</a> </li>-->
		</ul> 
		<ul class="list-group">
			<li class="list-group-heading">任务管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "add"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/add'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 任务下单</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="all"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'all')); ?>"><i class="fa fa-list fa-fw"></i> 任务中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="user"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'user')); ?>"><i class="fa fa-list fa-fw"></i> 我的任务</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "order"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/order'); ?>"><i class="fa fa-list fa-fw"></i> 我接的单</a>
			</li>
		</ul>
		<ul class="list-group">
			<li class="list-group-heading">用户中心</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "recharge" or \think\Request::instance()->action() == "recharge_bank"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/recharge'); ?>"><i class="fa fa-cny fa-fw"></i> 充值</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "rechargelist"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/rechargelist'); ?>"><i class="fa fa-rmb fa-fw"></i> 充值明细</a>
			</li>

			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd" and \think\Request::instance()->param('type')==1): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>1)); ?>"><i class="fa fa-rmb fa-fw"></i> 佣金提现</a>
			</li>
			
			<?php if($userinfo['u_sub']==2): ?>
			<!--<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd"and \think\Request::instance()->param('type')==2): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>2)); ?>"><i class="fa fa-rmb fa-fw"></i> 分站提现</a>
			</li>-->
			<?php endif; ?>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixian"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixian'); ?>"><i class="fa fa-rmb fa-fw"></i> 提现明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "score"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/score'); ?>"><i class="fa fa-rmb fa-fw"></i> 资金明细</a>
			</li>
			
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "credit"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/credit'); ?>"><i class="fa fa-rmb fa-fw"></i> 积分明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "union"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/union'); ?>"><i class="fa fa-share-alt fa-fw"></i> 推广中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "sendinfo"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/sendinfo'); ?>"><i class="fa fa-address-card fa-fw"></i> 发件信息</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "changepwd"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/changepwd'); ?>"><i class="fa fa-key fa-fw"></i> 修改密码</a> 
			</li>
			<!--<li class="list-group-item "> <a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出</a> </li>-->
		</ul>
    </div>
</div>
        <div class="col-md-9">
            <div class="panel panel-default panel-recharge">
                <div class="panel-body">
                    <h2 class="page-header">充值余额</h2>
						<button type="button" class="btn btn-default" onClick="url('<?php echo url('user/recharge'); ?>')"> 客服充值</button>
                        <?php if(!empty($paylist)): if(is_array($paylist) || $paylist instanceof \think\Collection || $paylist instanceof \think\Paginator): $i = 0; $__LIST__ = $paylist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        	<button type="button" class="btn <?php if($pay==$vo['pay_tag']): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" onClick="url('<?php echo url('user/recharge_pay',array('pay'=>$vo['pay_tag'])); ?>')"> <?php echo $vo['pay_title']; ?></button>
                            <?php endforeach; endif; else: echo "" ;endif; endif; if($config['s_bank_open']==1): ?>
						<button type="button" class="btn btn-default" onClick="url('<?php echo url('user/recharge_bank'); ?>')"> 银行卡充值</button>
                        <?php endif; ?>
					<div style="height:20px;"></div>
                    <div class="alert alert-info-light"><?php echo $config['s_qrcode_title']; ?></div>
                    <div class="clearfix" ></div>

                    <form id="sendinfo-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" >
                        <div class="form-group">
                            <label for="fren" class="control-label col-xs-12 col-sm-2">充值金额:</label>
                            <div class="col-xs-12 col-sm-4"><input type="text" class="form-control" id="money" name="money"  placeholder="充值金额" value=""/></div>
                        </div>
                        </form>
                        <div class="row row-recharge" style="margin:20px -15px 0 -15px;">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                                <input type="submit" class="btn btn-success btn-recharge btn-block sub" value="立即充值" />
                            </div>
                        </div>
                         
   
                   
                </div>
            </div>
        </div>
    </div>
</div>
        </main>
				<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
		
<script>
function url(url)
{
	window.location.href = url;
}


$(".sub").click(function(){
	var money = $("#money").val();

	if(money == ""){
		alert("ERROR：充值金额不能为空！");
		return false
	}
		
	if(money < 0){
		alert("ERROR：充值金额不能小于0！");
		return false
	}
	

	$.ajax({
		type:"POST",
		url:"<?php echo url('user/recharge_pay_ok'); ?>",
		dataType:"json",
		data:{
			money:money,
			paytag:"<?php echo $pay; ?>",
		},
		success:function(res){
			if(res.status == "success"){
				window.location.href = res.data;
			}else{
				alert(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});

</script>
    </body>
</html>