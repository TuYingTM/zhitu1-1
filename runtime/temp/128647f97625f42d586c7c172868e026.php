<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:38:"template/manage/manage/manageedit.html";i:1595743856;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1595724944;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">修改管理员</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">

          
          <div class="layui-form-item">
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-block">
              <input name="username" type="text" disabled class="layui-input" id="username" value="<?php echo $info['m_username']; ?>" readonly="readonly"   placeholder="用户名称">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">用户密码</label>
            <div class="layui-input-block">
              <input type="password" name="password" id="password"   placeholder="用户密码" class="layui-input">
            </div>
          </div>
		  
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-block">
              <input type="password" name="endpassword" id="endpassword"   placeholder="确认密码" class="layui-input">
            </div>
          </div>
		  
		 

          <div class="layui-form-item">
            <label class="layui-form-label">用户状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" <?php if($info['m_status']==1): ?>checked<?php endif; ?>>
              <input type="radio" name="status" value="2" title="禁用" <?php if($info['m_status']==2): ?>checked<?php endif; ?>>
            </div>
          </div>
		  
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">所属群组</label>
            <div class="layui-input-block">
				<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
              <input type="radio" name="group" value="<?php echo $vo['g_id']; ?>" title="<?php echo $vo['g_title']; ?>" <?php if($info['g_id']==$vo['g_id']): ?>checked<?php endif; ?>>
             <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
          </div> 

		  
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>
$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var password    = $("#password").val();
	var endpassword = $("#endpassword").val();
	var status      = $("input[name='status']:checked").val();
	var group       = $("input[name='group']:checked").val();
	
	if(password != "" || endpassword != ""){
		
		if(password == ""){
			show_toast("<?php echo $language['manage_user_empty_password']; ?>");
			return false
		}
		
		if(endpassword == ""){
			show_toast("<?php echo $language['manage_user_empty_endpassword']; ?>");
			return false
		}
		
		if(password != endpassword){
			show_toast("<?php echo $language['manage_user_error_password_and_endpassword']; ?>");
			return false
		}
	}
	
	if(typeof(group) == "undefined"){
		show_toast("<?php echo $language['manage_user_empty_group']; ?>");
		return false
	}

	
	
	$.ajax({
		type:"POST",
		url:"<?php echo url('manage/manageedit'); ?>",
		dataType:"json",
		data:{
			id:<?php echo $info['m_id']; ?>,
			password:password,
			status:status,
			group:group,
		},

		success:function(res){
			
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('manage/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
