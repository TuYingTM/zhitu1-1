<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:32:"template/homes/notice/lists.html";i:1596112558;s:46:"/www/wwwroot/zhitu1/template/homes/header.html";i:1596423938;s:54:"/www/wwwroot/zhitu1/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $notice_type['nt_title']; ?>-<?php echo $web_config['web_title']; ?></title>
        <!-- Bootstrap Core CSS -->
        <link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="//cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
            <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="page-top">
				<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="/assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="content">
            <div class="container">
              <div class="row">
              	<div class="col-lg-9">
				  <ol class="breadcrumb">
					<li><a href="/" aria-label="首页">首页</a></li>
					<li class="am-active"><?php echo $notice_type['nt_title']; ?></li>
				  </ol>
            	  <div class="panel-body">
					<ul class="list-unstyled">
						<?php if(is_array($noticelist) || $noticelist instanceof \think\Collection || $noticelist instanceof \think\Paginator): $i = 0; $__LIST__ = $noticelist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
						<li>
							<p class="clearfix">
								<a href="<?php echo url('notice/content',array('id'=>$vo['n_id'])); ?>" class="pull-left"><?php echo $vo['n_title']; ?></a>
                                <span class="text-muted pull-right"><?php echo date('Y-m-d',(strtotime($vo['n_addtime']))); ?></span>
							</p>
						</li>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
					<div class="widget-footer am-fr" style=" text-align: center;">
						<?php echo $page; ?>
					</div>
            	  </div> 
             	</div>
              	<div class="col-lg-3">
				  <div class="panel panel-default index-discussionlist">
                    <div class="panel-heading"><h3 class="panel-title"><i class="es-icon es-icon-whatshot pull-left"></i>热卖礼品</h3></div>
                    <div class="panel-body row">
						<?php if(is_array($gift) || $gift instanceof \think\Collection || $gift instanceof \think\Paginator): $i = 0; $__LIST__ = $gift;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
						<div class='floor-recommend'>
							<div class='floor-recommend-pic'>
							  <a href="<?php echo url('goods/content',array('id'=>$vo['gift_id'])); ?>">
								<img src="<?php echo $vo['gift_img']; ?>" width='65px' height='65px' alt="<?php echo $vo['gift_title']; ?>">
							  </a>
							</div>
							<div class='floor-recommend-desc'>
								<div class='floor-recommend-name'><a href="<?php echo url('goods/content',array('id'=>$vo['gift_id'])); ?>"><?php echo $vo['gift_title']; ?></a></div>
								<div class='floor-recommend-weight'>重量:<?php echo $vo['gift_heft']; ?></div>
								<div class='floor-recommend-price'>￥<?php echo $vo['gift_money']; ?></div>
							</div>
						</div>
						<?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                </div>  
                </div>
              </div>
			</div>
        </main>
        		<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
    </body>
</html>