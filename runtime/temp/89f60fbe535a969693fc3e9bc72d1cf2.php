<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:37:"template/manage/substation/index.html";i:1595840978;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
            <table class="layui-table" lay-even="" lay-skin="nob">
            <thead>
              <tr>
                <th>#序号</th>
                <th>状态</th>
				<th>所属用户</th>
				<th>站点名称</th>
				<th>站点域名</th>
				<th>充值比例</th>
				<th>快捷金额</th>
                <th>直推拥金</th>
				<th>二级拥金</th>
				<th>三级拥金</th>
                <th>添加时间</th>
				<th>管理</th>
              </tr> 
            </thead>
            <tbody>
             <?php if(is_array($substation) || $substation instanceof \think\Collection || $substation instanceof \think\Paginator): $i = 0; $__LIST__ = $substation;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
				<tr id="tr_<?php echo $vo['sub_id']; ?>">
					<td>#<?php echo $vo['sub_id']; ?></td>
					<td class="text-center"><?php if($vo['sub_status']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
					<td><?php echo $vo['u_phone']; ?></td>
					<td><?php echo $vo['sub_webtitle']; ?></td>
					<td><?php echo $vo['sub_domain']; ?></td>
					<td>1:<?php echo $vo['sub_credit']; ?></td>
					<td><?php echo $vo['sub_fastmoney']; ?></td>
					<td><?php echo $vo['sub_commission_one']; ?></td>
					<td><?php echo $vo['sub_commission_two']; ?></td>
					<td><?php echo $vo['sub_commission_three']; ?></td>
					<td><?php echo $vo['sub_addtime']; ?></td>
					<td>
							<div class="layui-btn-group">
                            <?php if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="calldel('<?php echo url('substation/del',array('id'=>$vo['sub_id'])); ?>','tr_<?php echo $vo['sub_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; if($ps['edit']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="callurl('<?php echo url('substation/edit',array('id'=>$vo['sub_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button>
							<?php endif; ?>
							</div>
					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>