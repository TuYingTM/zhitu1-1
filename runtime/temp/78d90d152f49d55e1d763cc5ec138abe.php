<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:29:"template/homes/order/buy.html";i:1599216396;s:46:"/www/wwwroot/zhitu1/template/homes/header.html";i:1620288014;s:49:"/www/wwwroot/zhitu1/template/homes/user_left.html";i:1599139314;s:54:"/www/wwwroot/zhitu1/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title>会员中心-<?php echo $web_config['web_title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
		<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>

    <body>
        		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="../assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
                        <li><a href="http://lipin.maer.pro" target="_blank" >2.0演示</a></li>
                        <li><a href="http://demo.maer.pro" target="_blank">3.0演示</a></li>
                        <li><a href="http://zhitu1.maer.pro/manage.php" target="_blank">后台管理</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="content">
            <style>
    .basicinfo {
        margin: 15px 0;
    }

    .basicinfo .row > .col-xs-4 {
        padding-right: 0;
    }

    .basicinfo .row > div {
        margin: 5px 0;
    }
</style>
<div id="content-container" class="container">
    <div class="row">
        <div class="col-md-2">
	<div class="sidenav">
		<ul class="list-group">
			<li class="list-group-heading">单号管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/index'); ?>"><i class="fa fa-user-circle fa-fw"></i> 会员首页</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "buy"): ?>active<?php endif; ?>">
				<a href="<?php echo url('goods/index'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 购买礼品</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>">
				<a href="<?php echo url('order/index'); ?>"><i class="fa fa-list fa-fw"></i> 订单管理</a>
			</li>
			<!--<li class="list-group-item "> <a href="/express/ulist.html"><i class="fa fa-list fa-fw"></i>导入记录</a> </li>
			<li class="list-group-item "> <a href="/goods/favorite.html"><i class="fa fa-star fa-fw"></i>我的收藏</a> </li>-->
		</ul> 
		<ul class="list-group">
			<li class="list-group-heading">任务管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "add"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/add'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 任务下单</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="all"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'all')); ?>"><i class="fa fa-list fa-fw"></i> 任务中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="user"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'user')); ?>"><i class="fa fa-list fa-fw"></i> 我的任务</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "order"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/order'); ?>"><i class="fa fa-list fa-fw"></i> 我接的单</a>
			</li>
		</ul>
		<ul class="list-group">
			<li class="list-group-heading">用户中心</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "recharge" or \think\Request::instance()->action() == "recharge_bank"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/recharge'); ?>"><i class="fa fa-cny fa-fw"></i> 充值</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "rechargelist"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/rechargelist'); ?>"><i class="fa fa-rmb fa-fw"></i> 充值明细</a>
			</li>

			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd" and \think\Request::instance()->param('type')==1): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>1)); ?>"><i class="fa fa-rmb fa-fw"></i> 佣金提现</a>
			</li>
			
			<?php if($userinfo['u_sub']==2): ?>
			<!--<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd"and \think\Request::instance()->param('type')==2): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>2)); ?>"><i class="fa fa-rmb fa-fw"></i> 分站提现</a>
			</li>-->
			<?php endif; ?>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixian"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixian'); ?>"><i class="fa fa-rmb fa-fw"></i> 提现明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "score"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/score'); ?>"><i class="fa fa-rmb fa-fw"></i> 资金明细</a>
			</li>
			
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "credit"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/credit'); ?>"><i class="fa fa-rmb fa-fw"></i> 积分明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "union"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/union'); ?>"><i class="fa fa-share-alt fa-fw"></i> 推广中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "sendinfo"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/sendinfo'); ?>"><i class="fa fa-address-card fa-fw"></i> 发件信息</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "changepwd"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/changepwd'); ?>"><i class="fa fa-key fa-fw"></i> 修改密码</a> 
			</li>
			<!--<li class="list-group-item "> <a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出</a> </li>-->
		</ul>
    </div>
</div>





<div class="col-md-10">

      <div class="panel panel-default ">
        <div class="panel-body">
          <div id="myTabContent" class="tab-content">
          <div class="tab-pane fade active in" id="one">
          <form name="form" role="form" id="adds-form" class="form-horizontal" method="POST" action="">
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2">商品信息:</label>
              <div class="col-xs-12 col-sm-8">
                <table class='table table-striped table-bordered'>
                  <thead>
                    <tr>
                      <th>商品名称</th>
                      <th>商品图片</th>
                      <th>商品单价</th>
                      <th>快递费用</th>
                      <th>商品总价</th>
                      <th>总价</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?php echo $gift['gift_title']; ?></td>
                      <td><img src="<?php echo $gift['gift_img']; ?>" width="45px" height="45px"></td>
                      <td><span class='price'><?php echo $gift['gift_money']; ?></span></td>
                      <td><span class='express_price' id="f_money"><?php echo $freight['f_money']; ?></span></td>
                      <td><span class="goods-price"><?php echo $gift['gift_money']; ?></span></td>
                      <td><span class='total-price' id="z_money"><?php echo $freight['f_money'] + $gift['gift_money']; ?></span></td>
                      
                    </tr>
                  </tbody>
                </table>
                <input type="hidden" name="gid" value="26">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2">发货仓:</label>
              <div class="col-xs-12 col-sm-8">
                <div class="radio">
					<?php if(is_array($war) || $war instanceof \think\Collection || $war instanceof \think\Paginator): $k = 0; $__LIST__ = $war;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?>
					<label>
						<input  name="store_id" type="radio" value="<?php echo $vo['w_id']; ?>" <?php if($k==1): ?>checked<?php endif; ?> onClick="select_fun(<?php echo $vo['w_id']; ?>,<?php echo $gift['gift_money']; ?>)"><?php echo $vo['w_title']; ?>                    &nbsp;
						<span>发货地址:<?php echo $vo['w_sender_province']; ?>/<?php echo $vo['w_sender_city']; ?>/<?php echo $vo['w_sender_district']; ?>/<?php echo $vo['w_address']; if(!empty($vo['w_content'])): ?>（<?php echo $vo['w_content']; ?>）<?php endif; ?></span>
					</label><br/>
					<?php endforeach; endif; else: echo "" ;endif; ?>
                   
				</div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2">使用平台:</label>
              <div class="col-xs-12 col-sm-8">
                <div class="radio" id="platid">
					<?php if(is_array($platform) || $platform instanceof \think\Collection || $platform instanceof \think\Paginator): $k = 0; $__LIST__ = $platform;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?>
					<label><input  name="plattype" type="radio" value="<?php echo $vo['pf_id']; ?>" <?php if($k==1): ?>checked<?php endif; ?>><?php echo $vo['pf_title']; if(!empty($vo['pf_content'])): ?>（<?php echo $vo['pf_content']; ?>）<?php endif; ?></label>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</div>
              </div>
            </div>
            
            
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2">下单方式:</label>
              <div class="col-xs-12 col-sm-8">
                <div class="radio" id="platid">
					<label><input  name="xdtype" type="radio" value="1" checked onClick="xd(1)">单个下单</label>
                    <label><input  name="xdtype" type="radio" value="2" onClick="xd(2)">批量导入</label>
				</div>
              </div>
            </div>

            
            <div class="form-group" style="display:none;" id="xdtype_3">
                <label class="control-label col-xs-12 col-sm-2">导入说明:</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="b">淘宝拼多多后台导出的表格直接上传导入</div>
                    <div class="b">其他平台<a href="/daoru.xls" target="_blank">点击这里下载</a>模板导入</div>
                    <div class="b" style="color: red">注意：礼品是真实发货，请核对好地址，下单后无法取消退款</div>
                </div>
              </div>
            

            
            
             <div class="form-group" id="xdtype_4">
              <label class="control-label col-xs-12 col-sm-2">地址说明:</label>
              <div class="col-xs-12 col-sm-8">
                <div class="b">格式：姓名，手机，地址</div>
                <div class="b">例如：张三，13812345678，浙江省 杭州市 下城区 文兴路888号，外部单号（此项可为空）</div>
				 <div class="b" style="color: red">注意：外部单号可为空</div>
                <div class="b" style="color: red">注意：礼品是真实发货，请核对好地址，下单后无法取消退款</div>
              </div>
            </div>
            
            <div class="form-group" style="display:none;" id="xdtype_1">
              <label class="control-label col-xs-12 col-sm-2">收货地址:</label>
              <div class="col-xs-12 col-sm-8">
					<!--<a href="javascript:;" class="btn btn-danger plupload" id="plupload-xls" data-mimetype="csv,xls,xlsx" data-input-id="c-avatar"><i class="fa fa-upload"></i> 点击上传表格</a>-->
                    <input type="file" multiple id="filelogo" name="filelogo"  onChange="upfile();" class="btn btn-danger plupload">
                     <input type="hidden" name="avatar" id="avatar"/>
                </div>
            </div>
            
            <div class="form-group" id="xdtype_2">
              <label class="control-label col-xs-12 col-sm-2">收货地址:</label>
              <div class="col-xs-12 col-sm-8">
                <textarea name="addstext" id="addstext" class="form-control" rows="5" cols="50" placeholder="一行一个地址" data-rule="required" ></textarea>
              </div>
            </div>

                        <div class="form-group layer-footer">
              <label class="control-label col-xs-12 col-sm-2"></label>
              <div class="col-xs-12 col-sm-4">
                <!--<button type="button" class="btn btn-danger btn-dialog">检查格式</button>-->
                <div type="submit" class="btn btn-success btn-embossed sub" id="submit" >提交下单</div>
              </div>
            </div>		  	
          </form>
          </div>
          </div>
          </div>
      </div>
    </div>
    </div>
</div>
        </main>
				<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
<script>
function upfile(){
	//alert("上传中");
	var timestamp  = Date.parse(new Date());
	var upload_url = "<?php echo url('upload/xls'); ?>";
	var files      = document.getElementById("files");
	var formData = new FormData();
	formData.append("filename", document.getElementById("filelogo").files[0]);
	//var f = 1
	var file = document.getElementById("filelogo").files[0];
	if (file) {
		var fileSize = 0;
		if (file.size > 1024 * 1024){
			fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
		}else{
			fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
		}      

		//alert("上传中:"+fileSize);
		var xhr = new XMLHttpRequest();
	
		/* 事件监听 */
		xhr.upload.addEventListener("progress", uploadProgress, false);
		xhr.addEventListener("load", uploadComplete, false);
		xhr.addEventListener("error", uploadFailed, false);
		xhr.addEventListener("abort", uploadCanceled, false);
		/* 下面的url一定要改成你要发送文件的服务器url */
		xhr.open("POST",upload_url);
		xhr.send(formData);
		
		function uploadProgress(evt) {
			if (evt.lengthComputable) {
				//if(f==1){
		 		//	f=2;
					//uploadload();
		 		//}
				var percentComplete = Math.round(evt.loaded * 100 / evt.total);
				//document.getElementById('upload_text').innerHTML = percentComplete.toString() + '%';
			}else {
				//document.getElementById('upload_text').innerHTML = '无法计算';
			}
		}
	
		/*function uploadload(evt){
			files.innerHTML = files.innerHTML + "<li class=\"weui-uploader__file weui-uploader__file_status\" id=\"file_"+timestamp+"\"><div class=\"weui-uploader__file-content\" id=\"upload_text\">1</div></li>";
		}*/
	
		function uploadComplete(evt) {
			/* 当服务器响应后，这个事件就会被触发 */
			//alert(evt.target.responseText);
			//ErrorAlert(evt.target.responseText);
			
			var obj = JSON.parse(evt.target.responseText);
			if(obj.success=="ok"){
				
				$("#avatar").val(obj.fileurl);
				alert("上传成功");
				//$("#aa_img"+id).attr("src", obj.fileurl);
			}else{
				alert(obj.msg);
			}
		}
	
		function uploadFailed(evt) {
			alert("上传文件发生了错误尝试");
		}
	
		function uploadCanceled(evt) {
			alert("上传被用户取消或者浏览器断开连接");
		}  
	}	

} 




function xd(id)
{
	if(id==1){
		$("#xdtype_1").hide();
		$("#xdtype_2").show();
		$("#xdtype_3").hide();
		$("#xdtype_4").show();
	}else{
		$("#xdtype_1").show();
		$("#xdtype_2").hide();
		$("#xdtype_3").show();
		$("#xdtype_4").hide();
	}
}
$(".sub").click(function(){

	if($("#submit").hasClass("sub")){
		
	}else{
		return false;
	}
	
	

	var store_id = $("input[name='store_id']:checked").val();
	var plattype = $("input[name='plattype']:checked").val();
	var addstext = $("#addstext").val();
	var xdtype = $("input[name='xdtype']:checked").val();
	var avatar = $("#avatar").val();
	
	
	
	if(xdtype==1){
		if(addstext ==""){
			alert("收货地址不能为空！ #1");
			return false;
		}
	}else{
		if(avatar ==""){
			alert("收货地址不能为空！ #2");
			return false;
		}
	}
	
	$("#submit").removeClass("sub");
	$('#submit').attr("disabled",true);
	
	$.ajax({
		type:"POST",
		url:"<?php echo url('order/addbuy'); ?>",
		dataType:"json",
		data:{
			giftid:<?php echo $gift['gift_id']; ?>,
			store_id:store_id,
			plattype:plattype,
			addstext:addstext,
			xdtype:xdtype,
			avatar:avatar,
		},
		success:function(res){
			$("#submit").addClass("sub");
			$('#submit').attr("disabled",false);
			if(res.status == "success"){
				alert("下单成功");
				window.location.href = "<?php echo url('order/index'); ?>";
			}else{
				alert(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});


function select_fun(id,money)
{

	$.ajax({
		type:"POST",
		url:"<?php echo url('order/change'); ?>",
		dataType:"json",
		data:{
			id:id,
		},
		success:function(res){
			var z_money = money + res.freight.f_money;
			$("#f_money").html(res.freight.f_money);
			$("#z_money").html(z_money.toFixed(1));
			$("#platid").html(" ");
			var len = res.platform.length;
			var html = "";
			for (var i = 0; i < len; i++){
				if(res.platform[i].pf_content==""){
					html = html + "<label><input  name='plattype' type='radio' value='"+res.platform[i].pf_id+"' checked>"+res.platform[i].pf_title+"</label> ";
				}else{
					html = html + "<label><input  name='plattype' type='radio' value='"+res.platform[i].pf_id+"' checked>"+res.platform[i].pf_title+"（"+res.platform[i].pf_content+"）</label> ";
				}
			}
			$("#platid").html(html);
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
}
</script>
    </body>
</html>