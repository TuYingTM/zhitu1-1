<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:31:"template/manage/users/edit.html";i:1596085148;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1595724944;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">修改群组</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">

          <div class="layui-form-item">
            <label class="layui-form-label">所属群组</label>
            <div class="layui-input-block">
              <select name="ug_id" id="ug_id">
				<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <option value="<?php echo $vo['ug_id']; ?>" <?php if($info['ug_id']==$vo['ug_id']): ?>selected="selected"<?php endif; ?>><?php echo $vo['ug_title']; ?></option>
				<?php endforeach; endif; else: echo "" ;endif; ?>
              </select>
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">用户手机号</label>
            <div class="layui-input-block">
              <input name="phone" type="text" disabled class="layui-input" id="phone"  value="<?php echo $info['u_phone']; ?>" readonly="readonly"   placeholder="用户手机号">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">密码</label>
            <div class="layui-input-block">
              <input type="password" name="password" id="password"   placeholder="密码" class="layui-input">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-block">
              <input type="password" name="endpassword" id="endpassword"   placeholder="确认密码" class="layui-input">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">积分</label>
            <div class="layui-input-block">
              <input type="test" name="credit" id="credit"   placeholder="积分" class="layui-input" value="<?php echo $info['u_credit']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">余额</label>
            <div class="layui-input-block">
              <input type="test" name="balance" id="balance"   placeholder="余额" class="layui-input" value="<?php echo $info['u_balance']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">冻结余额</label>
            <div class="layui-input-block">
              <input type="test" name="balance_freeze" id="balance_freeze"   placeholder="冻结余额" class="layui-input" value="<?php echo $info['u_balance_freeze']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">佣金余额</label>
            <div class="layui-input-block">
              <input type="test" name="commission" id="commission"   placeholder="佣金余额" class="layui-input" value="<?php echo $info['u_commission']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">冻结佣金余额</label>
            <div class="layui-input-block">
              <input type="test" name="commission_freeze" id="commission_freeze"   placeholder="冻结佣金余额" class="layui-input" value="<?php echo $info['u_commission_freeze']; ?>">
            </div>
          </div>
		  
		  <!--<div class="layui-form-item">
            <label class="layui-form-label">分站余额</label>
            <div class="layui-input-block">
              <input type="test" name="substation" id="substation"   placeholder="分站余额" class="layui-input" value="<?php echo $info['u_substation']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">冻结分站余额</label>
            <div class="layui-input-block">
              <input type="test" name="substation_freeze" id="substation_freeze"   placeholder="冻结分站余额" class="layui-input" value="<?php echo $info['u_substation_freeze']; ?>">
            </div>
          </div>-->
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发件人</label>
            <div class="layui-input-block">
              <input type="test" name="send_name" id="send_name"   placeholder="发件人" class="layui-input" value="<?php echo $info['u_send_name']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发件电话</label>
            <div class="layui-input-block">
              <input type="test" name="send_phone" id="send_phone"   placeholder="发件电话" class="layui-input" value="<?php echo $info['u_send_phone']; ?>">
            </div>
          </div>
		  
		  
		  <!--<div class="layui-form-item">
            <label class="layui-form-label">银行名称</label>
            <div class="layui-input-block">
              <input type="test" name="bank_title" id="bank_title"   placeholder="银行名称" class="layui-input" value="<?php echo $info['u_bank_title']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">开户行信息</label>
            <div class="layui-input-block">
              <input type="test" name="bank_content" id="bank_content"   placeholder="开户行信息" class="layui-input" value="<?php echo $info['u_bank_content']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">银行卡号</label>
            <div class="layui-input-block">
              <input type="test" name="bank_card" id="bank_card"   placeholder="银行卡号" class="layui-input" value="<?php echo $info['u_bank_card']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">开户姓名</label>
            <div class="layui-input-block">
              <input type="test" name="bank_name" id="bank_name"   placeholder="开户姓名" class="layui-input" value="<?php echo $info['u_bank_name']; ?>">
            </div>
          </div>-->
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">支付宝帐号</label>
            <div class="layui-input-block">
              <input type="test" name="zfb_title" id="zfb_title"   placeholder="支付宝帐号" class="layui-input" value="<?php echo $info['u_zfb_title']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">支付宝名称</label>
            <div class="layui-input-block">
              <input type="test" name="zfb_name" id="zfb_name"   placeholder="支付宝名称" class="layui-input" value="<?php echo $info['u_zfb_name']; ?>">
            </div>
          </div>
		  
		  

          <div class="layui-form-item">
            <label class="layui-form-label">用户状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" <?php if($info['u_status']==1): ?>checked=""<?php endif; ?>>
              <input type="radio" name="status" value="2" title="禁用" <?php if($info['u_status']==2): ?>checked=""<?php endif; ?>>
            </div>
          </div>       
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>
$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var phone        = $("#phone").val();
	var password      = $("#password").val();
	var endpassword = $("#endpassword").val();
	
	var credit = $("#credit").val();
	var balance = $("#balance").val();
	var commission = $("#commission").val();
	var substation = $("#substation").val();
	
	var balance_freeze = $("#balance_freeze").val();
	var commission_freeze = $("#commission_freeze").val();
	var substation_freeze = $("#substation_freeze").val();
	
	
	var ug_id = $("#ug_id").val();
	var send_name = $("#send_name").val();
	var send_phone = $("#send_phone").val();
	
	var bank_title = $("#bank_title").val();
	var bank_content = $("#bank_content").val();
	var bank_card = $("#bank_card").val();
	var bank_name = $("#bank_name").val();
	var zfb_title = $("#zfb_title").val();
	var zfb_name = $("#zfb_name").val();
	

	var status       = $("input[name='status']:checked").val();

	
	if(password != ""){
		if(endpassword == ""){
			show_error("确认密码不能为空");
			return false
		}
		
		if(endpassword != password){
			show_error("二次输入密码不一至，请重新输入！");
			return false
		}
	}
	

	$.ajax({
		type:"POST",
		url:"<?php echo url('users/edit'); ?>",
		dataType:"json",
		data:{
			id:<?php echo $info['u_id']; ?>,
			bank_title:bank_title,
			bank_content:bank_content,
			bank_card:bank_card,
			bank_name:bank_name,
			zfb_title:zfb_title,
			zfb_name:zfb_name,
			send_phone:send_phone,
			send_name:send_name,
			phone:phone,
			password:password,
			status:status,
			credit:credit,
			balance:balance,
			commission:commission,
			substation:substation,
			balance_freeze:balance_freeze,
			commission_freeze:commission_freeze,
			substation_freeze:substation_freeze,
			ug_id:ug_id,
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('users/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
