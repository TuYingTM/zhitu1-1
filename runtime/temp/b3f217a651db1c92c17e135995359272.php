<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:35:"template/manage/warehouse/edit.html";i:1599238302;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">修改仓库</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">

          
          <div class="layui-form-item">
            <label class="layui-form-label">仓库名称</label>
            <div class="layui-input-block">
              <input type="text" name="title" id="title"   placeholder="仓库名称" class="layui-input" value="<?php echo $info['w_title']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">仓库备注</label>
            <div class="layui-input-block">
              <input type="text" name="content" id="content"   placeholder="仓库备注" class="layui-input" value="<?php echo $info['w_content']; ?>">
            </div>
          </div>
		  
		  <!--<div class="layui-form-item">
            <label class="layui-form-label">仓库排序</label>
            <div class="layui-input-block">
              <input type="text" name="sort" id="sort"   placeholder="仓库排序,默认999" class="layui-input" value="<?php echo $info['w_sort']; ?>">
            </div>
          </div>-->
		  


          <div class="layui-form-item">
            <label class="layui-form-label">仓库状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" <?php if($info['w_status']==1): ?>checked=""<?php endif; ?>>
              <input type="radio" name="status" value="2" title="禁用" <?php if($info['w_status']==2): ?>checked=""<?php endif; ?>>
            </div>
          </div>  

		<div class="layui-form-item">
            <label class="layui-form-label">电商平台</label>
            <div class="layui-input-block">
				<?php if(is_array($platform) || $platform instanceof \think\Collection || $platform instanceof \think\Paginator): $i = 0; $__LIST__ = $platform;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
              <input type="checkbox" name="pf_id" value="<?php echo $vo['pf_id']; ?>" title="<?php echo $vo['pf_title']; ?>" <?php if(in_array(($vo['pf_id']), is_array($info['pf_id'])?$info['pf_id']:explode(',',$info['pf_id']))): ?>checked<?php endif; ?>>
             <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
          </div> 
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发货模式</label>
            <div class="layui-input-block">
              <input type="radio" name="w_fahuo" value="1" title="手工" lay-filter="levelM" <?php if($info['w_fahuo']==1): ?>checked=""<?php endif; ?>>
              <input type="radio" name="w_fahuo" value="2" title="快宝" lay-filter="levelM" <?php if($info['w_fahuo']==2): ?>checked=""<?php endif; ?>>
			  <input type="radio" name="w_fahuo" value="3" title="api" lay-filter="levelM" <?php if($info['w_fahuo']==3): ?>checked=""<?php endif; ?>>
            </div>
          </div> 
		
		  <div id="kuaidihelp" <?php if($info['w_fahuo']!=2): ?>style="display:none;"<?php endif; ?>>
		  <div class="layui-form-item">
            <label class="layui-form-label">快递品牌</label>
            <div class="layui-input-block">
              <select name="w_cp_code" id="w_cp_code">
				<option value="yt"   <?php if($info['w_cp_code']=='yt'): ?>selected="selected"<?php endif; ?>>圆通快递</option>
				<option value="sto"  <?php if($info['w_cp_code']=='sto'): ?>selected="selected"<?php endif; ?>>申通快递</option>
				<option value="zt"   <?php if($info['w_cp_code']=='zt'): ?>selected="selected"<?php endif; ?>>中通快递</option>
				<option value="yd"   <?php if($info['w_cp_code']=='yd'): ?>selected="selected"<?php endif; ?>>韵达快递</option>
				<option value="ht"   <?php if($info['w_cp_code']=='ht'): ?>selected="selected"<?php endif; ?>>百世汇通</option>
				<option value="tt"   <?php if($info['w_cp_code']=='tt'): ?>selected="selected"<?php endif; ?>>天天快递</option>
				<option value="ems"  <?php if($info['w_cp_code']=='ems'): ?>selected="selected"<?php endif; ?>>EMS标准快递</option>
				<option value="post2" <?php if($info['w_cp_code']=='post2'): ?>selected="selected"<?php endif; ?>>邮政小包</option>
				<option value="zjs"  <?php if($info['w_cp_code']=='zjs'): ?>selected="selected"<?php endif; ?>>宅急送</option>
				<option value="ys"   <?php if($info['w_cp_code']=='ys'): ?>selected="selected"<?php endif; ?>>优速快递</option>
				<option value="gt"   <?php if($info['w_cp_code']=='gt'): ?>selected="selected"<?php endif; ?>>国通快递</option>
				<option value="kj"   <?php if($info['w_cp_code']=='kj'): ?>selected="selected"<?php endif; ?>>快捷快递</option>
				<option value="dp"   <?php if($info['w_cp_code']=='dp'): ?>selected="selected"<?php endif; ?>>德邦快递</option>
				<option value="qf"   <?php if($info['w_cp_code']=='qf'): ?>selected="selected"<?php endif; ?>>全峰快递</option>	
				<option value="sf"   <?php if($info['w_cp_code']=='sf'): ?>selected="selected"<?php endif; ?>>顺丰快递</option>
				<option value="ems2" <?php if($info['w_cp_code']=='ems2'): ?>selected="selected"<?php endif; ?>>EMS经济快递</option>
				<option value="rfd"  <?php if($info['w_cp_code']=='rfd'): ?>selected="selected"<?php endif; ?>>如风达</option>
				<option value="post" <?php if($info['w_cp_code']=='post'): ?>selected="selected"<?php endif; ?>>邮政标准快递</option>
				<option value="best" <?php if($info['w_cp_code']=='best'): ?>selected="selected"<?php endif; ?>>百世快运</option>
				<option value="se"   <?php if($info['w_cp_code']=='se'): ?>selected="selected"<?php endif; ?>>速尔快递</option>
				<option value="ky"   <?php if($info['w_cp_code']=='ky'): ?>selected="selected"<?php endif; ?>>跨越速运</option>
				<option value="ane2" <?php if($info['w_cp_code']=='ane2'): ?>selected="selected"<?php endif; ?>>安能</option>
				<option value="ju"   <?php if($info['w_cp_code']=='ju'): ?>selected="selected"<?php endif; ?>>九曳</option>
				<option value="yc"   <?php if($info['w_cp_code']=='yc'): ?>selected="selected"<?php endif; ?>>远成快运</option>
				<option value="ztky" <?php if($info['w_cp_code']=='ztky'): ?>selected="selected"<?php endif; ?>>中通快运</option>
              </select>
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">快宝打印机密钥</label>
            <div class="layui-input-block">
              <input type="text" name="w_agent_id" id="w_agent_id"   placeholder="快宝打印机密钥" class="layui-input" value="<?php echo $info['w_agent_id']; ?>">
            </div>
          </div>
		  
		  </div>
		  
		  <div id="sendsapi" <?php if($info['w_fahuo']!=3): ?>style="display:none;"<?php endif; ?>>
			
            
            <div class="layui-form-item">
            <label class="layui-form-label">平台名称</label>
            <div class="layui-input-block">
              <input type="text" name="w_api_name" id="w_api_name"   placeholder="平台名称" class="layui-input" value="<?php echo $info['w_api_name']; ?>">
            </div>
          </div>
          
          
          <div class="layui-form-item">
            <label class="layui-form-label">APPID</label>
            <div class="layui-input-block">
              <input type="text" name="w_api_appid" id="w_api_appid"   placeholder="APPID" class="layui-input" value="<?php echo $info['w_api_appid']; ?>">
            </div>
          </div>
          
          <div class="layui-form-item">
            <label class="layui-form-label">APPUSER</label>
            <div class="layui-input-block">
              <input type="text" name="w_api_appuser" id="w_api_appuser"   placeholder="APPUSER" class="layui-input" value="<?php echo $info['w_api_appuser']; ?>">
            </div>
          </div>
          
          <div class="layui-form-item">
            <label class="layui-form-label">APPKEY</label>
            <div class="layui-input-block">
              <input type="text" name="w_api_appkey" id="w_api_appkey"   placeholder="APPKEY" class="layui-input" value="<?php echo $info['w_api_appkey']; ?>">
            </div>
          </div>
          

            
		  </div>
		  
		  
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发货人姓名</label>
            <div class="layui-input-block">
              <input type="text" name="w_sender_name" id="w_sender_name"   placeholder="发货人姓名" class="layui-input" value="<?php echo $info['w_sender_name']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发货人电话</label>
            <div class="layui-input-block">
              <input type="text" name="w_sender_phone" id="w_sender_phone"   placeholder="发货人电话" class="layui-input" value="<?php echo $info['w_sender_phone']; ?>">
            </div>
          </div>

		  <div class="layui-form-item">
            <label class="layui-form-label">发货省份</label>
            <div class="layui-input-block">
              <input type="text" name="w_sender_province" id="w_sender_province"   placeholder="发货省份" class="layui-input" value="<?php echo $info['w_sender_province']; ?>">
            </div>
          </div>
		  		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发货市</label>
            <div class="layui-input-block">
              <input type="text" name="w_sender_city" id="w_sender_city"   placeholder="发货市" class="layui-input" value="<?php echo $info['w_sender_city']; ?>">
            </div>
          </div>
		   
		  <div class="layui-form-item">
            <label class="layui-form-label">发货区县</label>
            <div class="layui-input-block">
              <input type="text" name="w_sender_district" id="w_sender_district"   placeholder="发货区县" class="layui-input" value="<?php echo $info['w_sender_district']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">发货地址</label>
            <div class="layui-input-block">
              <input type="text" name="address" id="address"   placeholder="发货地址" class="layui-input" value="<?php echo $info['w_address']; ?>">
            </div>
          </div>

		  
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>
    layui.use(['form'], function () {
    var upload = layui.upload;
    var form = layui.form;
    //此处即为 radio 的监听事件
    form.on('radio(levelM)', function(data){
    	if(data.value == 1){
			$("#kuaidihelp").hide();
			$("#sendsapi").hide();
		}else if(data.value == 2){
			$("#kuaidihelp").show();
			$("#sendsapi").hide();
		}else if(data.value == 3){
			$("#kuaidihelp").hide();
			$("#sendsapi").show();
		}
    });
 });



$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var title        = $("#title").val();
	var address        = $("#address").val();
	var content        = $("#content").val();
	var sort      = $("#sort").val();
	var status       = $("input[name='status']:checked").val();
	var pf_id = ""
	
	
	var w_fahuo             = $("input[name='w_fahuo']:checked").val();
	var w_cp_code           = $("#w_cp_code").val();
	var w_agent_id          = $("#w_agent_id").val();
	var w_sender_name       = $("#w_sender_name").val();
	var w_sender_phone      = $("#w_sender_phone").val();
	var w_sender_province   = $("#w_sender_province").val();
	var w_sender_city       = $("#w_sender_city").val();
	var w_sender_district   = $("#w_sender_district").val();
	
	var w_api_name   = $("#w_api_name").val();
	var w_api_appid   = $("#w_api_appid").val();
	var w_api_appuser   = $("#w_api_appuser").val();
	var w_api_appkey   = $("#w_api_appkey").val();

	

	if(title == ""){
		show_error("仓库名称不能为空");
		return false
	}
	
	if(w_fahuo == 2){
		if(w_cp_code == ""){
			show_error("快宝打印机密钥不能为空!");
			return false
		}
	}
	
	if(w_sender_name == ""){
		show_error("发货人姓名不能为空");
		return false
	}
	
	if(w_sender_phone == ""){
		show_error("发货人电话不能为空");
		return false
	}
	
	if(w_sender_province == ""){
		show_error("发货省份不能为空");
		return false
	}
	
	if(address == ""){
		show_error("发货市不能为空");
		return false
	}
	
	if(w_sender_city == ""){
		show_error("发货区县不能为空");
		return false
	}
	
	if(w_sender_district == ""){
		show_error("发货地址不能为空");
		return false
	}


	if(address == ""){
		show_error("发货地址不能为空");
		return false
	}
	
	if(sort == ""){
		sort = 999;
	}
	
	$.each($('input:checkbox'),function(){
		if(this.checked){
			pf_id += $(this).val()+",";
		}
	});
	$.ajax({
		type:"POST",
		url:"<?php echo url('warehouse/edit'); ?>",
		dataType:"json",
		data:{
			id:<?php echo $info['w_id']; ?>,
			sort:sort,
			address:address,
			content:content,
			title:title,
			status:status,
			pf_id:pf_id,
			
			w_fahuo:w_fahuo,
			w_cp_code:w_cp_code,
			w_agent_id:w_agent_id,
			w_sender_name:w_sender_name,
			w_sender_phone:w_sender_phone,
			w_sender_province:w_sender_province,
			w_sender_city:w_sender_city,
			w_sender_district:w_sender_district,
			w_api_name:w_api_name,
			w_api_appid:w_api_appid,
			w_api_appuser:w_api_appuser,
			w_api_appkey:w_api_appkey,
			
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('warehouse/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
