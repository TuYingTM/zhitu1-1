<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:31:"template/homes/order/index.html";i:1599216170;s:46:"/www/wwwroot/zhitu1/template/homes/header.html";i:1620288014;s:49:"/www/wwwroot/zhitu1/template/homes/user_left.html";i:1599139314;s:54:"/www/wwwroot/zhitu1/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title>会员中心-<?php echo $web_config['web_title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
		<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>

    <body>
        		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="../assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
                        <li><a href="http://lipin.maer.pro" target="_blank" >2.0演示</a></li>
                        <li><a href="http://demo.maer.pro" target="_blank">3.0演示</a></li>
                        <li><a href="http://zhitu1.maer.pro/manage.php" target="_blank">后台管理</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="content">
            <style>
    .basicinfo {
        margin: 15px 0;
    }

    .basicinfo .row > .col-xs-4 {
        padding-right: 0;
    }

    .basicinfo .row > div {
        margin: 5px 0;
    }
</style>


<div id="content-container" class="container">
    <div class="row">
        <div class="col-md-2">
	<div class="sidenav">
		<ul class="list-group">
			<li class="list-group-heading">单号管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/index'); ?>"><i class="fa fa-user-circle fa-fw"></i> 会员首页</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "buy"): ?>active<?php endif; ?>">
				<a href="<?php echo url('goods/index'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 购买礼品</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>">
				<a href="<?php echo url('order/index'); ?>"><i class="fa fa-list fa-fw"></i> 订单管理</a>
			</li>
			<!--<li class="list-group-item "> <a href="/express/ulist.html"><i class="fa fa-list fa-fw"></i>导入记录</a> </li>
			<li class="list-group-item "> <a href="/goods/favorite.html"><i class="fa fa-star fa-fw"></i>我的收藏</a> </li>-->
		</ul> 
		<ul class="list-group">
			<li class="list-group-heading">任务管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "add"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/add'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 任务下单</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="all"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'all')); ?>"><i class="fa fa-list fa-fw"></i> 任务中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="user"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'user')); ?>"><i class="fa fa-list fa-fw"></i> 我的任务</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "order"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/order'); ?>"><i class="fa fa-list fa-fw"></i> 我接的单</a>
			</li>
		</ul>
		<ul class="list-group">
			<li class="list-group-heading">用户中心</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "recharge" or \think\Request::instance()->action() == "recharge_bank"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/recharge'); ?>"><i class="fa fa-cny fa-fw"></i> 充值</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "rechargelist"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/rechargelist'); ?>"><i class="fa fa-rmb fa-fw"></i> 充值明细</a>
			</li>

			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd" and \think\Request::instance()->param('type')==1): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>1)); ?>"><i class="fa fa-rmb fa-fw"></i> 佣金提现</a>
			</li>
			
			<?php if($userinfo['u_sub']==2): ?>
			<!--<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd"and \think\Request::instance()->param('type')==2): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>2)); ?>"><i class="fa fa-rmb fa-fw"></i> 分站提现</a>
			</li>-->
			<?php endif; ?>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixian"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixian'); ?>"><i class="fa fa-rmb fa-fw"></i> 提现明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "score"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/score'); ?>"><i class="fa fa-rmb fa-fw"></i> 资金明细</a>
			</li>
			
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "credit"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/credit'); ?>"><i class="fa fa-rmb fa-fw"></i> 积分明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "union"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/union'); ?>"><i class="fa fa-share-alt fa-fw"></i> 推广中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "sendinfo"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/sendinfo'); ?>"><i class="fa fa-address-card fa-fw"></i> 发件信息</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "changepwd"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/changepwd'); ?>"><i class="fa fa-key fa-fw"></i> 修改密码</a> 
			</li>
			<!--<li class="list-group-item "> <a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出</a> </li>-->
		</ul>
    </div>
</div>
        <div class="col-md-10">
            <div class="panel panel-default ">
                <div class="panel-body">
					<h2 class="page-header">订单管理</h2>

					<script type="text/javascript" src="/template/datetime/moment.min.js"></script>
                    <script type="text/javascript" src="/template/datetime/daterangepicker.min.js"></script>
                    <link rel="stylesheet" type="text/css" href="/template/datetime/daterangepicker.css" />
					<div id="toolbar" class="toolbar" style="margin-bottom:10px;">
                    	<input id="demo"></input>
						<button class="btn btn-success btn-addtabs export" title="导出全部"><i class="fa fa-plus"></i> 导出订单</button>
					</div>					<script>
                    $(function () {
                        var locale = {
                            "format": 'YYYY-MM-DD',
                            "separator": " - ",
                            "applyLabel": "确定",
                            "cancelLabel": "取消",
                            "fromLabel": "起始时间",
                            "toLabel": "结束时间'",
                            "customRangeLabel": "自定义",
                            "weekLabel": "W",
                            "daysOfWeek": ["日", "一", "二", "三", "四", "五", "六"],
                            "monthNames": ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                            "firstDay": 1
                        };
                        $('#demo').daterangepicker({
                            'locale': locale,
                            ranges: {
                                '今日': [moment(), moment()],
                                '昨日': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                '最近7日': [moment().subtract(6, 'days'), moment()],
                                '最近30日': [moment().subtract(29, 'days'), moment()],
                                '本月': [moment().startOf('month'), moment().endOf('month')],
                                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                                    .endOf('month')
                                ]
                            },
                            "alwaysShowCalendars": true,
                            "startDate": new Date(),
                            "endDate": new Date(),
                            "opens": "right",
                        }, function (start, end, label) {
                            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
                        });
                    })
                </script>
                <style>
                    input {
                        height: 28px;
                        line-height: 28px;
                        text-align: center;
                    }
                    .btn-default {
                        background-color: #fff;
                        border: 1px solid #828080;
                        border-radius: 3px;
                        cursor: pointer;
                    }
                    .btn-primary {
                        background-color: #08c;
                        border: 1px solid #08c;
                        border-radius: 3px;
                        color: #fff;
                        cursor: pointer;
                    }
                    .btn-primary:hover{
                        background-color: #357ebd;
                    }
                </style>

				   <div class="form-group">
						
                            <table class="table table-bordered">

                                <tbody>
									<?php if(is_array($order) || $order instanceof \think\Collection || $order instanceof \think\Paginator): $i = 0; $__LIST__ = $order;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                    <tr>
                                        <th colspan="5">编号：<?php echo $vo['o_sncode']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(!empty($vo['o_out_sncode'])): ?>外部编号：<?php echo $vo['o_out_sncode']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php endif; ?>支付时间：<?php echo $vo['o_addtime']; ?></th>
                                    </tr>
									<tr style="<?php if($vo['o_status']==1): ?>background:#F3F3F3;<?php else: ?>background:#E1FBED;<?php endif; ?>">
										<td><?php echo $vo['pf_title']; ?></td>
                                        <td><?php echo $vo['w_title']; ?></td>
                                        <td>
                                        	<p><?php echo $vo['gift_title']; ?></p>
                                            <p>商品总价：<?php echo $vo['o_gift_money']; ?></p>
                                         </td>
										<td>
                                        
                                        <p>快递总价：<?php echo $vo['o_kd_money']; ?></p>
                                        <p>任务总价：<?php echo $vo['o_zmoney']; ?></p>
                                        </td>
										<td>
                                        <?php if($vo['o_status']==1): ?>出单中
                                        <?php else: ?>
                                        <p style=" color:#090">已发出<p>
                                        <p>单号：<?php echo $vo['o_kddh_sncode']; ?></p>
                                        <?php endif; ?></td>
									</tr>
									<tr>
										<td colspan="5">收货人：<?php echo $vo['o_sho_name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收货电话：<?php echo $vo['o_sho_phone']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收货地址：<?php echo $vo['o_sho_address']; ?></td>
										<!--<td><?php echo $vo['o_sho_address']; ?></td>-->
									</tr>
									<?php endforeach; endif; else: echo "" ;endif; ?>
                                </tbody>
                            </table>
 <div class="fixed-table-pagination"><?php echo $page; ?></div>
                  </div>
				   
				   
				   
					
                </div>
            </div>
        </div>
    </div>
</div>
        </main>
				<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
<script>

$(".export").click(function(){
	var datetime = $("#demo").val();
	if(datetime == ""){
		alert("请先选择导出的时间！");
		return false;
	}
	
	url = "<?php echo url('order/export',array('datetime'=>'AAAAA')); ?>";
	url = url.replace('AAAAA', datetime);
	window.open(url); 

});
</script>
    </body>
</html>