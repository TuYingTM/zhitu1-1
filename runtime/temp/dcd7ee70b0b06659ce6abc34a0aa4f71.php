<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:33:"template/manage/platform/add.html";i:1596033850;s:57:"/wwwroot/67.taocen.com/template/manage/common_header.html";i:1595724944;s:54:"/wwwroot/67.taocen.com/template/manage/common_top.html";i:1595724012;s:57:"/wwwroot/67.taocen.com/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">添加平台</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">

          
          <div class="layui-form-item">
            <label class="layui-form-label">平台名称</label>
            <div class="layui-input-block">
              <input type="text" name="title" id="title"   placeholder="平台名称" class="layui-input">
            </div>
          </div>
		  
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">平台备注</label>
            <div class="layui-input-block">
              <input type="text" name="content" id="content"   placeholder="平台备注" class="layui-input">
            </div>
          </div>
		  
		  <!--<div class="layui-form-item">
            <label class="layui-form-label">平台排序</label>
            <div class="layui-input-block">
              <input type="text" name="sort" id="sort"   placeholder="平台排序,默认999" class="layui-input">
            </div>
          </div>-->


          <div class="layui-form-item">
            <label class="layui-form-label">平台状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" checked="">
              <input type="radio" name="status" value="2" title="禁用">
            </div>
          </div>       
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>
$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var title        = $("#title").val();
	var content        = $("#content").val();
	var sort      = $("#sort").val();
	var status       = $("input[name='status']:checked").val();

	if(title == ""){
		show_error("平台名称不能为空");
		return false
	}
	
	if(sort == ""){
		sort = 999;
	}
	
	$.ajax({
		type:"POST",
		url:"<?php echo url('platform/add'); ?>",
		dataType:"json",
		data:{
			sort:sort,
			content:content,
			title:title,
			status:status,
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('platform/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
