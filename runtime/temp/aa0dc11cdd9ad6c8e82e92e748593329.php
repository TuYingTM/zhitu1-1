<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:31:"template/manage/model/edit.html";i:1595742738;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">修改模块</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">
          <div class="layui-form-item">
            <label class="layui-form-label">上级模块</label>
            <div class="layui-input-block">
              <select name="sid" id="sid">
                <option value="0">==顶级模块==</option>
				<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <option value="<?php echo $vo['mo_id']; ?>" <?php if($info['mo_sid']==$vo['mo_id']): ?>selected="selected"<?php endif; ?>><?php echo $vo['mo_title']; ?></option>
				<?php endforeach; endif; else: echo "" ;endif; ?>
              </select>
            </div>
          </div>
          
          <div class="layui-form-item">
            <label class="layui-form-label">模块名称</label>
            <div class="layui-input-block">
              <input type="text" name="title" id="title"   placeholder="模块名称" class="layui-input" value="<?php echo $info['mo_title']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">简写名称</label>
            <div class="layui-input-block">
              <input type="text" name="shorttitle" id="shorttitle"   placeholder="简写名称" class="layui-input" value="<?php echo $info['mo_shorttitle']; ?>">
            </div>
          </div>
		  
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">控制器</label>
            <div class="layui-input-block">
              <input type="text" name="controller" id="controller"   placeholder="控制器" class="layui-input" value="<?php echo $info['mo_controller']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">方法</label>
            <div class="layui-input-block">
              <input type="text" name="action" id="action"   placeholder="方法" class="layui-input" value="<?php echo $info['mo_action']; ?>">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">排序</label>
            <div class="layui-input-block">
              <input type="text" name="sort" id="sort"   placeholder="排序" class="layui-input" value="<?php echo $info['mo_sort']; ?>">
            </div>
          </div>
		  

          <div class="layui-form-item">
            <label class="layui-form-label">可用导航显示</label>
            <div class="layui-input-block">
              <input type="radio" name="show" value="0" title="不显示"  <?php if($info['mo_show']==0): ?>checked<?php endif; ?>>
              <input type="radio" name="show" value="1" title="显示"  <?php if($info['mo_show']==1): ?>checked<?php endif; ?>>
            </div>
          </div>       
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>
$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var sid        = $("#sid").val();
	var title      = $("#title").val();
	var shorttitle = $("#shorttitle").val();
	var controller = $("#controller").val();
	var action     = $("#action").val();
	var sort       = $("#sort").val();
	var show       = $("input[name='show']:checked").val();
	var navplace   = 1;


	if(title == ""){
		show_error("<?php echo $language['model_empty_title']; ?>");
		return false
	}
	
	if(shorttitle == ""){
		show_error("<?php echo $language['model_empty_shorttitle']; ?>");
		return false
	}
	
	if(controller == ""){
		show_error("<?php echo $language['model_empty_controller']; ?>");
		return false
	}
	
	if(action == ""){
		show_error("<?php echo $language['model_empty_action']; ?>");
		return false
	}
	
	if(sort == ""){
		sort = 999;
	}

	$.ajax({
		type:"POST",
		url:"<?php echo url('model/edit'); ?>",
		dataType:"json",
		data:{
			id:<?php echo $info['mo_id']; ?>,
			sid:sid,
			title:title,
			shorttitle:shorttitle,
			controller:controller,
			action:action,
			sort:sort,
			show:show,
			navplace:navplace,
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('model/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
