<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:32:"template/manage/index/login.html";i:1620287932;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>登入</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
  <link rel="stylesheet" href="/template/layuiadmin/style/login.css" media="all">
</head>
<body>

  <div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
      <div class="layadmin-user-login-box layadmin-user-login-header">
        <h2>礼物代发管理系统</h2>
        
      </div>
      <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
        <div class="layui-form-item">
          <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
          <input type="text" name="username" id="username" lay-verify="required" value="admin" placeholder="用户名" class="layui-input">
        </div>
        <div class="layui-form-item">
          <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
          <input type="password" name="password" id="password" lay-verify="required" value="123456" placeholder="密码" class="layui-input">
        </div>
        
        
        <div class="layui-form-item">
          <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit">登 入</button>
        </div>
        
      </div>
    </div>
    
    <!--<div class="layui-trans layadmin-user-login-footer">
      <p>© 2018 <a href="http://www.layui.com/" target="_blank">layui.com</a></p>
    </div>-->
    
    <!--<div class="ladmin-user-login-theme">
      <script type="text/html" template>
        <ul>
          <li data-theme=""><img src="{{ layui.setter.base }}style/res/bg-none.jpg"></li>
          <li data-theme="#03152A" style="background-color: #03152A;"></li>
          <li data-theme="#2E241B" style="background-color: #2E241B;"></li>
          <li data-theme="#50314F" style="background-color: #50314F;"></li>
          <li data-theme="#344058" style="background-color: #344058;"></li>
          <li data-theme="#20222A" style="background-color: #20222A;"></li>
        </ul>
      </script>
    </div>-->
    
  </div>

  <script src="/template/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'user'], function(){
    var $ = layui.$
    ,setter = layui.setter
    ,admin = layui.admin
    ,form = layui.form
    ,router = layui.router()
    ,search = router.search;
    form.render();

    //提交
    form.on('submit(LAY-user-login-submit)', function(obj){
		var username = $("#username").val();
		var password = $("#password").val();
		if(username == ""){
			alert("<?php echo $language['username_empty']; ?>");
			return false
		}
		
		if(password == ""){
			alert("<?php echo $language['password_empty']; ?>");
			return false
		}
		
		$.ajax({
			type:"POST",
			url:"<?php echo url('index/login'); ?>",
			dataType:"json",
			data:{
				username:username,
				password:password,
			},
			success:function(res){
				
				if(res.status == "success"){
					layer.msg(res.data, {offset: '15px',icon: 1,time: 1000}, function(){location.href = '<?php echo url('index/index'); ?>';});
				}else{
					layer.msg(res.data, {offset: '15px',icon: 2});
				}
				
			},
			error:function(jqXHR){
				console.log("Error: "+jqXHR.status);
			},
		});
      
    });    
  });
  </script>
</body>
</html>