<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:36:"template/manage/warehouse/index.html";i:1599220270;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
            <table class="layui-table" lay-even="" lay-skin="nob">

            <thead>
              <tr>
                <th>#序号</th>
				<th>发货模式</th>
                <th>仓库名称</th>
				<th>发货地址</th>
                <th>状态</th>
				<th>备注</th>
				<th>添加时间</th>
				<th>管理</th>
              </tr> 
            </thead>
            <tbody>
             <?php if(is_array($warehouse) || $warehouse instanceof \think\Collection || $warehouse instanceof \think\Paginator): $i = 0; $__LIST__ = $warehouse;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
				<tr id="tr_<?php echo $vo['w_id']; ?>">
					<td><?php echo $vo['w_id']; ?></td>
					<td><?php if($vo['w_fahuo']==1): ?>手工<?php elseif($vo['w_fahuo']==2): ?>快宝<?php else: ?>API<?php endif; ?></td>
					<td><?php echo $vo['w_title']; ?></td>
					<td><?php echo $vo['w_sender_province']; ?>/<?php echo $vo['w_sender_city']; ?>/<?php echo $vo['w_sender_district']; ?>/<?php echo $vo['w_address']; ?></td>
					<td class="text-center"><?php if($vo['w_status']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
					<td><?php echo $vo['w_content']; ?></td>
					<td><?php echo $vo['w_addtime']; ?></td>
					<td>
							<div class="layui-btn-group">
                            <?php if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="calldel('<?php echo url('warehouse/del',array('id'=>$vo['w_id'])); ?>','tr_<?php echo $vo['w_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; if($ps['edit']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="callurl('<?php echo url('warehouse/edit',array('id'=>$vo['w_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button>
							<?php endif; ?>
							</div>
					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>