<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:35:"template/manage/recharge/index.html";i:1598883562;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1595724944;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
		  <div class="layui-box layui-laypage layui-laypage-molv"><?php echo $page; ?></div>
            <table class="layui-table" lay-even="" lay-skin="nob">

            <thead>
              <tr>
                <th>#序号</th>
				<th>用户</th>
                <th>金额</th>
				<th>时间</th>
				<th>状态</th>
				<th>支付时间</th>
				<th>支付通道</th>
				<th>备注</th>
				<th>管理</th>
              </tr> 
            </thead>
            <tbody>
             <?php if(is_array($recharge) || $recharge instanceof \think\Collection || $recharge instanceof \think\Paginator): $i = 0; $__LIST__ = $recharge;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
				<tr id="tr_<?php echo $vo['r_id']; ?>">
					<td><?php echo $vo['r_id']; ?></td>
					<td><?php echo $vo['u_phone']; ?></td>
					<td><?php echo $vo['r_money']; ?></td>
					<td><?php echo $vo['r_addtime']; ?></td>
					<td><?php if($vo['r_status']==1): ?>审核<?php elseif($vo['r_status']==2): ?>通过<?php elseif($vo['r_status']==3): ?>拒绝<?php elseif($vo['r_status'] ==4): ?>待支付<?php endif; ?></td>
					<td><?php echo $vo['r_pay_addtime']; ?></td>
					<td><?php if($vo['pay_id']==0): ?>后台充值<?php elseif($vo['pay_id']==9999): ?>银行卡充值<?php else: ?><?php echo $vo['pay_title']; endif; ?></td>
					<td><?php echo $vo['r_content']; ?></td>
					<td>
							<div class="layui-btn-group">
                            <?php if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onClick="calldel('<?php echo url('recharge/del',array('id'=>$vo['r_id'])); ?>','tr_<?php echo $vo['r_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; if($ps['edit']['power']): if($vo['r_status']==1 or $vo['r_status']==4): ?>
								<button class="layui-btn layui-btn-sm" onClick="callurl('<?php echo url('recharge/edit',array('id'=>$vo['r_id'])); ?>')"><i class="layui-icon">审核</i></button>
								<?php endif; endif; ?>
							</div>
					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
		  <div class="layui-box layui-laypage layui-laypage-molv"><?php echo $page; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>