<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:33:"template/homes/goods/content.html";i:1596112516;s:49:"/wwwroot/67.taocen.com/template/homes/header.html";i:1596423938;s:57:"/wwwroot/67.taocen.com/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title><?php echo $info['gift_title']; ?>,价格低至<?php echo $info['gift_money']; ?>元-<?php echo $web_config['web_title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
    </head>

    <body>
        		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="/assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="content">
            <div id='content-container' class='container'>
    <ol class="breadcrumb"><li><a href="/" aria-label="首页">首页</a></li><li><a href="<?php echo url("goods/index"); ?>" >礼品中心</a></li><li class="active">详情</li></ol>
	<div class="row">
		<div class="col-md-9">
           <div class='list-desc' >
				<div class='left-img'>
					<div class='big-image'>
						<img src="<?php echo $info['gift_img']; ?>" alt="<?php echo $info['gift_title']; ?>">
					</div>
					<div class='small-img'>
						<img src="<?php echo $info['gift_img']; ?>"  class='small-list'>
					</div>
				</div>
				<div class='right-operate'>
					<h2><?php echo $info['gift_title']; ?></h2>
					<div class='sell-price'>
						<span>价格</span><i>￥<?php echo $info['gift_money']; ?></i>
					</div>
					<div class='at-once'><a href="<?php echo url('order/buy',array('id'=>$info['gift_id'])); ?>" class="btn btn-danger btn-lg">立即购买</a></div>

				</div>
		    </div>
            <div class="goods-detail">
				<div class='boundary'>
					<span class="page-header">商品详情</span>
				</div>
				<div class='textarea'><?php echo $info['gift_content']; ?></div>
		    </div>
        </div>
        <div class="col-md-3">
           <div class="panel panel-default index-discussionlist">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="es-icon es-icon-whatshot pull-left"></i>热卖推荐</h3>
                    </div>
                    <div class="panel-body row">
						<?php if(is_array($gift) || $gift instanceof \think\Collection || $gift instanceof \think\Paginator): $i = 0; $__LIST__ = $gift;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
						<div class='floor-recommend'>
							<div class='floor-recommend-pic'>
								<a href="<?php echo url('goods/content',array('id'=>$vo['gift_id'])); ?>">
									<img src="<?php echo $vo['gift_img']; ?>" width='65px' height='65px' alt="<?php echo $vo['gift_title']; ?>">
								</a>
							</div>
							<div class='floor-recommend-desc'>
								<div class='floor-recommend-name'><a href="<?php echo url('goods/content',array('id'=>$vo['gift_id'])); ?>"><?php echo $vo['gift_title']; ?></a></div>
								<div class='floor-recommend-weight'>重量:<?php echo $vo['gift_heft']; ?></div>
								<div class='floor-recommend-price'>￥<?php echo $vo['gift_money']; ?></div>
							</div>
						</div>	
						<?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                </div> 
        </div>
	</div>
</div>
        </main>
				<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
    </body>
</html>