<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:33:"template/manage/manage/index.html";i:1595850140;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1595724944;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
		  <div style="padding-bottom: 10px;">
          <?php if($ps['add']['power']): ?><button class="layui-btn layuiadmin-btn-useradmin" onclick="callurl('<?php echo url('manage/manageadd'); ?>')" data-type="add">添加</button><?php endif; ?>
        </div>
            <table class="layui-table" lay-even="" lay-skin="nob">

            <thead>
						<tr>
							<th class="text-center">序号</th>
							<th>用户名</th>
							<th>群组</th>
							<th class="text-center">状态</th>
							<th class="text-center">超管</th>
							<th>最近IP</th>
							<th>最近时间</th>
							<th>登录数</th>
							<th>系统</th>
							<th>浏览器</th>
							<?php if($ps['del']['power'] or  $ps['edit']['power']): ?>
							<th>管理</th>
							<?php endif; ?>
						</tr>
            </thead>
            <tbody>
						<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
						<tr id="tr_<?php echo $vo['m_id']; ?>">
							<td class="text-center"><?php echo $vo['m_id']; ?></td>
							<td><?php echo $vo['m_username']; ?></td>
							<td><?php echo $vo['g_title']; ?></td>
							<td class="text-center"><?php if($vo['m_status']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
							<td class="text-center"><?php if($vo['m_manage']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
							<td><?php echo $vo['m_this_ip']; ?></td>
							<td><?php echo $vo['m_this_time']; ?></td>
							<td><?php echo $vo['m_count']; ?></td>
							<td><?php echo $vo['m_this_os']; ?></td>
							<td><?php echo $vo['m_this_broswer']; ?></td>
                            <?php if($ps['del']['power'] or  $ps['edit']['power']): ?>
							<td>
							<div class="layui-btn-group">
							<?php if($vo['m_manage']!=1): if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="calldel('<?php echo url('manage/managedel',array('id'=>$vo['m_id'])); ?>','tr_<?php echo $vo['m_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; endif; if($ps['edit']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="callurl('<?php echo url('manage/manageedit',array('id'=>$vo['m_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button>
							<?php endif; ?>
							</div>
							</td>
                            <?php endif; ?>
						</tr>
						<?php endforeach; endif; else: echo "" ;endif; ?>

            </tbody>
          </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>