<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:30:"template/manage/users/add.html";i:1595811872;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1595724944;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">添加用户</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">

		  
		  <div class="layui-form-item">
            <label class="layui-form-label">用户手机号</label>
            <div class="layui-input-block">
              <input type="text" name="phone" id="phone"   placeholder="用户手机号" class="layui-input">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">密码</label>
            <div class="layui-input-block">
              <input type="password" name="password" id="password"   placeholder="密码" class="layui-input">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-block">
              <input type="password" name="endpassword" id="endpassword"   placeholder="确认密码" class="layui-input">
            </div>
          </div>
		  

          <div class="layui-form-item">
            <label class="layui-form-label">用户状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" checked="">
              <input type="radio" name="status" value="2" title="禁用">
            </div>
          </div>       
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>
$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	var phone        = $("#phone").val();
	var password      = $("#password").val();
	var endpassword = $("#endpassword").val();
	var status       = $("input[name='status']:checked").val();

	if(phone == ""){
		show_error("用户手机号不能为空");
		return false
	}

	if(phone.length < 11 || phone.length > 11){
		show_error("用户手机号格式错误，请重新输入！");
		return false
	}
	
	if(password == ""){
		show_error("密码不能为空");
		return false
	}
	
	if(endpassword == ""){
		show_error("确认密码不能为空");
		return false
	}
	
	if(endpassword != password){
		show_error("二次输入密码不一至，请重新输入！");
		return false
	}

	

	$.ajax({
		type:"POST",
		url:"<?php echo url('users/add'); ?>",
		dataType:"json",
		data:{
			phone:phone,
			password:password,
			status:status,
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('users/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
