<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:35:"template/manage/substation/add.html";i:1595832988;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1620285691;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>知途云仓管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">添加分站</div>
      <div class="layui-card-body" style="padding: 15px;">
        <form class="layui-form" action="" lay-filter="component-form-group">

          
          <div class="layui-form-item">
            <label class="layui-form-label">会员帐号</label>
            <div class="layui-input-block">
              <input type="text" name="phone" id="phone"   placeholder="所属会员帐号，请输入手机号" class="layui-input">
            </div>
          </div>

		  
		  <div class="layui-form-item">
            <label class="layui-form-label">站点名称</label>
            <div class="layui-input-block">
              <input type="text" name="title" id="title"   placeholder="站点名称" class="layui-input">
            </div>
          </div>
		  
		  <div class="layui-form-item">
            <label class="layui-form-label">站点域名</label>
            <div class="layui-input-block">
              <input type="text" name="domain" id="domain"   placeholder="站点域名" class="layui-input">
            </div>
          </div>
		  

          <div class="layui-form-item">
            <label class="layui-form-label">站点状态</label>
            <div class="layui-input-block">
              <input type="radio" name="status" value="1" title="启用" checked="">
              <input type="radio" name="status" value="2" title="禁用">
            </div>
          </div>       
          <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
              <div class="layui-footer" style="left: 0;">
                <div class="layui-btn sub">立即提交</div>
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
<script>

$(".sub").click(function(){
	//if(!$(".btn").hasClass("sub")){return false;}
	
	var phone        = $("#phone").val();
	var title        = $("#title").val();
	var domain      = $("#domain").val();
	var status       = $("input[name='status']:checked").val();
	
	if(phone == ""){
		show_error("会员帐号不能为空");
		return false
	}


	if(title == ""){
		show_error("站点名称不能为空");
		return false
	}
	
	$.ajax({
		type:"POST",
		url:"<?php echo url('substation/add'); ?>",
		dataType:"json",
		data:{
			phone:phone,
			title:title,
			status:status,
			domain:domain,
		},
		success:function(res){
			if(res.status == "success"){
				show_toast_callurl(res.data,"<?php echo url('substation/index'); ?>","success");
			}else{
				show_error(res.data);
			}
		},
		error:function(jqXHR){
			console.log("Error: "+jqXHR.status);
		},
	});
	
});
</script>
</body>
</html>
