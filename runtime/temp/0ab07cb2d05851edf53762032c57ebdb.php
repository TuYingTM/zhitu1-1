<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:30:"template/homes/user/index.html";i:1596112936;s:46:"/www/wwwroot/zhitu1/template/homes/header.html";i:1620288014;s:49:"/www/wwwroot/zhitu1/template/homes/user_left.html";i:1599139314;s:54:"/www/wwwroot/zhitu1/template/homes/footer_content.html";i:1595987484;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title>会员中心-<?php echo $web_config['web_title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<link href="/assets/css/frontend.min.css?v=1.0.1" rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
        <link href="/assets/css/user.css?v=1.0.1" rel="stylesheet">
    </head>

    <body>
        		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" style="padding:6px 15px;"><img src="../assets/img/logo.png" style="height:40px;" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/" >首页</a></li>
                        <li><a href="<?php echo url("goods/index"); ?>" >礼品代发商城</a></li>
                        <li><a href="<?php echo url('notice/lists',array('id'=>3)); ?>" >常见问题</a></li>
                        <li><a href="http://lipin.maer.pro" target="_blank" >2.0演示</a></li>
                        <li><a href="http://demo.maer.pro" target="_blank">3.0演示</a></li>
                        <li><a href="http://zhitu1.maer.pro/manage.php" target="_blank">后台管理</a></li>
						<?php if(empty(session('user_id_'.$web_config['sub_id']))): ?>
						<li><a href="<?php echo url("index/login"); ?>"><i class="fa fa-sign-in fa-fw"></i> 登 录</a></li>
						<li><a href="<?php echo url("index/reg"); ?>"><i class="fa fa-user-o fa-fw"></i> 注 册</a></li>
						<?php else: ?>
						<li><a href="<?php echo url('user/index'); ?>">您好：<?php echo session('user_phone_'.$web_config['sub_id']); ?></a></li>
						<li><a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i>退出</a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="content">
            <style>
    .basicinfo {
        margin: 15px 0;
    }

    .basicinfo .row > .col-xs-4 {
        padding-right: 0;
    }

    .basicinfo .row > div {
        margin: 5px 0;
    }
</style>
<div id="content-container" class="container">
    <div class="row">
        <div class="col-md-2">
	<div class="sidenav">
		<ul class="list-group">
			<li class="list-group-heading">单号管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/index'); ?>"><i class="fa fa-user-circle fa-fw"></i> 会员首页</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "buy"): ?>active<?php endif; ?>">
				<a href="<?php echo url('goods/index'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 购买礼品</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Order" and \think\Request::instance()->action() == "index"): ?>active<?php endif; ?>">
				<a href="<?php echo url('order/index'); ?>"><i class="fa fa-list fa-fw"></i> 订单管理</a>
			</li>
			<!--<li class="list-group-item "> <a href="/express/ulist.html"><i class="fa fa-list fa-fw"></i>导入记录</a> </li>
			<li class="list-group-item "> <a href="/goods/favorite.html"><i class="fa fa-star fa-fw"></i>我的收藏</a> </li>-->
		</ul> 
		<ul class="list-group">
			<li class="list-group-heading">任务管理</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "add"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/add'); ?>"><i class="fa fa-shopping-cart fa-fw"></i> 任务下单</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="all"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'all')); ?>"><i class="fa fa-list fa-fw"></i> 任务中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "index" and \think\Request::instance()->param('type')=="user"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/index',array('type'=>'user')); ?>"><i class="fa fa-list fa-fw"></i> 我的任务</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "Task" and \think\Request::instance()->action() == "order"): ?>active<?php endif; ?>">
				<a href="<?php echo url('task/order'); ?>"><i class="fa fa-list fa-fw"></i> 我接的单</a>
			</li>
		</ul>
		<ul class="list-group">
			<li class="list-group-heading">用户中心</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "recharge" or \think\Request::instance()->action() == "recharge_bank"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/recharge'); ?>"><i class="fa fa-cny fa-fw"></i> 充值</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "rechargelist"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/rechargelist'); ?>"><i class="fa fa-rmb fa-fw"></i> 充值明细</a>
			</li>

			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd" and \think\Request::instance()->param('type')==1): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>1)); ?>"><i class="fa fa-rmb fa-fw"></i> 佣金提现</a>
			</li>
			
			<?php if($userinfo['u_sub']==2): ?>
			<!--<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixianadd"and \think\Request::instance()->param('type')==2): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixianadd',array('type'=>2)); ?>"><i class="fa fa-rmb fa-fw"></i> 分站提现</a>
			</li>-->
			<?php endif; ?>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "tixian"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/tixian'); ?>"><i class="fa fa-rmb fa-fw"></i> 提现明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "score"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/score'); ?>"><i class="fa fa-rmb fa-fw"></i> 资金明细</a>
			</li>
			
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "credit"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/credit'); ?>"><i class="fa fa-rmb fa-fw"></i> 积分明细</a>
			</li>
			
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "union"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/union'); ?>"><i class="fa fa-share-alt fa-fw"></i> 推广中心</a>
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "sendinfo"): ?>active<?php endif; ?>"> 
				<a href="<?php echo url('user/sendinfo'); ?>"><i class="fa fa-address-card fa-fw"></i> 发件信息</a> 
			</li>
			<li class="list-group-item <?php if(\think\Request::instance()->controller() == "User" and \think\Request::instance()->action() == "changepwd"): ?>active<?php endif; ?>">
				<a href="<?php echo url('user/changepwd'); ?>"><i class="fa fa-key fa-fw"></i> 修改密码</a> 
			</li>
			<!--<li class="list-group-item "> <a href="<?php echo url('index/outlogin'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出</a> </li>-->
		</ul>
    </div>
</div>
        <div class="col-md-10">
            <div class="panel panel-default ">
                <div class="panel-body">
                   <h2 class="page-header"><?php echo session('user_phone_'.$web_config['sub_id']); ?>的会员中心</h2>
                    <div class="row user-baseinfo">
                        <div class="col-md-9 col-sm-9 col-xs-10">
                            <!-- Content -->
                            <div class="ui-content">
                                <!-- Success -->
                                <div class="basicinfo">
                                    <div class="row">
                                        <div class="col-xs-4 col-md-2">余额</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['u_balance']; ?></div>
                                        <div class="col-xs-4 col-md-2">等级积分</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['u_credit']; ?></div>
                                    </div>
									<div class="row">
                                        <div class="col-xs-4 col-md-2">拥金余额</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['u_commission']; ?></div>
                                        <!--<div class="col-xs-4 col-md-2">分站余额</div>
                                        <div class="col-xs-8 col-md-4">
											<a href="/user/score.html" data-toggle="tooltip" class="viewscore"><?php echo $userinfo['u_substation']; ?></a>
                                        </div>-->
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-2">登录次数</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['u_count']; ?> 次</div>
                                        <div class="col-xs-4 col-md-2">等级</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['ug_title']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-2">本次时间</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['u_this_time']; ?></div>
                                        <div class="col-xs-4 col-md-2">上次登录</div>
                                        <div class="col-xs-8 col-md-4"><?php echo $userinfo['u_last_time']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">推广赚钱</div>
                                        <div class="col-md-10" style="color: red">	
										成功推广一个即可获得下线充值金额的<?php echo $web_config['s_commission_one']; ?>%提成，永久有效。。http://<?php echo $web_domain; ?><?php echo url("index/reg",array('code'=>$userinfo['u_promo'])); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="page-header">公告&帮助中心</h2>
                    <div class="row user-dashboard">
                      <div class="col-md-5">
					    <ul class="list-unstyled">
						<?php if(is_array($notice) || $notice instanceof \think\Collection || $notice instanceof \think\Paginator): $i = 0; $__LIST__ = $notice;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
							<li>
								<p class="clearfix" style="border-bottom: 1px solid #eeeeee;">
									<a href="<?php echo url('notice/content',array('id'=>$vo['n_id'])); ?>" class="pull-left" target="_blank"><?php echo $vo['n_title']; ?></a>
									<span class="text-muted pull-right"><?php echo date('Y-m-d',(strtotime($vo['n_addtime']))); ?></span>
								</p>
							</li>
					    <?php endforeach; endif; else: echo "" ;endif; ?>
						</ul>	
					  </div>
					  <div class="col-md-7">
                        <table class="table table-striped table-bordered table-hover">
                         <thead>
							<tr>
								<th>会员等级</th>
								<th>所需积分</th>
								<?php if(is_array($warehouse) || $warehouse instanceof \think\Collection || $warehouse instanceof \think\Paginator): $i = 0; $__LIST__ = $warehouse;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<th><?php echo $vo['w_title']; ?></th>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tr>
						</thead>
                         <tbody>
							<?php if(is_array($user_group) || $user_group instanceof \think\Collection || $user_group instanceof \think\Paginator): $i = 0; $__LIST__ = $user_group;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
							<tr>
								<td><?php echo $vo['ug_title']; ?></td>
								<td><?php echo $vo['ug_credit']; ?></td>
								<?php if(is_array($vo['group']) || $vo['group'] instanceof \think\Collection || $vo['group'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['group'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?>
									<td><?php echo $vo1['f_money']; ?></td>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
							</tr>
							<?php endforeach; endif; else: echo "" ;endif; ?>
							<tr><td colspan="9">等级积分等于您的累计充值金额(如：累计充值1元，等级积分为<?php echo $web_config['s_credit']; ?>)</td></tr>
                          </tbody>
                         </table>
                      </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
        </main>
				<footer class="footer" style="clear:both">
			<p class="copyright">&copy; 2020 <?php echo $web_config['web_title']; ?> All Rights Reserved.</p>
		</footer>
    </body>
</html>