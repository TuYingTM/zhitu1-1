<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:32:"template/manage/index/index.html";i:1596525296;s:57:"/wwwroot/67.taocen.com/template/manage/common_header.html";i:1595724944;s:54:"/wwwroot/67.taocen.com/template/manage/common_top.html";i:1595724012;s:57:"/wwwroot/67.taocen.com/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body class="layui-layout-body">
  <div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
      <div class="layui-header">
        <!-- 头部区域 -->
        <ul class="layui-nav layui-layout-left">
          <li class="layui-nav-item layadmin-flexible" lay-unselect>
            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
              <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
            </a>
          </li>
          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;" layadmin-event="refresh" title="刷新">
              <i class="layui-icon layui-icon-refresh-3"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords="> 
          </li>
        </ul>
        <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="fullscreen">
              <i class="layui-icon layui-icon-screen-full"></i>
            </a>
          </li>
          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;">
              <cite><?php echo session("m_username"); ?> [ <?php echo session("m_group"); ?> ]</cite>
            </a>
            <dl class="layui-nav-child">
				<dd><a lay-href="<?php echo url("user/logs"); ?>">登录日志</a></dd>
              <dd><a lay-href="<?php echo url("user/password"); ?>">修改密码</a></dd>
              <hr>
              <dd style="text-align: center; position:cursor;"><a class="outlogin" href="javascript:void(0)">退出</a></dd>
            </dl>
          </li>
		 
        </ul>
      </div>
      
      <!-- 侧边菜单 -->
      <div class="layui-side layui-side-menu">
        <div class="layui-side-scroll">
          <div class="layui-logo" lay-href="<?php echo url('index/home'); ?>">
            <span>Gift System</span>
          </div>
          
		  
          <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
            <li data-name="home" class="layui-nav-item layui-nav-itemed">
              <a href="javascript:;" lay-tips="主页" lay-direction="2">
                <i class="layui-icon layui-icon-home"></i>
                <cite>主页</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="console" class="layui-this">
                  <a lay-href="<?php echo url('index/home'); ?>">控制台</a>
                </dd>
              </dl>
            </li>
			<?php if(is_array($navplace_1) || $navplace_1 instanceof \think\Collection || $navplace_1 instanceof \think\Paginator): $i = 0; $__LIST__ = $navplace_1;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($vo['offline_status']==1): ?>
					<li data-name="<?php echo $vo['mo_action']; ?>" class="layui-nav-item">
					  <a href="javascript:;" lay-href="<?php echo url($vo['mo_controller']."/".$vo['mo_action']); ?>" lay-tips="<?php echo $vo['mo_shorttitle']; ?>" lay-direction="2">
						<i class="layui-icon layui-icon-auz"></i>
						<cite><?php echo $vo['mo_shorttitle']; ?></cite>
					  </a>
					</li>
				<?php else: ?>
					<li data-name="<?php echo $vo['mo_action']; ?>" class="layui-nav-item">
					  <a href="javascript:;" lay-tips="<?php echo $vo['mo_shorttitle']; ?>" lay-direction="2">
						<i class="layui-icon layui-icon-home"></i>
						<cite><?php echo $vo['mo_shorttitle']; ?></cite>
					  </a>
					  <dl class="layui-nav-child">
						<?php if(is_array($vo['offline']) || $vo['offline'] instanceof \think\Collection || $vo['offline'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['offline'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?>
						<dd data-name="<?php echo $vo1['mo_action']; ?>" >
						  <a lay-href="<?php echo url($vo1['mo_controller']."/".$vo1['mo_action']); ?>"><?php echo $vo1['mo_shorttitle']; ?></a>
						</dd>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					  </dl>
					</li>
				<?php endif; endforeach; endif; else: echo "" ;endif; ?>
		  
		  
          </ul>
        </div>
      </div>

      <!-- 页面标签 -->
      <div class="layadmin-pagetabs" id="LAY_app_tabs">
        <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-down">
          <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
            <li class="layui-nav-item" lay-unselect>
              <a href="javascript:;"></a>
              <dl class="layui-nav-child layui-anim-fadein">
                <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
              </dl>
            </li>
          </ul>
        </div>
        <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
          <ul class="layui-tab-title" id="LAY_app_tabsheader">
            <li lay-id="home/console.html" lay-attr="home/console.html" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
          </ul>
        </div>
      </div>
      
      
      <!-- 主体内容 -->
      <div class="layui-body" id="LAY_app_body">
        <div class="layadmin-tabsbody-item layui-show">
          <iframe src="home/console.html" frameborder="0" class="layadmin-iframe"></iframe>
        </div>
      </div>
      
      <!-- 辅助元素，一般用于移动设备下遮罩 -->
      <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
  </div>
	<script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script>
	<script>
	$(".outlogin").click(function(){
		$.ajax({
			type:"POST",
			url:"<?php echo url('index/outlogin'); ?>",
			dataType:"json",
			data:{},
			success:function(res){
				if(res.status == "success"){
					layer.msg(res.data, {offset: '15px',icon: 1,time: 1000}, function(){location.href = '<?php echo url('index/login'); ?>';});
				}else{
					layer.msg(res.data, {offset: '15px',icon: 2});
				}
			},
			error:function(jqXHR){
				console.log("Error: "+jqXHR.status);
			},
		});
		
	});
  </script>
</body>
</html>