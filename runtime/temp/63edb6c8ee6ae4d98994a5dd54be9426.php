<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:38:"template/manage/capitallogs/users.html";i:1596291776;s:54:"/www/wwwroot/zhitu1/template/manage/common_header.html";i:1595724944;s:51:"/www/wwwroot/zhitu1/template/manage/common_top.html";i:1595724012;s:54:"/www/wwwroot/zhitu1/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
		   <div class="layui-box layui-laypage layui-laypage-molv"><?php echo $page; ?></div>
            <table class="layui-table" lay-even="" lay-skin="nob">

            <thead>
              <tr>
                <th>#序号</th>
				<th>用户</th>
				<th>方向</th>
                <th>变动金额</th>
				<th>变动后余额</th>
				<th>备注</th>
				<th>时间</th>
				<th>类型</th>
                <th>所属</th>
				<th>管理</th>
              </tr> 
            </thead>
            <tbody>
             <?php if(is_array($capitallogs) || $capitallogs instanceof \think\Collection || $capitallogs instanceof \think\Paginator): $i = 0; $__LIST__ = $capitallogs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
				<tr id="tr_<?php echo $vo['cl_id']; ?>">
					<td><?php echo $vo['cl_id']; ?></td>
					<td><?php echo $vo['u_phone']; ?></td>
					<td><?php if($vo['cl_direction']==1): ?>增加<?php else: ?>减少<?php endif; ?></td>
					<td><?php echo $vo['cl_money']; ?></td>
					<td><?php echo $vo['cl_balance']; ?></td>
					<td><?php echo $vo['cl_content']; ?></td>
					<td><?php echo $vo['cl_addtime']; ?></td>
					<td>
					<?php if($vo['cl_type']==1): ?>充值
					<?php elseif($vo['cl_type']==2): ?>礼物订单
					<?php elseif($vo['cl_type']==3): ?>拥金
					<?php elseif($vo['cl_type']==4): ?>提现
					<?php elseif($vo['cl_type']==5): ?>提现失败退回
					<?php elseif($vo['cl_type']==6): ?>任务下单
					<?php elseif($vo['cl_type']==7): ?>任务退回
                    <?php elseif($vo['cl_type']==8): ?>用户完成任务
					<?php endif; ?>
					</td>
					<td>
					<?php if($vo['cl_attribution']==1): ?>余额
					<?php elseif($vo['cl_attribution']==2): ?>推广拥余额
					<?php elseif($vo['cl_attribution']==3): ?>分站赢利余额
					<?php endif; ?>
					</td>
					<td>
							<div class="layui-btn-group">
                            <?php if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onClick="calldel('<?php echo url('capitallogs/del',array('id'=>$vo['cl_id'])); ?>','tr_<?php echo $vo['cl_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; ?>
							</div>
					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
		   <div class="layui-box layui-laypage layui-laypage-molv"><?php echo $page; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>