<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:35:"template/manage/platform/index.html";i:1596033832;s:57:"/wwwroot/67.taocen.com/template/manage/common_header.html";i:1595724944;s:54:"/wwwroot/67.taocen.com/template/manage/common_top.html";i:1595724012;s:57:"/wwwroot/67.taocen.com/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
            <table class="layui-table" lay-even="" lay-skin="nob">

            <thead>
              <tr>
                <th>#序号</th>
				<!--<th>排序</th>-->
                <th>平台名称</th>
                <th>状态</th>
				<th>备注</th>
				<th>添加时间</th>
				<th>管理</th>
              </tr> 
            </thead>
            <tbody>
             <?php if(is_array($platform) || $platform instanceof \think\Collection || $platform instanceof \think\Paginator): $i = 0; $__LIST__ = $platform;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
				<tr id="tr_<?php echo $vo['pf_id']; ?>">
					<td><?php echo $vo['pf_id']; ?></td>
					<!--<td><?php echo $vo['pf_sort']; ?></td>-->
					<td><?php echo $vo['pf_title']; ?></td>
					<td class="text-center"><?php if($vo['pf_status']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
					<td><?php echo $vo['pf_content']; ?></td>
					<td><?php echo $vo['pf_addtime']; ?></td>
					<td>
							<div class="layui-btn-group">
                            <?php if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="calldel('<?php echo url('platform/del',array('id'=>$vo['pf_id'])); ?>','tr_<?php echo $vo['pf_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; if($ps['edit']['power']): ?>
								<button class="layui-btn layui-btn-sm" onclick="callurl('<?php echo url('platform/edit',array('id'=>$vo['pf_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button>
							<?php endif; ?>
							</div>
					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>