<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:32:"template/manage/users/index.html";i:1596158662;s:57:"/wwwroot/67.taocen.com/template/manage/common_header.html";i:1595724944;s:54:"/wwwroot/67.taocen.com/template/manage/common_top.html";i:1595724012;s:57:"/wwwroot/67.taocen.com/template/manage/common_footer.html";i:1595918830;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <title>礼物代发管理系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="/template/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/template/layuiadmin/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
			<div class="layui-box layui-laypage layui-laypage-molv"><?php echo $page; ?></div>
            <table class="layui-table" lay-even="" lay-skin="nob">
            <thead>
              <tr>
                <th>#序号</th>
				<th>状态</th>
                <!--<th>分站</th>
				<th>平台</th>-->
				<th>上级</th>
				<th>群组</th>
				<th>手机号</th>
				<th>积分</th>
				<th>余额</th>
				<th>佣金余额</th>
				<th>分站余额</th>
				<th>登录次数</th>
				<th>发件人</th>
				<th>发件电话</th>
				<th>上次登录时间</th>
                <th>注册时间</th>
                <th>推广码</th>
				<th>明细</th>
				<th>管理</th>
              </tr> 
            </thead>
            <tbody>
             <?php if(is_array($users) || $users instanceof \think\Collection || $users instanceof \think\Paginator): $i = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
				<tr id="tr_<?php echo $vo['u_id']; ?>">
					<td><?php echo $vo['u_id']; ?></td>
					<td class="text-center"><?php if($vo['u_status']==1): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
					<!--<td ><?php if($vo['u_sub']==2): ?><span class="layui-badge-dot layui-bg-green"></span><?php else: ?><span class="layui-badge-dot"></span><?php endif; ?></td>
					<td class="text-center"><?php if($vo['sub_id']==0): ?>总站<?php else: ?>分站用户<?php endif; ?></td>-->
					<td class="text-center"><?php if($vo['su_id']!=0): ?><?php echo $vo['su_phone']; endif; ?></td>
					<td><?php echo $vo['ug_title']; ?></td>
					<td><?php echo $vo['u_phone']; ?></td>
					<td><?php echo $vo['u_credit']; ?></td>
					<td><?php echo $vo['u_balance']; ?></td>
					<td><?php echo $vo['u_commission']; ?></td>
					<td><?php echo $vo['u_substation']; ?></td>
					<td><?php echo $vo['u_count']; ?></td>
					<td><?php echo $vo['u_send_name']; ?></td>
					<td><?php echo $vo['u_send_phone']; ?></td>
					<td><?php echo $vo['u_last_time']; ?></td>
					<td><?php echo $vo['u_addtime']; ?></td>
					<td><?php echo $vo['u_promo']; ?></td>
					<td><a href="<?php echo url('creditlogs/users',array('id'=>$vo['u_id'])); ?>">积分</a> | <a href="<?php echo url('capitallogs/users',array('id'=>$vo['u_id'])); ?>">资金</a> | <a href="<?php echo url('tixian/users',array('id'=>$vo['u_id'])); ?>">提现</a></td>
					<td>
							<div class="layui-btn-group">
                            <?php if($ps['del']['power']): ?>
								<button class="layui-btn layui-btn-sm" onClick="calldel('<?php echo url('users/del',array('id'=>$vo['u_id'])); ?>','tr_<?php echo $vo['u_id']; ?>')"><i class="layui-icon">&#xe640;</i></button>
                            <?php endif; if($ps['edit']['power']): ?>
								<button class="layui-btn layui-btn-sm" onClick="callurl('<?php echo url('users/edit',array('id'=>$vo['u_id'])); ?>')"><i class="layui-icon">&#xe642;</i></button>
							<?php endif; ?>
							</div>
					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
          </table> 
		  <div class="layui-box layui-laypage layui-laypage-molv"><?php echo $page; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/template/layuiadmin/layui/layui.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/template/showjs.js"></script>
<script>
  layui.config({
    base: '/template/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','form', 'laydate','set']);
</script> 
</body>
</html>