ALTER TABLE 'gift_warehouse' ADD 'w_api_name' varchar(36) DEFAULT NULL;
ALTER TABLE 'gift_warehouse' ADD 'w_api_appid' varchar(36) DEFAULT NULL;
ALTER TABLE 'gift_warehouse' ADD 'w_api_appuser' varchar(60) DEFAULT NULL;
ALTER TABLE 'gift_warehouse' ADD 'w_api_appkey' varchar(60) DEFAULT NULL;