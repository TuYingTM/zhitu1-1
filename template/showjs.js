function show_error(title)
{
	layer.msg(title, {offset: '15px',icon: 2});
}

function show_success(title)
{
	layer.msg(title, {offset: '15px',icon: 1});
}

function show_toast_callurl(title,url)
{
	layer.msg(title, {offset: '15px',icon: 1,time: 1000}, function(){location.href = url;});
}

/*
用于跳转
*/
function callurl(url)
{
	window.location = url;
}

function calldel(url,id)
{
	layer.confirm('您确定删除此数据吗!', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	  //layer.msg('的确很重要', {icon: 1});
        /*提交删除*/
        $.ajax({
			type:"get",
			url:url,
			dataType:"json",
			data:{},
			success:function(res){
				if(res.status == "success"){
					show_success(res.data,"success");
					$("#"+id).remove();
				}else{
					show_error(res.data);
				}
			},
			error:function(jqXHR){
				console.log("Error: "+jqXHR.status);
			},
        });
        /*提交删除*/
	}, function(){

	});
}