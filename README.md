# 知途云仓1.0 淘宝礼品一件代发php源码

## 该版本停止更新，请勿继续调试使用，请选用知途云仓2.0开源版，或选购知途云仓3.0商业版

知途云仓2.0 开源版地址：https://gitee.com/TuYingTM/zhitu-v2

知途云仓2.0演示 http://lipin.maer.pro

知途云仓3.0演示 http://demo.maer.pro

### 介绍
知途云仓是专业的礼品代发源码提供商，现已更新至3.0版本，旧版本将逐渐开源，礼品一件代发,礼品商城,代发货平台源码,代发平台开发,一件代发,礼品单代发,礼品代发网源码,礼品包,礼品代发,代发快递,代发快递平台开发,代发,代发补单,礼品网,thinkPHP礼品代发源码, 2020礼品代发系统源码

![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/122815_a0af869a_9075012.png "知途云仓-专业的礼品代发源码、一件代发源码、代发货平台,thinkPHP礼品代发源码开发二次开发提供")
### 软件架构
Thinkphp 5.0 + mysql


### 安装教程

1.导入数据库


2.修改config/database.php数据库信息


3.登入网址 http://域名/manage.php 


4.修改【系统管理】->【系统设置】->网站域名处修改成当前域名


简单方便，基础功能齐全。

### 所需要的接口请自行注册

码支付 https://codepay.fateqq.com/

短信接口平台 http://www.smschinese.cn/default.shtml

快宝云打印机下载地址 https://kbydy.cn/CloudPrint/intro



### 欢迎访问知途云仓
#### 源码演示


知途云仓官网 http://zhitu.tuyingteam.com

知途云仓1.0演示（本款） http://zhitu1.maer.pro

知途云仓2.0演示 http://lipin.maer.pro

知途云仓3.0演示 http://demo.maer.pro

### 演示站点账密请联系客服索取


### 欢迎骚扰客服定制

## 联系QQ：511895959

## 联系微信：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/161703_3ffffc8e_9075012.jpeg "微信图片_20210506161515.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/122835_8528e8a8_9075012.png "知途云仓-专业的礼品代发源码、一件代发源码、代发货平台,thinkPHP礼品代发源码开发二次开发提供.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0506/122505_80d03960_9075012.png "知途云仓-专业的礼品代发源码、一件代发源码、代发货平台,thinkPHP礼品代发源码开发二次开发提供.png")

# 知途云仓推荐服务器配置：

基础配置：CPU：1v 内存：2GB 网络带宽：2M

最优配置：CPU：4v 内存：8GB 网络带宽：8M

++性价比配置：CPU：2v 内存：4GB 网络带宽：5M++

**推荐使用阿里云ecs服务器**

**专享基础配置 85 元/年起**

阿里云推荐配置服务器列表：[点击这里查看](https://www.aliyun.com/1111/new?userCode=cqz9iye6)

[![阿里云 235/3 年](https://z3.ax1x.com/2021/07/06/RI8A54.jpg "title")](https://www.aliyun.com/1111/new?userCode=cqz9iye6)


推荐系统：Centos 8.0 （也可用 windows2018 服务器版）

推荐管理工具：宝塔（请注册账号，方便管理） [点击查看](https://www.bt.cn/?invite_code=MV91bWpjcWo=)

推荐运行环境：LNMP:Linux+Nginx1.8+Mysql5.6+PHP7.3



# 知途云仓所需三方接口

> **==++建议在条件允许的情况下全部实名认证确保业务稳定！！++==**

### 1.短信验证码
#### 接口名字：短信宝（0.075/条）

注册地址： 
[点击注册](http://www.smsbao.com/reg?r=11752)

用户手册：http://www.smsbao.com/openapi/

接口用途：发送注册短信

需要：账号密码

### 2.滑块验证码
#### 接口名称：vaptcha（免费）

注册地址：https://www.vaptcha.com/

用户手册：https://www.vaptcha.com/document/faq.html

需要：VID&KEY



接口用途：用户发送验证码前的人机验证

### 3.免签支付接口

#### 接口名称：支付 FM（0.5%手续费）

注册地址：http://zhifu.fm

用户手册：https://docs.nephalem.cn/read/zhifufm/merchant-info

接口用途：用户充值

需要：商户号&key

在线客服：https://wpa1.qq.com/qP5Ngij0?_type=wpa&qidian=true

### 4.快递相关

#### 接口名称：快宝开放平台（免费）

注册地址：https://open.kuaidihelp.com/home

用户手册：https://open.kuaidihelp.com/api

接口用途：绑定单号和自动打单

需要：用户ID&API key

客服电话：021-62153711转8055

客服微信：kuaibao135

技术支持微信：kuaibao11

