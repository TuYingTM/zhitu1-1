<?php
//读取系统配置信息
function GetConfig()
{
	return Db("systems")->where("1=1")->find();
}


/**
 * 生成不重复的随机数字
 * @param  int $start  需要生成的数字开始范围
 * @param  int $end    结束范围
 * @param  int $length 需要生成的随机数个数
 * @return number      生成的随机数
 */
function getRandNumber($start=0,$end=9,$length=8){
	//初始化变量为0
	$connt = 0;
	//建一个新数组
	$temp = array();
	while($connt < $length){
	//在一定范围内随机生成一个数放入数组中
	$temp[] = mt_rand($start, $end);
	//$data = array_unique($temp);
	//去除数组中的重复值用了“翻翻法”，就是用array_flip()把数组的key和value交换两次。这种做法比用 array_unique() 快得多。	
	$data = array_flip(array_flip($temp));
	//将数组的数量存入变量count中	
	$connt = count($data);
	}
	//为数组赋予新的键名
	shuffle($data);
	//数组转字符串
	$str=implode(",", $data);
	//替换掉逗号
	$number=str_replace(',', '', $str);
	return $number;
}


//去除域名前面的http和https格式
function domain_Http_Clear($domain)
{
	$domain = strtolower($domain);
	$domain = str_replace('http://','',$domain);
	$domain = str_replace('https://','',$domain);
	$domain = str_replace('/','',$domain);
	return $domain;
}

//输出JSON信息，用于数据输出
function eyEchoJson($data = array())
{
	echo json_encode($data);
	exit;
}

//输出JSON信息，用于提示信息输出
function eyJson($data,$status="error")
{
	$json['status'] = $status;
	$json['data'] = $data;
	echo json_encode($json);
	exit;
}

//信息提示，判断AJAX还是正常提示
function eyAlert($content,$status="error",$url="null")
{
	if(IS_AJAX){
		eyJson($content,$status);
	}else{
		
		if($url == "null"){
			eyError($content);
			exit;
		}else{
			eyUrl($url);
		}
	}
}

//用于处理跳转信息
function eyError($title,$url="nourl"){
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <title>跳转提示</title>
    <style type="text/css">
        *{ padding: 0; margin: 0; }
        body{ background: #fff; font-family: "Microsoft Yahei","Helvetica Neue",Helvetica,Arial,sans-serif; color: #333; font-size: 16px; }
        .system-message{ padding: 24px 48px; }
        .system-message h1{ font-size: 100px; font-weight: normal; line-height: 120px; margin-bottom: 12px; }
        .system-message .jump{ padding-top: 10px; }
        .system-message .jump a{ color: #333; }
        .system-message .success,.system-message .error{ line-height: 1.8em; font-size: 36px; }
        .system-message .detail{ font-size: 12px; line-height: 20px; margin-top: 12px; display: none; }
    </style>
</head>
<body>
    <div class="system-message">
                    <h1>:(</h1>
            <p class="error">'.$title.'</p>
                    <p class="detail"></p>
        <p class="jump">';
		if($url=="nourl"){
			echo '页面自动 <a id="href" href="javascript:history.back(-1);">跳转</a> 等待时间： <b id="wait">3</b>';
		}else{
			echo '页面自动 <a id="href" href="'.$url.'">跳转</a> 等待时间： <b id="wait">3</b>';
		}
		
      echo ' </p>
    </div>
    <script type="text/javascript">
        (function(){
            var wait = document.getElementById("wait"),
                href = document.getElementById("href").href;
            var interval = setInterval(function(){
                var time = --wait.innerHTML;
                if(time <= 0) {
                    location.href = href;
                    clearInterval(interval);
                };
            }, 1000);
        })();
    </script>
</body>
</html>';
}

//用于URL跳转
function eyUrl($urlfile)
{
	header('Location: '.$urlfile);
	exit;
}

//获取浏览器信息
function GetBroswer(){
     $sys = $_SERVER['HTTP_USER_AGENT'];  //获取用户代理字符串
     if (stripos($sys, "Firefox/") > 0) {
         preg_match("/Firefox\/([^;)]+)+/i", $sys, $b);
         $exp[0] = "Firefox";
         $exp[1] = $b[1];  //获取火狐浏览器的版本号
     } elseif (stripos($sys, "Maxthon") > 0) {
         preg_match("/Maxthon\/([\d\.]+)/", $sys, $aoyou);
         $exp[0] = "傲游";
         $exp[1] = $aoyou[1];
	} elseif (stripos($sys, "Safari") > 0) {
         preg_match("/Safari\/([\d\.]+)/", $sys, $aoyou);
         $exp[0] = "Safari";
         $exp[1] = $aoyou[1];
     } elseif (stripos($sys, "MSIE") > 0) {
         preg_match("/MSIE\s+([^;)]+)+/i", $sys, $ie);
         $exp[0] = "IE";
         $exp[1] = $ie[1];  //获取IE的版本号
     } elseif (stripos($sys, "OPR") > 0) {
             preg_match("/OPR\/([\d\.]+)/", $sys, $opera);
         $exp[0] = "Opera";
         $exp[1] = $opera[1];  
     } elseif(stripos($sys, "Edge") > 0) {
         //win10 Edge浏览器 添加了chrome内核标记 在判断Chrome之前匹配
         preg_match("/Edge\/([\d\.]+)/", $sys, $Edge);
         $exp[0] = "Edge";
         $exp[1] = $Edge[1];
     } elseif (stripos($sys, "Chrome") > 0) {
             preg_match("/Chrome\/([\d\.]+)/", $sys, $google);
         $exp[0] = "Chrome";
         $exp[1] = $google[1];  //获取google chrome的版本号
     } elseif(stripos($sys,'rv:')>0 && stripos($sys,'Gecko')>0){
         preg_match("/rv:([\d\.]+)/", $sys, $IE);
             $exp[0] = "IE";
         $exp[1] = $IE[1];
     }else {
        $exp[0] = "未知浏览器";
        $exp[1] = ""; 
     }
     return $exp[0].'('.$exp[1].')';
}

//获取操作系统信息
function GetOS(){
	$agent = $_SERVER['HTTP_USER_AGENT'];
    $os = false;
    if (preg_match('/win/i', $agent) && strpos($agent, '95')){
      $os = 'Windows 95';
    }else if (preg_match('/win 9x/i', $agent) && strpos($agent, '4.90')){
      $os = 'Windows ME';
    }else if (preg_match('/win/i', $agent) && preg_match('/98/i', $agent)){
      $os = 'Windows 98';
    }else if (preg_match('/win/i', $agent) && preg_match('/nt 6.0/i', $agent)){
      $os = 'Windows Vista';
    }else if (preg_match('/win/i', $agent) && preg_match('/nt 6.1/i', $agent)){
      $os = 'Windows 7';
    }else if (preg_match('/win/i', $agent) && preg_match('/nt 6.2/i', $agent)){
      $os = 'Windows 8';
    }else if(preg_match('/win/i', $agent) && preg_match('/nt 10.0/i', $agent)){
      $os = 'Windows 10';#添加win10判断
    }else if (preg_match('/win/i', $agent) && preg_match('/nt 5.1/i', $agent)){
      $os = 'Windows XP';
    }else if (preg_match('/win/i', $agent) && preg_match('/nt 5/i', $agent)){
      $os = 'Windows 2000';
    }else if (preg_match('/win/i', $agent) && preg_match('/nt/i', $agent)){
      $os = 'Windows NT';
    }else if (preg_match('/win/i', $agent) && preg_match('/32/i', $agent)){
      $os = 'Windows 32';
    }else if (preg_match('/linux/i', $agent)){
      $os = 'Linux';
    }else if (preg_match('/unix/i', $agent)){
      $os = 'Unix';
    }else if (preg_match('/sun/i', $agent) && preg_match('/os/i', $agent)){
      $os = 'SunOS';
    }else if (preg_match('/ibm/i', $agent) && preg_match('/os/i', $agent)){
      $os = 'IBM OS/2';
    }else if (preg_match('/Mac/i', $agent) && preg_match('/PC/i', $agent)){
      $os = 'Macintosh';
    }else if (preg_match('/PowerPC/i', $agent)){
      $os = 'PowerPC';
    }else if (preg_match('/AIX/i', $agent)){
      $os = 'AIX';
    }else if (preg_match('/HPUX/i', $agent)){
      $os = 'HPUX';
    }else if (preg_match('/NetBSD/i', $agent)){
      $os = 'NetBSD';
    }else if (preg_match('/BSD/i', $agent)){
      $os = 'BSD';
    }else if (preg_match('/OSF1/i', $agent)){
      $os = 'OSF1';
    }else if (preg_match('/IRIX/i', $agent)){
      $os = 'IRIX';
    }else if (preg_match('/FreeBSD/i', $agent)){
      $os = 'FreeBSD';
    }else if (preg_match('/teleport/i', $agent)){
      $os = 'teleport';
    }else if (preg_match('/flashget/i', $agent)){
      $os = 'flashget';
    }else if (preg_match('/webzip/i', $agent)){
      $os = 'webzip';
    }else if (preg_match('/offline/i', $agent)){
      $os = 'offline';
	}else if (preg_match('/iphone/i', $agent)){
      $os = 'iphone';
    }else{
      $os = '未知操作系统';
    }
    return $os;  
}



//短信接口
function GetSms($phone,$type=1){
	$rand = rand(1000,9999);
	$sms = GetConfig();
	if($type==1){
		$sms_content = "您注册的验证码为：".$rand."，验证码10分钟内有效！";
	}elseif($type==2){
		$sms_content = "您的修改密码验证码：".$rand."，验证码10分钟内有效！";
	}
	$url = 'http://utf8.api.smschinese.cn/?Uid='.$sms['s_smsusername'].'&Key='.$sms['s_smspassword'].'&smsMob='.$phone.'&smsText='.$sms_content;
	$ch = curl_init(); // curl_init()需要php_curl.dll扩展
	$timeout = 5;
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$file_contents = curl_exec($ch);
	curl_close($ch);
	if($file_contents>0){
		if($type != 4){
			$data2 = ([
				'sms_phone'   => $phone,
				'sms_code'    => $rand,
				'sms_addtime' => date("Y-m-d H:i:s"),
				'sms_type'    => $type
			]);
			Db("sms")->insertGetId($data2);
			return true;
		}
	}else{
		return false;
	}
}
?>