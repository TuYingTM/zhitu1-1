<?php
namespace app\homes\controller;
use app\homes\controller\Base;
use think\Session;
use think\Request;
class Index extends Base{
	
	public function index()
	{
		$this->getNotice();
		$this->getGift();
		$this->getWarehouse();
		return view();
		
	}

	public function getsms()
	{
		$phone = input("phone");
		$sms = Db("sms")->where("sms_phone = '{$phone}' and sms_type = 1")->order("sms_id desc")->find();
		if(empty($sms)){
			//没有发送过验证码发送
			$id = GetSms($phone,1);
			if($id){
				//发送成功
				eyJson("验证码发送成功，请注意查收","ok");
			}else{
				//发送失败
				eyJson("Error：验证码发送失败！");
			}
		}else{
			$end_time = strtotime($sms['sms_addtime'])+600; //支付时间加10分钟，看是否到过期了还没有确认，如果没有，系统自动确定
			$do_time = date("Y-m-d H:i:s");
			if($end_time<=strtotime($do_time)){
				//验证码已过期重新发送
				$id = GetSms($phone,1);
				if($id){
					//发送成功
					eyJson("验证码发送成功，请注意查收","success");
				}else{
					//发送失败
					eyJson("Error：验证码发送失败！");
				}
			}else{
				//验证码未过期
				eyJson("Error：请使用上次接码的验证码！");
			}
		}
	}	
	
	public function reg()
	{

		if($this->IS_POWER()){
			header('Location: /');
			exit;
		}
		
		if(Request::instance()->isAjax()){
			$phone      = safe_string(trim(input('phone')));
			$password = md5(safe_string(trim(input('password'))));
			$promo = getRandNumber(0,999999,8);
			$code = input("code");
			

			$users_info = Db("users")->where("u_phone = '{$phone}'")->find();
			if(!empty($users_info)){
				eyJson("ERROR:用户已存在！");
			}
			
			//处理邀请码
			if(!empty($code))
			{
				$s_user_info = Db("users")->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_promo ='{$code}'")->find();
				if(empty($s_user_info)){
					eyJson("ERROR:邀请码不存在！");
				}
			}
			
			//处理邀请码
			
			if($this->__CONFIG__['s_smsswitch']==1){
				$sms = Db("sms")->where("sms_phone = '{$phone}' and sms_type = 1")->order("sms_id desc")->find();
				
				if(empty($sms)){
					eyJson("Error：请先获取验证码！");
				}else{
					$end_time = strtotime($sms['sms_addtime'])+600; //支付时间加10分钟，看是否到过期了还没有确认，如果没有，系统自动确定
					$do_time = date("Y-m-d H:i:s");
					if($end_time<=strtotime($do_time)){
						//验证码已过期重新发送
						eyJson("Error：手机验证码已过期，请重新获取！");
					}else{
						//验证码未过期
						$smscode = input('smscode');
						if($sms['sms_code'] != $smscode){
							eyJson("Error：验证码错误，请输入正确的验证！");
						}
					}
				}
			}
			

			$MODEL_DATA = array(
				'u_phone'   => $phone,
				'u_password' => $password,
				'u_addtime' => date("Y-m-d H:i:s"),
				'u_regip' => Request::instance()->ip(), //后台创建
				'u_status'  => 1,
				'ug_id' => $this->__CONFIG__['ug_id'],
				'u_promo' => $promo,
				'sub_id' => $this->__WEB_CONFIG__['sub_id'],
			);
			
			if(!empty($code))
			{
				$MODEL_DATA['su_id'] = $s_user_info['u_id'];
			}
			

			$res = Db("users")->insert($MODEL_DATA);
			if($res){
				eyJson("注册成功！","success");
			}else{
				eyJson("ERROR:注册失败！");
			}
		}
		$code = input("code");
		$this->assign("code",$code);
		return view();
	}
	
	public function login()
	{
		if($this->IS_POWER()){
			header('Location: /');
			exit;
		}
		if(Request::instance()->isAjax()){
			$phone    = safe_string(trim(input('phone')));
			$password = md5(safe_string(trim(input('password'))));
			
			$users_info = Db("users")
								//->alias("u")
								//->join("__USER_GROUP__ ug","u.ug_id = ug.ug_id")
								->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_phone = '{$phone}' and u_password = '{$password}'")->find();
			if(empty($users_info)){
				eyJson("ERROR:用户不存在！");
			}
			
			if($users_info['u_status']==2){
				eyJson("ERROR:用户已被禁用！");
			}
			
			//处理用户升级
			$user_group = Db("user_group")->where("ug_credit <= ".$users_info['u_credit']." and ug_status = 1")->order("ug_id desc")->find();
			if(!empty($user_group)){
				if($user_group['ug_id'] != $users_info['ug_id']){
					$USER_DATA = array(
						'ug_id' => $user_group['ug_id'],
					);
					Db("users")->where("u_id = {$users_info['u_id']}")->update($USER_DATA);
				}
			}
			//处理用户升级
		
			//对相关信息进行更新操作
			$USER_DATA = array(
				'u_count'        => $users_info['u_count'] + 1,
				'u_last_ip'      => $users_info['u_this_ip'],
				'u_last_time'    => $users_info['u_this_time'],
				'u_this_ip'      => Request::instance()->ip(),
				'u_this_time'    => date('Y-m-d H:i:s'),
			);

			Db("users")->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = {$users_info['u_id']}")->update($USER_DATA);
			//对写入登录成功日志表
			session("user_group_".$this->__WEB_CONFIG__['sub_id'],$user_group['ug_title']); //群名
			session("user_phone_".$this->__WEB_CONFIG__['sub_id'],$phone);
			session("user_id_".$this->__WEB_CONFIG__['sub_id'],$users_info['u_id']);
			eyJson("登录成功","success");
			
		}		
		return view();
	}
	
	public function outlogin()
	{
		session("user_group_".$this->__WEB_CONFIG__['sub_id'],null);
		session("user_phone_".$this->__WEB_CONFIG__['sub_id'],null);
		session("user_id_".$this->__WEB_CONFIG__['sub_id'],null);
		header('Location: /');
		exit;
	}
	
	public function getNotice() //查公告
	{
		if($this->__WEB_CONFIG__['sub_id']==0){ //所有主站公告 3 帮助说明 4 	网站公告
			$notice_1 = Db("notice")->where("sub_id = 0 and n_status = 1 and nt_id = 3")->order("n_id desc")->paginate(10);
			$notice_2 = Db("notice")->where("sub_id = 0 and n_status = 1 and nt_id = 4")->order("n_id desc")->paginate(10);
		}else{ //包含分站内容
			$notice_1 = Db("notice")->where("(sub_id = 0 or sub_id = ".$this->__WEB_CONFIG__['sub_id'].") and n_status = 1 and nt_id = 3")->order("n_id desc")->paginate(10);
			$notice_2 = Db("notice")->where("(sub_id = 0 or sub_id = ".$this->__WEB_CONFIG__['sub_id'].") and n_status = 1 and nt_id = 4")->order("n_id desc")->paginate(10);
		}
		$this->assign("notice_1",$notice_1);
		$this->assign("notice_2",$notice_2);
	}
	
	public function getGift() //查礼物
	{
		$gift = Db("gift")->where("gift_status = 1 and w_id <> ''")->order("gift_id desc")->paginate(8);
		$this->assign("gift",$gift);
	}
	
	public function getWarehouse() //查出仓库
	{
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_id desc")->select();
		$this->assign("warehouse",$warehouse);
	}


}