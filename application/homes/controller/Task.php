<?php
namespace app\homes\controller;
use app\homes\controller\Base;
use think\Session;
use think\Request;
class Task extends Base{
	
	public $userinfo;
	
	public function order()
	{
		$this->POWER();
		$this->GetUserInfo();
		$task = Db("Task_order")
				->alias("to")
				->join("__TASK__ t","t.t_id = to.t_id","left")
				->join("__TASK_TYPE__ tt","tt.tt_id  = t.tt_id","left")
				->join("__TASK_PLATFORM__ tp","tp.tpf_id = t.tpf_id","left")
				->join("__USERS__ u","u.u_id = to.u_id","left")
				->where("to.sub_id = ".$this->__WEB_CONFIG__['sub_id'])->order("to.t_id desc")->paginate(10);
		$page = $task->render();
		$this->assign('page', $page);
		$this->assign("task",$task);
		return view();		
	}
	
	public function index()
	{
		$this->POWER();
		$this->GetUserInfo();
		$type = input("type");
		if($type=="user"){
			$where = "t.u_id = ".$this->userinfo['u_id'];
		}else{
			$where = "t.t_status = 2";
		}
		$task = Db("Task")
				->field("t.*,tt.*,tp.*,u.*,(select count(to_id) from gift_task_order tos where tos.t_id = t.t_id and tos.to_status = 2) as counts ")
				->alias("t")
				->join("__TASK_TYPE__ tt","tt.tt_id  = t.tt_id","left")
				->join("__TASK_PLATFORM__ tp","tp.tpf_id = t.tpf_id","left")
				->join("__USERS__ u","u.u_id = t.u_id","left")
				->where("t.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and t.t_y_count > 0 and ".$where)->order("t.t_id desc")->paginate(10);
		$page = $task->render();
		$this->assign('page', $page);
		$this->assign("task",$task);
		return view();
	}
	
	public function subokuser()
	{
		$this->POWER();
		$this->GetUserInfo();
		if(Request::instance()->isAjax()){
			$t_id = safe_string(trim(input('t_id')));
			$id  = safe_string(trim(input('id')));
			
			$task_order = Db("task_order")->alias("to")->join("__USERS__ u","to.u_id = u.u_id","left")->where("t_id = {$t_id} and to_id = {$id}")->find();
			
			$MODEL_DATA = array(
				'to_status' => 4,
				'to_ok_addtime' => date("Y-m-d H:i:s"),
			);
			
			$res = Db("task_order")->where("t_id = {$t_id} and to_id = {$id}")->update($MODEL_DATA);
			if($res){
				//任务完成，余额相加
				$balance = $task_order['u_balance'] + $task_order['to_money'];
				$USERS_DATA = array(
					'u_balance' => $balance,
				);
				Db("users")->where("u_id = {$task_order['u_id']}")->update($USERS_DATA);
				write_acpital($task_order['u_id'],$task_order['sub_id'],1,$task_order['to_money'],"#{$t_id}任务完成",8,1); //写入资金记录表
				//任务完成，余额相加
				//余额减1
				$_task_ = Db("task")->where("t_id = {$t_id}")->find();
				$MODEL_DATA = array(
					't_y_money' => $_task_['t_y_money']-$_task_['t_money'],
				);
				Db("task")->where("t_id = {$t_id}")->update($MODEL_DATA);
				//余额减1
				eyJson("确认成功!","success");
			}else{
				eyJson("ERROR:确认失败！");
			}
		}		
	}
	
	public function sub()
	{
		$this->POWER();
		$this->GetUserInfo();
		if(Request::instance()->isAjax()){
			$editor = trim(input('editor'));
			$t_id   = trim(input('t_id'));
			$money   = trim(input('money'));


			$DATA = array(
				'u_id'          => $this->userinfo['u_id'],
				'sub_id'        => $this->userinfo['sub_id'],
				't_id'          => $t_id,
				'to_addtime'    => date("Y-m-d H:i:s"),
				'to_ok_addtime' => date("Y-m-d H:i:s"),
				'to_count'      => 1,
				'to_money'      => $money,
				'to_status'     => 2,
				'to_desc'       => $editor,
			);
			
			$res = Db("task_order")->insert($DATA);
			if($res){
				//余单减1
				$_task_ = Db("task")->where("t_id = {$t_id}")->find();
				$MODEL_DATA = array(
					't_y_count' => $_task_['t_y_count']-1,
				);
				Db("task")->where("t_id = {$t_id}")->update($MODEL_DATA);
				//余单减1 
				eyJson("提交任务成功！","success");
			}else{
				eyJson("ERROR:提交任务失败！");
			}
			
		}		
	}
	
	public function content()
	{
		$this->POWER();
		$this->GetUserInfo();
		$id = safe_string(trim(input('id')));
		$info = Db("Task")
				->alias("t")
				->join("__TASK_TYPE__ tt","tt.tt_id  = t.tt_id","left")
				->join("__TASK_PLATFORM__ tp","tp.tpf_id = t.tpf_id","left")
				->join("__USERS__ u","u.u_id = t.u_id","left")
				->where("t.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and t.t_id = {$id}")->find();
		
		$task_order = Db("task_order")
				->alias("t")
				->join("__USERS__ u","u.u_id = t.u_id","left")
				->where("t.t_id = {$id} and t.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and t.u_id = ".$this->userinfo['u_id'])
				->find();
		$task_select = Db("task_order")
					->alias("t")
					->join("__USERS__ u","u.u_id = t.u_id","left")
					->where("t.t_id = {$id} and t.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and t.u_id <> ".$this->userinfo['u_id'])
					->select();
		
		$this->assign("task_select",$task_select);
		$this->assign("task_order",$task_order);
		$this->assign("info",$info);
		return view();
	}
	
	public function add()
	{
		$this->POWER();
		$this->GetUserInfo();
		if(Request::instance()->isAjax()){
			$tpf_id  = safe_string(trim(input('tpf_id')));
			$tt_id   = safe_string(trim(input('tt_id')));
			//$title   = safe_string(trim(input('title')));
			$dpname  = safe_string(trim(input('dpname')));
			$url     = safe_string(trim(input('url')));
			$count   = safe_string(trim(input('count')));
			$money   = safe_string(trim(input('money')));
			$editor  = trim(input('editor'));
			$zjmoney = $count * $money;
			
			if($zjmoney > $this->userinfo['u_balance']){
				eyJson("您的余额不足，请先充值！");
			}
			
			$DATA = array(
				'u_id'      => $this->userinfo['u_id'],
				'sub_id'    => $this->userinfo['sub_id'],
				'tpf_id'    => $tpf_id,
				'tt_id'     => $tt_id,
				//'t_title'   => $title,
				't_count'   => $count,
				't_url'     => $url,
				//'t_key'     => $newpassword,
				't_desc'    => $editor,
				't_money'   => $money,
				't_z_money' => $zjmoney,
				't_y_money' => $zjmoney,
				't_y_count' => $count,
				't_addtime' => date("Y-m-d H:i:s"),
				't_status'  => 2,
				't_dp_name' => $dpname,
			);
			
			$res = Db("task")->insert($DATA);
			if($res){
				//扣除用户余额
				$balance = $this->userinfo['u_balance'] - $zjmoney;
				$USERS_DATA = array(
					'u_balance' => $balance,
				);
				Db("users")->where("u_id = {$this->userinfo['u_id']}")->update($USERS_DATA);
				//扣除用户余额
				write_acpital($this->userinfo['u_id'],$this->userinfo['sub_id'],2,$zjmoney,"下发任务",6,1); //写入资金记录表
				eyJson("发布任务成功！","success");
			}else{
				eyJson("ERROR:发布任务失败！");
			}

			
			
		}
		$task_type = Db("task_type")->where("tt_status = 1")->select();
		$task_platform = Db("task_platform")->where("tpf_status = 1")->select();
		$this->assign("task_type",$task_type);
		$this->assign("task_platform",$task_platform);
		return view();	
	}
	
	
	
	
	public function GetUserInfo()
	{
		$this->userinfo = Db("users")
					->alias("u")
					->join("__USER_GROUP__ ug","ug.ug_id = u.ug_id","left")
					->where("u.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u.u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->find();
		$this->assign("userinfo",$this->userinfo);
	}
	
	
	
	
}