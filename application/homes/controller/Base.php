<?php
namespace app\homes\controller;
use think\Controller;
use think\Db;
use think\Session;
use think\Request;

class Base extends Controller{
	
	public $__WEB_CONFIG__ = array();
	public $__CONFIG__ = "";
	
	public function _initialize(){
		$request = Request::instance();
		$this->__CONFIG__ = GetConfig(); //读取配置信息
		$this->isDomain();
		
		//$this->assign("config",$this->__CONFIG__); 
		//$this->__MODELFILE__ = strtolower($request->controller()."/".$request->action());
		//$this->assign("language",$this->__LANGCONFIG__);
		
	}
	
	public function POWER()
	{
	    $request = Request::instance();
	    $url11 = strtolower($request->controller()."/".$request->action());
	    
		if(empty(session("user_id_".$this->__WEB_CONFIG__['sub_id'])))
		{
		    $login_url = url("index/login");
		    if($url11 == "order/buy"){
                header("Location: ".$login_url);
                exit;
		    }
			eyError("您未登录，请先登录再下单！",$login_url);
			exit;
		}
	}
	
	//验证域名是否包含在后台平台中
	protected function isDomain(){
		$domain = $_SERVER['HTTP_HOST'];
		if(strtolower($this->__CONFIG__['s_domain']) != strtolower($domain)){  //判断是不是总站域名
			$sub_domain = Db("substation")->where("sub_domain = '{$domain}'")->find();
			if(empty($sub_domain)){ //分站不存在
				Header("HTTP/1.1 303 See Other");
				Header("Location: http://www.baidu.com/");
				exit;
			}else{
				if($sub_domain['sub_status']==2){ //分站被禁用
					Header("HTTP/1.1 303 See Other");
					Header("Location: http://www.baidu.com/");
					exit;
				}else{
					$this->__WEB_CONFIG__['web_title']     = $sub_domain['sub_webtitle'];  //网站名称
					$this->__WEB_CONFIG__['web_logo']      = $sub_domain['sub_weblogo'];   //网站标志
					$this->__WEB_CONFIG__['web_fastmoney'] = $sub_domain['sub_fastmoney']; //快捷金额
					$this->__WEB_CONFIG__['qq_1']          = $sub_domain['sub_qq1']; //客服QQ
					$this->__WEB_CONFIG__['qq_2']          = $sub_domain['sub_qq2']; //财务QQ
					$this->__WEB_CONFIG__['sub_id']        = $sub_domain['sub_id']; //分站ID
					//配置信息
					$this->__WEB_CONFIG__['s_commission_one']   = $sub_domain['sub_commission_one']; //直推用金
					$this->__WEB_CONFIG__['s_credit']   = $sub_domain['sub_credit']; //积分比例
				}
			}
		}else{
			$this->__WEB_CONFIG__['web_title']     = $this->__CONFIG__['s_webtitle'];  //网站名称
			$this->__WEB_CONFIG__['web_logo']      = $this->__CONFIG__['s_weblogo'];   //网站标志
			$this->__WEB_CONFIG__['web_fastmoney'] = $this->__CONFIG__['s_fastmoney']; //快捷金额
			$this->__WEB_CONFIG__['qq_1']          = $this->__CONFIG__['s_qq1']; //客服QQ
			$this->__WEB_CONFIG__['qq_2']          = $this->__CONFIG__['s_qq2']; //财务QQ
			$this->__WEB_CONFIG__['sub_id']        = 0; //表示主站
			//配置信息
			$this->__WEB_CONFIG__['s_commission_one'] = $this->__CONFIG__['s_commission_one']; //直推用金
			$this->__WEB_CONFIG__['s_credit'] = $this->__CONFIG__['s_credit']; //积分比例
			//----------------------分站纬使用的配置
			$this->__WEB_CONFIG__['s_tixian_min'] = $this->__CONFIG__['s_tixian_min']; //最小提现
			$this->__WEB_CONFIG__['s_smsswitch'] = $this->__CONFIG__['s_smsswitch']; //短信开关
		}
		
		
		
		$this->assign("web_domain",$domain); 
		$this->assign("web_config",$this->__WEB_CONFIG__); 
		$this->assign("config",$this->__CONFIG__); 
	}
	
	public function IS_POWER()
	{
		if(empty(session("user_id_".$this->__WEB_CONFIG__['sub_id']))){
			return false; //表示未登录
		}else{
			return true; //表示已登录
		}
	}
	
}
