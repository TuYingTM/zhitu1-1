<?php
namespace app\homes\controller;
use app\homes\controller\Base;
use think\Session;
use think\Request;

class Upload extends Base
{

    public function xls()
    {
		$JsonMessage['success'] = 'error';
		$UP_type = "xls,xlsx,vnd.ms-excel";
		$UP_type = explode(",",$UP_type); //上传类型\
		
		$UP_file = _PATH_."/upload/xls/";  //上传目录
		$_UP_file = "/upload/xls/";
		if(!is_dir($UP_file)){
			 mkdir ($UP_file,0777,true);
		}
		$tmp_arr = $_FILES['filename'];
		$_type   = explode("/",$tmp_arr['type']);
		$_type   = @$_type[1];
		$tmp_name= time();
		
		
		if($_type == "vnd.ms-excel"){
			$_type = "xls";
		}
		
		$tmp_name= $tmp_name.".".$_type;
		if(!in_array($_type,$UP_type)){
			$JsonMessage['success'] = 'error';
			$JsonMessage['msg'] = 'error:文件类型错误，只支持xls,xlsx！'.$tmp_name;
			echo json_encode($JsonMessage);
			exit;
		}
		
		
		
		if(move_uploaded_file($tmp_arr["tmp_name"],$UP_file.$tmp_name)){
			$JsonMessage['success'] = 'ok';
			$JsonMessage['fileurl'] = $_UP_file.$tmp_name;
			$JsonMessage['msg']     = '上传成功';
			echo json_encode($JsonMessage);
			exit;
		}else{
			$JsonMessage['success'] = 'error';
			$JsonMessage['msg'] = 'error:文件保存失败，请确认有写入权限！';
			echo json_encode($JsonMessage);
			exit;
		};
		
		
		
    }

}
