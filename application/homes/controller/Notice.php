<?php
namespace app\homes\controller;
use app\homes\controller\Base;
use think\Session;
use think\Request;
class Notice extends Base{
	
	public function content()
	{
		$this->GetOneNotice();
		$this->getNewNotice();
		return view();
	}
	
	public function lists()
	{
		$id = input("id");
		$notice_type = Db("notice_type")->where("nt_id = {$id}")->find();
		$this->assign("notice_type",$notice_type);
		$this->GetListNotice();
		$this->GetRandGift();
		return view();
	}
	
	public function GetRandGift() //取随机礼物
	{
		$gift = Db("gift")->where("gift_status = 1")->orderRaw('rand()')->limit(6)->select();
		$this->assign('gift', $gift);
	}
	
	public function GetListNotice($pagecount=20)
	{
		$id = input("id");
		if($this->__WEB_CONFIG__['sub_id']==0){ //所有主站公告
			$noticelist = Db("notice")->where("sub_id = 0 and n_status = 1 and nt_id = {$id}")->order("n_id desc")->paginate($pagecount);
		}else{ //包含分站内容
			$noticelist = Db("notice")->where("(sub_id = 0 or sub_id = ".$this->__WEB_CONFIG__['sub_id'].") and n_status = 1 and nt_id = {$id}")->order("n_id desc")->paginate($pagecount);
		}
		$page = $noticelist->render();
		$this->assign('page', $page);
		$this->assign("noticelist",$noticelist);	
	}
	
	public function GetOneNotice()
	{
		$id = input("id");
		if($this->__WEB_CONFIG__['sub_id']==0){ //所有主站公告
			$notice = Db("notice")->alias("n")->join("__NOTICE_TYPE__ nt","nt.nt_id = n.nt_id","left")->where("sub_id = 0 and n_status = 1 and n_id = {$id}")->find();
		}else{ //包含分站内容
			$notice = Db("notice")->alias("n")->join("__NOTICE_TYPE__ nt","nt.nt_id = n.nt_id","left")->where("(sub_id = 0 or sub_id = ".$this->__WEB_CONFIG__['sub_id'].") and n_status = 1 and n_id = {$id}")->find();
		}
		if(empty($notice)){
			echo "公告不存在！";
			exit;
		}
		$this->SetReadCount($notice);
		$this->assign("notice",$notice);
	}
	
	public function SetReadCount($notice)
	{
		$DATA = array(
			'n_read'   => $notice['n_read'] + 1,
		);
			
		Db("notice")->where("n_id = '{$notice['n_id']}'")->update($DATA);
	}
	
	public function getNewNotice() //查公告
	{
		if($this->__WEB_CONFIG__['sub_id']==0){ //所有主站公告 3 帮助说明 4 	网站公告
			$notice_list = Db("notice")->where("sub_id = 0 and n_status = 1")->order("n_id desc")->paginate(10);
		}else{ //包含分站内容
			$notice_list = Db("notice")->where("(sub_id = 0 or sub_id = ".$this->__WEB_CONFIG__['sub_id'].") and n_status = 1")->order("n_id desc")->paginate(10);
		}
		$this->assign("notice_list",$notice_list);
	}	
	
	

}