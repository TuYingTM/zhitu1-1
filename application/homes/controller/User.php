<?php
namespace app\homes\controller;
use app\homes\controller\Base;
use think\Session;
use think\Request;
class User extends Base{
	
	public function index()
	{
		$this->POWER();
		$this->GetNotice();
		$this->GetUserInfo();
		$this->GetUserGroup();
		$this->getWarehouse();
		return view();
	}
	
	public function sendinfo()
	{
		$this->POWER();
		if(Request::instance()->isAjax()){
			$send_phone = safe_string(trim(input('send_phone')));
			$send_name  = safe_string(trim(input('send_name')));
			
			//对相关信息进行更新操作
			$USER_DATA = array(
				'u_send_phone' => $send_phone,
				'u_send_name'  => $send_name,
			);

			$res = Db("users")
				->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))
				->update($USER_DATA);
			if($res){
				eyJson("修改成功","success");
			}else{
				eyJson("ERROR:修改失败！");
			}
		}	
		$this->GetUserInfo();
		return view();
	}
	
	public function changepwd()
	{
		$this->POWER();
		if(Request::instance()->isAjax()){
			$oldpassword = safe_string(trim(input('oldpassword')));
			$newpassword = safe_string(trim(input('newpassword')));
			$oldpassword = md5($oldpassword);
			
			$userinfo = Db("users")
						->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id'])." and u_password = '{$oldpassword}'")
						->find();
			if(empty($userinfo)){
				eyJson("旧密码错误，请重新输入！");
			}
			
			
			$USER_DATA = array(
				'u_password' => md5($newpassword),
			);
			
			$res = Db("users")->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->update($USER_DATA);
			if($res){
				eyJson("修改密码成功","success");
			}else{
				eyJson("修改密码失败");
			}
			
		}
		$this->GetUserInfo();
		return view();
	}
	
	public function union()
	{
		$this->POWER();
		$this->GetUserInfo();
		$sql = "select count(r_id) from gift_recharge r where r.u_id = u.u_id";
		$user = Db("users")->field("u.*,({$sql}) as tgcount")->alias("u")->where("u.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u.su_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->paginate(10);
		$page = $user->render();
		$this->assign('page', $page);
		$this->assign("user",$user);
		return view();
	}
	
	public function credit()
	{
		$this->POWER();
		$this->GetUserInfo();
		$credit = Db("credit_logs")->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->order("crl_id desc")->paginate(10);
		$page = $credit->render();
		$this->assign('page', $page);
		$this->assign("credit",$credit);
		return view();
	}
	
	public function tixian()
	{
		$this->POWER();
		$this->GetUserInfo();
		$tixian = Db("tixian")->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->order("tx_id desc")->paginate(10);
		$page = $tixian->render();
		$this->assign('page', $page);
		$this->assign("tixian",$tixian);
		return view();	
	}
	
	public function tixianadd()//提现，1是用金提现 2是分站提现
	{
		$this->POWER();
		$this->GetUserInfo();
		$typeid = input("type");
		if(Request::instance()->isAjax()){
			$money = safe_string(trim(input('money')));
			if($typeid==1){
				$res = WriteUser_commission(session("user_id_".$this->__WEB_CONFIG__['sub_id']),$this->__WEB_CONFIG__['sub_id'],$money,2);
				if($res){
					write_tixian_log(session("user_id_".$this->__WEB_CONFIG__['sub_id']),$this->__WEB_CONFIG__['sub_id'],$money,"冻结佣金{$money}",1);
					write_acpital(session("user_id_".$this->__WEB_CONFIG__['sub_id']),$this->__WEB_CONFIG__['sub_id'],2,$money,"",4,2);
				}
			}else{
				$res = WriteUser_substation(session("user_id_".$this->__WEB_CONFIG__['sub_id']),$this->__WEB_CONFIG__['sub_id'],$money,2);
				if($res){
					write_tixian_log(session("user_id_".$this->__WEB_CONFIG__['sub_id']),$this->__WEB_CONFIG__['sub_id'],$money,"冻结赢利{$money}",2);
					write_acpital(session("user_id_".$this->__WEB_CONFIG__['sub_id']),$this->__WEB_CONFIG__['sub_id'],2,$money,"",4,3);
				}
			}

			if($res){
				
				
				$u_zfb_title = safe_string(trim(input('u_zfb_title')));
				$u_zfb_name  = safe_string(trim(input('u_zfb_name')));
			
				//对相关信息进行更新操作
				$USER_DATA = array(
					'u_zfb_title' => $u_zfb_title,
					'u_zfb_name'  => $u_zfb_name,
				);
				$res = Db("users")
					->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))
					->update($USER_DATA);	
				eyJson("提现申请提交成功","success");
			}else{
				eyJson("提现申请提交失败");
			}
			
		}		
		
		$this->assign('typeid', $typeid);
		return view();
	}
	
	public function score()
	{
		$this->POWER();
		$this->GetUserInfo();
		//处理资金明细
		$type = input("type");
		if(empty($type)){
			$where = "1=1"; //全部
		}elseif($type==1){
			$where = "cl_attribution = 1"; //余额
		}elseif($type==2){
			$where = "cl_attribution = 2"; //用金
		}
		$capital = Db("capital_logs")->where("sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id'])." and ".$where)->order("cl_id desc")->paginate(10);
		$page = $capital->render();
		$this->assign('page', $page);
		$this->assign("capital",$capital);
		$this->assign("type",$type);
		//处理资金明细
		return view();
	}
	
	public function rechargelist()
	{
		$this->POWER();
		$this->GetUserInfo();
		$recharge = Db("recharge")
						->field("r.*,p.pay_id as pay_ids,p.pay_title")
						->alias("r")
						->join("__PAY__ p","p.pay_id = r.pay_id","left")
						->where("r.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and r.u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->order("r.r_id desc")->paginate(10);
		$page = $recharge->render();
		$this->assign('page', $page);
		$this->assign("recharge",$recharge);
		return view();
	}
	
	public function paylist()
	{
		$pay = input("pay");
		$paylist = Db("pay")->where("pay_status = 1")->select();
		$this->assign('pay', $pay);
		$this->assign('paylist', $paylist);
	}
	
	public function recharge_pay_ok()
	{
		$this->POWER();
		$this->GetUserInfo();
		if(Request::instance()->isAjax()){
			$money = safe_string(trim(input('money')));
			$paytag = input("paytag");
			$payinfo = Db("pay")->where("pay_tag = '{$paytag}'")->find();
			$MODEL_DATA = array(
				'u_id'          => session("user_id_".$this->__WEB_CONFIG__['sub_id']),
				'sub_id'        => $this->__WEB_CONFIG__['sub_id'],
				'r_money'       => $money,
				'r_addtime'     => date("Y-m-d H:i:s"),
				'r_status'      => 4,
				'pay_id'        => $payinfo['pay_id'],
			);
		
			$res = Db("recharge")->insertGetId($MODEL_DATA);
			if($res){
				import("pay.Pay{$paytag}");
				$Pay = new \Pays;
				$Pay->payinfo['appid']      = $payinfo['pay_appid'];
				$Pay->payinfo['appkey']     = $payinfo['pay_key'];
				$Pay->payinfo['money']      = $money;
				$Pay->payinfo['uid']        = $this->__WEB_CONFIG__['sub_id']."_".session("user_id_".$this->__WEB_CONFIG__['sub_id']);
				$Pay->payinfo['param']      = "uid:".session("user_id_".$this->__WEB_CONFIG__['sub_id'])."_orderid:".$res."_money:".$money."_content:";
				$Pay->payinfo['notify_url'] = $this->get_http_type().$_SERVER['HTTP_HOST'].url("user/pay_notify");
				$Pay->payinfo['return_url'] = $this->get_http_type().$_SERVER['HTTP_HOST'].url("user/pay_notify");
				$url = $Pay->pay();
				eyJson($url,"success");
			}else{
				eyJson("ERROR:提交充值信息失败！");
			}
		}
	}
	
	function get_http_type()
	{
			$http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
			return $http_type;
	}
	
	public function pay_notify_url()
	{
		echo "我用于跳转";
		exit;
	}
	
	//用于返回数据处理
	public function pay_notify()
	{
		$params = $_REQUEST;
		if(!empty($params)){
			$url = url("user/rechargelist");
			$param = explode("_",$params['param']);
			$money = explode(":",$param[2]);
			$money = $money[1];
			$content = explode(":",$param[3]);
			$content = $content[1];
			$orderid = explode(":",$param[1]);
			$orderid = $orderid[1];
		
		    //
			$info = Db("recharge")->alias("r")->where("r.r_id = {$orderid} and r.r_status = 4")->join("__PAY__ pay","pay.pay_id = r.pay_id","left")->find();
		
			
			if(empty($info)){
					
				echo "<script>alert('订单不存在!');window.location.href = '".$url."';</script>";
				exit;
				//exit("订单不存在！");
			}else{
				import("pay.Pay{$info['pay_tag']}");
				$Pay = new \Pays;
				$Pay->payinfo = $params;
				$Pay->notify();
				if($Pay->param['message'] == "ok"){
					
					//if($res){
						//处理充值积分等操作
	
			
						//修改用户积分，余额
						$users = Db("users")->where("u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->find();
						
						
						
						$credit = $money * $this->__CONFIG__['s_credit'];
						$credit = $users['u_credit'] + $credit;
						
						$balance = $money + $users['u_balance'];
						$USERS_DATA = array(
							'u_credit'  => $credit,
							'u_balance' => $balance,
						);
						
					
						
						Db("users")->where("u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->update($USERS_DATA);
						//修改用户积分，余额
						//写入积分日记
						$LOG_DATA = array(
							'u_id'        => $users['u_id'],
							'sub_id'      => $users['sub_id'],
							'crl_creadit' => $money,
							'crl_balance' => $credit,
							'crl_addtime' => date("Y-m-d H:i:s"),
							'crl_content' => $content,
						);
					
						Db("credit_logs")->insert($LOG_DATA);
						//写入积分日记
						write_acpital($users['u_id'],$users['sub_id'],1,$money,$content,1,1); //写入资金记录表
						//写入分销信息
						if($users['su_id']!=0){
							//为上级写入余额
							$su_users = Db("users")->where("u_id = '{$users['su_id']}'")->find(); //查询上级用户
							$yz_money   = $money * $this->__CONFIG__['s_commission_one'];
							$u_commission = $yz_money + $su_users['u_commission'];
							$USERS_DATA_1 = array(
								'u_commission' => $u_commission,
							);
							Db("users")->where("u_id = '{$users['su_id']}'")->update($USERS_DATA_1);
							//为上级写入余额
							
							write_acpital($users['su_id'],$users['sub_id'],1,$yz_money,"直推用户分润",3,2); //为上级写入资金记录表
						}
						//写入分销信息
						
	                    $MODEL_DATA = array(
					    	'r_status' => 2,
					    	'r_pay_addtime'  => date("Y-m-d H:i:s"),
				    	);
				    	$res = Db("recharge")->where("r_id = {$orderid}")->update($MODEL_DATA);
						
						echo "<script>alert('支付成功!');window.location.href = '".$url."';</script>";
						
					//}
				}
				exit($Pay->param['title']);
			}
		}else{
			exit("error:请不要直接访问！");
		}
	}
	
	public function recharge_pay()
	{
		$this->POWER();
		$this->GetUserInfo();
		$this->paylist();
		return view();
	}
	
	public function recharge()
	{
		$this->POWER();
		$this->GetUserInfo();
		$this->paylist();
		return view();
	}
	
	public function recharge_bank()
	{
		$this->POWER();
		if(Request::instance()->isAjax()){
			$money = safe_string(trim(input('money')));
			$MODEL_DATA = array(
				'u_id'          => session("user_id_".$this->__WEB_CONFIG__['sub_id']),
				'sub_id'        => $this->__WEB_CONFIG__['sub_id'],
				'r_money'       => $money,
				'r_addtime'     => date("Y-m-d H:i:s"),
				'r_status'      => 1,
				'r_pay_addtime' => date("Y-m-d H:i:s"),
				'pay_id'        => 9999,
			);
		
			$res = Db("recharge")->insert($MODEL_DATA);
			if($res){
				eyJson("提交充值信息成功！","success");
			}else{
				eyJson("ERROR:提交充值信息失败！");
			}
		}
		$this->GetUserInfo();
		$this->paylist();
		return view();
	}	
	
	
	public function GetNotice()
	{
		if($this->__WEB_CONFIG__['sub_id']==0){ //所有主站公告 3 帮助说明 4 	网站公告
			$notice = Db("notice")->where("sub_id = 0 and n_status = 1")->order("n_id desc")->paginate(8);
		}else{ //包含分站内容
			$notice = Db("notice")->where("(sub_id = 0 or sub_id = ".$this->__WEB_CONFIG__['sub_id'].") and n_status = 1")->order("n_id desc")->paginate(8);
		}
		$this->assign("notice",$notice);
	}
	
	public function getWarehouse() //查出仓库
	{
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_id desc")->select();
		$this->assign("warehouse",$warehouse);
	}
	
	public function GetUserInfo()
	{
		$userinfo = Db("users")
					->alias("u")
					->join("__USER_GROUP__ ug","ug.ug_id = u.ug_id","left")
					->where("u.sub_id = ".$this->__WEB_CONFIG__['sub_id']." and u.u_id = ".session("user_id_".$this->__WEB_CONFIG__['sub_id']))->find();
		$this->assign("userinfo",$userinfo);
	}
	
	public function GetUserGroup()
	{
		$user_group = Db("user_group")
			//->alias("ug")
			//->join("__FREIGHT__ f","f.ug_id = ug.ug_id","right")
			->where("ug_status = 1")
			->order("ug_credit asc")
			->select();
		if(!empty($user_group))
		{
			$count = count($user_group);
			for($i=0;$i<$count;$i++){
				$group = Db("freight")
							->where("f_status = 1 and ug_id = ".$user_group[$i]['ug_id']." and sub_id = ".$this->__WEB_CONFIG__['sub_id'])
							->order("w_id desc")
							->select();
				$user_group[$i]['group'] = $group;
			}
		}
		$this->assign("user_group",$user_group);
	}
	

}