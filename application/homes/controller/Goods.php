<?php
namespace app\homes\controller;
use app\homes\controller\Base;
use think\Session;
use think\Request;
class Goods extends Base{
	
	public function index() //所有礼物
	{
		$this->getWarehouse();
		$this->GetAll();
		return view();
		
	}
	
	public function lists() //按仓库查找商品
	{
		$wid = input("wid");
		$this->getWarehouse();
		$this->GetWAll($wid);
		$this->assign("wid",$wid);
		return view();
	}
	
	
	public function content()
	{
		$this->GetOneGift();
		$this->GetRandGift();
		return view();
	}
	
	public function GetRandGift() //取随机礼物
	{
		$gift = Db("gift")->where("gift_status = 1")->orderRaw('rand()')->limit(6)->select();
		$this->assign('gift', $gift);
	}	
	
	
	public function getWarehouse() //查出仓库
	{
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_id desc")->select();
		$this->assign("warehouse",$warehouse);
	}
	
	public function GetOneGift()
	{
		$id = input("id");
		$info = Db("gift")->where("gift_status = 1 and gift_id = {$id}")->find();
		if(empty($info)){
			echo "数据不存在,请勿非法提交！";
			exit;
		}
		
		$this->assign("info",$info);
	}
	
	public function GetAll()
	{
		$gift = Db("gift")->where("gift_status = 1 and w_id <> ''")->order("gift_id desc")->select();
		$this->assign("gift",$gift);
	}
	
	public function GetWAll($wid)
	{
		$gift = Db("gift")->where("gift_status = 1 and w_id <> '' and (w_id like '%,{$wid},%' or w_id like '{$wid},%')")->order("gift_id desc")->select();
		$this->assign("gift",$gift);
	}
	
}