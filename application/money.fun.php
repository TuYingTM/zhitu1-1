<?php
/*
1.增加余额 （余额=余额+增加金额 冻结不变）
2.用户提现（提现金额，余额=余额-提现金额，冻结=冻结+提现金额）
3.支付退回（退回金额，余额=余额+退回金额，冻结=冻结-退回金额）
4.确认支付（余额不变，冻结=冻结-确认金额）
*/
function WriteUser_commission($userid,$subid,$money,$type)
{
	//用户佣金操作
	$table = "users";
	$userinfo = Db($table)->where("sub_id = {$subid} and u_id = {$userid}")->find();
	if($type==1){
		$yr_money = $userinfo['u_commission'] + $money;
		$dj_money = $userinfo['u_commission_freeze'];
	}elseif($type==2){
		$yr_money = $userinfo['u_commission'] - $money;
		$dj_money = $userinfo['u_commission_freeze'] + $money;
	}elseif($type==3){
		$yr_money = $userinfo['u_commission'] + $money;
		$dj_money = $userinfo['u_commission_freeze'] - $money;
	}elseif($type==4){
		$yr_money = $userinfo['u_commission'];
		$dj_money = $userinfo['u_commission_freeze'] - $money;
	}
	
	$data = ([
		"u_commission"        => $yr_money,
		"u_commission_freeze" => $dj_money,
	]);
				
	$ID = Db($table)->where("sub_id = {$subid} and u_id = {$userid}")->update($data);
	if(!empty($ID)){
		return true;
	}else{
		return false;
	}
	
}

function WriteUser_substation($userid,$subid,$money,$type)
{
	//分站赢利操作
	$table = "users";
	$userinfo = Db($table)->where("sub_id = {$subid} and u_id = {$userid}")->find();
	if($type==1){
		$yr_money = $userinfo['u_substation'] + $money;
		$dj_money = $userinfo['u_substation_freeze'];
	}elseif($type==2){
		$yr_money = $userinfo['u_substation'] - $money;
		$dj_money = $userinfo['u_substation_freeze'] + $money;
	}elseif($type==3){
		$yr_money = $userinfo['u_substation'] + $money;
		$dj_money = $userinfo['u_substation_freeze'] - $money;
	}elseif($type==4){
		$yr_money = $userinfo['u_substation'];
		$dj_money = $userinfo['u_substation_freeze'] - $money;
	}
	
	$data = ([
		"u_substation"        => $yr_money,
		"u_substation_freeze" => $dj_money,
	]);
				
	$ID = Db($table)->where("sub_id = {$subid} and u_id = {$userid}")->update($data);
	if(!empty($ID)){
		return true;
	}else{
		return false;
	}
	
}

//提现相关写入
function write_tixian_log($userid,$subid,$money,$content,$tx_type)
{
	$data = ([
		"u_id"       => $userid,
		"sub_id"     => $subid,
		"tx_addtime" => date("Y-m-d H:i:s"),
		"tx_money"   => $money,
		"tx_content" => $content,
		"tx_status"  => 1,
		"tx_type"    => $tx_type,
	]);
	$ID = Db("tixian")->insertGetId($data);
	if(!empty($ID)){
		return true; //添加失败
	}else{
		return false; //添加成功
	}
}


//写入资金记录表
function write_acpital($userid,$subid,$direction,$money,$content,$type,$attribution)
{
	$userinfo = Db("users")->where("u_id = {$userid} and sub_id = {$subid}")->find();
		if($attribution==1){ //余额
			$balance = $userinfo['u_balance'];
		}elseif($attribution==2){ //用金
			$balance = $userinfo['u_commission'];
		}
	

	$data = ([
		"u_id"            => $userid,
		"sub_id"          => $subid,
		"cl_direction"    => $direction,
		"cl_money"        => $money,
		"cl_balance"      => $balance,
		"cl_content"      => $content,
		"cl_addtime"      => date("Y-m-d H:i:s"),
		"cl_type"         => $type,
		"cl_attribution"  => $attribution,
	]);
	$ID = Db("capital_logs")->insertGetId($data);
	if(!empty($ID)){
		return true; //添加失败
	}else{
		return false; //添加成功
	}	
}
?>