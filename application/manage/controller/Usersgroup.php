<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Usersgroup extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("users");
		//增删改权限显示处理
		$user_group = Db("user_group")->where("1 = 1")->order("ug_credit desc,ug_id desc")->select();
		$this->assign("group",$user_group);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			
			$users = Db("users")->where("ug_id = {$id}")->find();
			if(!empty($users)){
				eyJson("此群组存在会员，无法删除，请先删除会员！");
			}
			
			if($this->__CONFIG__['ug_id'] == $id){
				eyJson("系统默认群组无法删除！");
			}

			$res = Db("user_group")->where("ug_id = {$id}")->delete();
			if($res){
				eyJson("删除用户群组信息成功！","success");
			}else{
				eyJson("ERROR：删除用户群组信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("user_group")->where("ug_id = {$id}")->find();
		
		
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$credit = safe_string(trim(input('credit')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'ug_title'   => $title,
				'ug_sort'    => $sort,
				'ug_status'  => $status,
				'ug_credit'  => $credit,
			);
			
			$res = Db("user_group")->where("ug_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改用户群组成功!","success");
			}else{
				eyJson("ERROR:修改用户群组失败！");
			}
		}
	
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$credit = safe_string(trim(input('credit')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'ug_title'   => $title,
				'ug_addtime' => date("Y-m-d H:i:s"),
				'ug_sort'    => $sort,
				'ug_status'  => $status,
				'ug_credit'  => $credit,
			);
			
			$res = Db("user_group")->insert($MODEL_DATA);
			if($res){
				eyJson("添加用户群组成功！","success");
			}else{
				eyJson("ERROR:添加用户群组失败！");
			}
		}
		return view();
	}

}