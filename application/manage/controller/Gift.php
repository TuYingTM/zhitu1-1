<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Gift extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("gift");
		//增删改权限显示处理
		$gift = Db("gift")->where("1 = 1")->order("gift_sort asc,gift_id desc")->select();
		$this->assign("gift",$gift);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}


			$res = Db("gift")->where("gift_id = {$id}")->delete();
			if($res){
				eyJson("删除礼物信息成功！","success");
			}else{
				eyJson("ERROR：删除礼物信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("gift")->where("gift_id = {$id}")->find();
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$content     = trim(input('content'));
			$money     = safe_string(trim(input('money')));
			$w_id     = safe_string(trim(input('w_id')));
			$count     = safe_string(trim(input('count')));
			$unit     = safe_string(trim(input('unit')));
			$heft     = safe_string(trim(input('heft')));
			$avatar     = safe_string(trim(input('avatar')));
			
			$MODEL_DATA = array(
				'w_id'  =>$w_id,
				'gift_title'   => $title,
				'gift_sort'    => $sort,
				'gift_status'  => $status,
				'gift_content'  => $content,
				'gift_money'  => $money,
				'gift_count'  => $count,
				'gift_unit'  => $unit,
				'gift_heft'  => $heft,
				'gift_img'   => $avatar,
			);
			
			$res = Db("gift")->where("gift_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改礼物信息成功!","success");
			}else{
				eyJson("ERROR:修改礼物信息失败！");
			}
		}
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_sort asc,w_id desc")->select();
		$this->assign("warehouse",$warehouse);
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$content     = trim(input('content'));
			$money     = safe_string(trim(input('money')));
			$w_id     = safe_string(trim(input('w_id')));
			$count     = safe_string(trim(input('count')));
			$unit     = safe_string(trim(input('unit')));
			$heft     = safe_string(trim(input('heft')));
			$avatar     = safe_string(trim(input('avatar')));
			
			$MODEL_DATA = array(
				'w_id'  =>$w_id,
				'gift_title'   => $title,
				'gift_sort'    => $sort,
				'gift_status'  => $status,
				'gift_content'  => $content,
				'gift_money'  => $money,
				'gift_count'  => $count,
				'gift_unit'  => $unit,
				'gift_heft'  => $heft,
				'gift_addtime' => date("Y-m-d H:i:s"),
				'gift_img'   => $avatar,
			);
			
			$res = Db("gift")->insert($MODEL_DATA);
			if($res){
				eyJson("添加礼物信息成功！","success");
			}else{
				eyJson("ERROR:添加礼物信息失败！");
			}
		}
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_sort asc,w_id desc")->select();
		$this->assign("warehouse",$warehouse);
		return view();
	}

}