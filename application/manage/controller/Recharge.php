<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Recharge extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("recharge");
		//增删改权限显示处理
		$recharge = Db("recharge")
			->field("r.*,p.pay_id as pay_ids,p.pay_title,u.*")
			->alias("r")
			->join("__USERS__ u","u.u_id = r.u_id","left")
			->join("__PAY__ p","p.pay_id = r.pay_id","left")
			->where("1 = 1")->order("r_id desc")->paginate(20);
		$page = $recharge->render();
		$this->assign('page', $page);
		$this->assign("recharge",$recharge);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("recharge")->where("r_id = {$id}")->delete();
			if($res){
				eyJson("删除充值记录成功！","success");
			}else{
				eyJson("ERROR：删除充值记录失败！");
			}
			
		}
	}	
	
	public function noshow()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("recharge");
		//增删改权限显示处理
		$recharge = Db("recharge")
			->field("r.*,p.pay_id as pay_ids,p.pay_title,u.*")
			->alias("r")
			->join("__USERS__ u","u.u_id = r.u_id","left")
			->join("__PAY__ p","p.pay_id = r.pay_id","left")
			->where("r_status = 1 or r_status = 4")->order("r_id desc")->paginate(20);
		$page = $recharge->render();
		$this->assign('page', $page);
		$this->assign("recharge",$recharge);
		return view();
	}
	
	public function edit()
	{
		$id = input("id");
		$recharge = Db("recharge")
			->alias("r")
			->join("__USERS__ u","u.u_id = r.u_id","left")
			//->join("__PAY__ p","p.pay_id = r.pay_id","left")
			->where("r.r_id = {$id}")->order("r_id desc")->find();
		if(Request::instance()->isAjax()){
			$content = safe_string(trim(input('content')));
			$status = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'r_content' => $content,
				'r_status'  => $status,
			);
			
			
			$res = Db("recharge")->where("r_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				if($status == 2){
					//修改用户积分，余额
					$credit = $recharge['r_money'] * $this->__CONFIG__['s_credit'];
					$credit = $recharge['u_credit'] + $credit;
					
					$balance = $recharge['r_money'] + $recharge['u_balance'];
					$USERS_DATA = array(
						'u_credit'  => $credit,
						'u_balance' => $balance,
					);
					Db("users")->where("u_phone = '{$recharge['u_phone']}'")->update($USERS_DATA);
					//修改用户积分，余额
					//写入积分日记
					$LOG_DATA = array(
						'u_id'        => $recharge['u_id'],
						'sub_id'      => $recharge['sub_id'],
						'crl_creadit' => $recharge['r_money'],
						'crl_balance' => $credit,
						'crl_addtime' => date("Y-m-d H:i:s"),
						'crl_content' => '银行卡充值',
					);
				
					Db("credit_logs")->insert($LOG_DATA);
					//写入积分日记
					write_acpital($recharge['u_id'],$recharge['sub_id'],1,$recharge['r_money'],"银行卡充值",1,1); //写入资金记录表
					//写入分销信息
					if($recharge['su_id']!=0){
						//为上级写入余额
						$su_users = Db("users")->where("u_id = '{$recharge['su_id']}'")->find(); //查询上级用户
						$yz_money   = $recharge['r_money'] * $this->__CONFIG__['s_commission_one'];
						$u_commission = $yz_money + $su_users['u_commission'];
						$USERS_DATA_1 = array(
							'u_commission' => $u_commission,
						);
						Db("users")->where("u_id = '{$recharge['su_id']}'")->update($USERS_DATA_1);
						//为上级写入余额
						write_acpital($recharge['su_id'],$recharge['sub_id'],1,$yz_money,"直推用户分润",3,2); //为上级写入资金记录表
					}
					//写入分销信息
				}
				eyJson("审核充值记录成功！","success");
			}else{
				eyJson("ERROR：审核充值记录失败！");
			}
			
		}
		
		$this->assign("info",$recharge);
		return view();
	}
	
	
	public function add()
	{
		
		if(Request::instance()->isAjax()){
			$phone   = safe_string(trim(input('phone')));
			$money   = safe_string(trim(input('money')));
			$content = safe_string(trim(input('content')));

			$users = Db("users")->where("u_phone = '{$phone}'")->find();
			if(empty($users)){
				eyJson("ERROR:用户不存在，无法充值！");
			}
			
			$MODEL_DATA = array(
				'u_id'          => $users['u_id'],
				'sub_id'        => $users['sub_id'],
				'r_money'       => $money,
				'r_addtime'     => date("Y-m-d H:i:s"),
				'r_status'      => 2,
				'r_pay_addtime' => date("Y-m-d H:i:s"),
				'pay_id'        => 0,
				'r_content'     => $content,
			);
		
			$res = Db("recharge")->insert($MODEL_DATA);
			if($res){
				//修改用户积分，余额
				$credit = $money * $this->__CONFIG__['s_credit'];
				$credit = $users['u_credit'] + $credit;
				
				$balance = $money + $users['u_balance'];
				$USERS_DATA = array(
					'u_credit'  => $credit,
					'u_balance' => $balance,
				);
				Db("users")->where("u_phone = '{$phone}'")->update($USERS_DATA);
				//修改用户积分，余额
				//写入积分日记
				$LOG_DATA = array(
					'u_id'        => $users['u_id'],
					'sub_id'      => $users['sub_id'],
					'crl_creadit' => $money,
					'crl_balance' => $credit,
					'crl_addtime' => date("Y-m-d H:i:s"),
					'crl_content' => '系统充值',
				);
			
				Db("credit_logs")->insert($LOG_DATA);
				//写入积分日记
				write_acpital($users['u_id'],$users['sub_id'],1,$money,"系统充值",1,1); //写入资金记录表
				//写入分销信息
				if($users['su_id']!=0){
					//为上级写入余额
					$su_users = Db("users")->where("u_id = '{$users['su_id']}'")->find(); //查询上级用户
					$yz_money   = $money * $this->__CONFIG__['s_commission_one'];
					$u_commission = $yz_money + $su_users['u_commission'];
					$USERS_DATA_1 = array(
						'u_commission' => $u_commission,
					);
					Db("users")->where("u_id = '{$users['su_id']}'")->update($USERS_DATA_1);
					//为上级写入余额
					
					write_acpital($users['su_id'],$users['sub_id'],1,$yz_money,"直推用户分润",3,2); //为上级写入资金记录表
				}
				//写入分销信息
				
				eyJson("充值成功！","success");
			}else{
				eyJson("ERROR:充值失败！");
			}
		}
		
		$user_group = Db("user_group")->where("ug_status = 1")->order("ug_credit asc,ug_id desc")->select();
		$this->assign("group",$user_group);
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_sort asc,w_id desc")->select();
		$this->assign("warehouse",$warehouse);
		return view();
	}

}