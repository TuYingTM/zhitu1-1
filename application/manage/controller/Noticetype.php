<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Noticetype extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("noticetype");
		//增删改权限显示处理
		$notice_type = Db("notice_type")->where("1 = 1")->order("nt_sort asc,nt_id desc")->select();
		$this->assign("notice_type",$notice_type);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}


			$res = Db("notice_type")->where("nt_id = {$id}")->delete();
			if($res){
				eyJson("删除公告分类信息成功！","success");
			}else{
				eyJson("ERROR：删除公告分类信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("notice_type")->where("nt_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'nt_title'   => $title,
				'nt_sort'    => $sort,
				'nt_status'  => $status,
			);
			
			$res = Db("notice_type")->where("nt_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改分类信息成功!","success");
			}else{
				eyJson("ERROR:修改分类信息失败！");
			}
		}
	
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'nt_title'   => $title,
				'nt_addtime' => date("Y-m-d H:i:s"),
				'nt_sort'    => $sort,
				'nt_status'  => $status,
			);
			
			$res = Db("notice_type")->insert($MODEL_DATA);
			if($res){
				eyJson("添加分类信息成功！","success");
			}else{
				eyJson("ERROR:添加分类信息失败！");
			}
		}
		return view();
	}

}