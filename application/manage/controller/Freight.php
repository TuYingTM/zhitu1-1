<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Freight extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("freight");
		//增删改权限显示处理
		$freight = Db("freight")
			->alias("f")
			->join("__SUBSTATION__ sub","sub.sub_id = f.sub_id","left")
			->join("__USER_GROUP__ ug","ug.ug_id = f.ug_id","left")
			->join("__WAREHOUSE__ w","w.w_id = f.w_id","left")
			->where("1 = 1")->order("f_id desc")->select();
		$this->assign("freight",$freight);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("freight")->where("f_id = {$id}")->delete();
			if($res){
				eyJson("删除运费模板成功！","success");
			}else{
				eyJson("ERROR：删除运费模板失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("freight")
				->alias("f")
				->join("__USER_GROUP__ ug","ug.ug_id = f.ug_id","left")
				->join("__WAREHOUSE__ w","w.w_id = f.w_id","left")
				->where("f.f_id = {$id}")->find();
		if(Request::instance()->isAjax()){
			$money      = safe_string(trim(input('money')));
			$status     = safe_string(trim(input('status')));
			$content     = safe_string(trim(input('content')));
			
			$MODEL_DATA = array(
				'f_money'  => $money,
				'f_status' => $status,
				'f_content' =>$content,
			);
			
			$res = Db("freight")->where("f_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改运费模板成功!","success");
			}else{
				eyJson("ERROR:修改运费模板失败！");
			}
		}
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		
		if(Request::instance()->isAjax()){
			$ug_id      = safe_string(trim(input('ug_id')));
			$w_id       = safe_string(trim(input('w_id')));
			$money      = safe_string(trim(input('money')));
			$status     = safe_string(trim(input('status')));
			$content     = safe_string(trim(input('content')));
			
			
			$freight = Db("freight")->where("sub_id = 0 and ug_id = {$ug_id} and w_id = {$w_id}")->find();
			if(!empty($freight)){
				eyJson("ERROR:此群组已添加了此仓库的运费模板，不能重复添加！");
			}
			
			$MODEL_DATA = array(
				'ug_id'   => $ug_id,
				'w_id' => $w_id,
				'f_addtime' => date("Y-m-d H:i:s"),
				'f_money'  => $money,
				'f_status' => $status,
				'f_content' =>$content,
			);
		
			$res = Db("freight")->insert($MODEL_DATA);
			if($res){
				eyJson("添加运费模板成功！","success");
			}else{
				eyJson("ERROR:添加运费模板失败！");
			}
		}
		
		$user_group = Db("user_group")->where("ug_status = 1")->order("ug_credit asc,ug_id desc")->select();
		$this->assign("group",$user_group);
		$warehouse = Db("warehouse")->where("w_status = 1")->order("w_sort asc,w_id desc")->select();
		$this->assign("warehouse",$warehouse);
		return view();
	}

}