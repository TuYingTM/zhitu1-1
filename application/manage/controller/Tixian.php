<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Tixian extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("tixian");
		//增删改权限显示处理
		$tixian = Db("tixian")
			->alias("tx")
			->join("__USERS__ u","u.u_id = tx.u_id","left")
			//->field("u.*,ug.*,su.u_phone as su_phone")
			->where("1 = 1")->order("tx_id desc")->paginate(20);
		$page = $tixian->render();
		$this->assign('page', $page);
		$this->assign("tixian",$tixian);
		return view();
	}
	
	public function users()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("tixian");
		//增删改权限显示处理
		$id = safe_string(trim(input('id')));
		$tixian = Db("tixian")
			->alias("tx")
			->join("__USERS__ u","u.u_id = tx.u_id","left")
			//->field("u.*,ug.*,su.u_phone as su_phone")
			->where("tx.u_id = {$id}")->order("tx_id desc")->paginate(20);
		$page = $tixian->render();
		$this->assign('page', $page);
		$this->assign("tixian",$tixian);
		return view();
	}
	
	
	public function untreated()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("tixian");
		//增删改权限显示处理
		$tixian = Db("tixian")
			->alias("tx")
			->join("__USERS__ u","u.u_id = tx.u_id","left")
			//->field("u.*,ug.*,su.u_phone as su_phone")
			->where("tx_status = 1")->order("tx_id desc")->paginate(20);
		$page = $tixian->render();
		$this->assign('page', $page);
		$this->assign("tixian",$tixian);
		return view();
	}	
	

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("tixian")->where("tx_id = {$id}")->delete();
			if($res){
				eyJson("删除提现信息成功！","success");
			}else{
				eyJson("ERROR：删除提现信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("tixian")->alias("tx")->join("__USERS__ u","u.u_id = tx.u_id","left")->where("tx_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$shcontent = safe_string(trim(input('shcontent')));
			$status    = safe_string(trim(input('status')));
			
			
			$MODEL_DATA = array(
				'tx_status'  => $status,
				'tx_sh_addtime' => date("Y-m-d H:i:s"),
				'tx_sh_content' => $shcontent,
			);

			$res = Db("tixian")->where("tx_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				if($status==3) //通过
				{
					if($info['tx_type']==1) //用金提现
					{
						WriteUser_commission($info['u_id'],$info['sub_id'],$info['tx_money'],4);
					}else{ //分站赢利提现
						WriteUser_substation($info['u_id'],$info['sub_id'],$info['tx_money'],4);
					}
				}else{ //拒绝
					if($info['tx_type']==1) //用金提现
					{
						WriteUser_commission($info['u_id'],$info['sub_id'],$info['tx_money'],3);
						write_acpital($info['u_id'],$info['sub_id'],1,$info['tx_money'],"提现失败退回",5,2);
					}else{ //分站赢利提现
						WriteUser_substation($info['u_id'],$info['sub_id'],$info['tx_money'],3);
						write_acpital($info['u_id'],$info['sub_id'],1,$info['tx_money'],"提现失败退回",5,3);
					}
				}
				eyJson("审核用户提现成功!","success");
			}else{
				eyJson("ERROR:审核用户提现失败！");
			}
		}
		$this->assign("info",$info);
		return view();
	}	

}