<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Task extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("task");
		//增删改权限显示处理
		$task = Db("Task")
				->alias("t")
				->join("__TASK_TYPE__ tt","tt.tt_id  = t.tt_id","left")
				->join("__TASK_PLATFORM__ tp","tp.tpf_id = t.tpf_id","left")
				->join("__USERS__ u","u.u_id = t.u_id","left")
				->where("1 = 1")->order("t.t_id desc")->paginate(10);
		$page = $task->render();
		$this->assign('page', $page);
		$this->assign("task",$task);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			$taskinfo = Db("Task")
				->alias("t")
				->join("__USERS__ u","u.u_id = t.u_id","left")
				->where("t.t_id = {$id}")->find();
			$res = Db("task")->where("t_id = {$id}")->delete();
			if($res){
				//退回余额
				//扣除用户余额
				$balance = $taskinfo['u_balance'] + $taskinfo['t_y_money'];
				$USERS_DATA = array(
					'u_balance' => $balance,
				);
				Db("users")->where("u_id = {$taskinfo['u_id']}")->update($USERS_DATA);
				//扣除用户余额
				write_acpital($taskinfo['u_id'],$taskinfo['sub_id'],1,$taskinfo['t_y_money'],"任务退回",7,1); //写入资金记录表
				//通回余额
				Db("task_order")->where("t_id = {$id}")->delete();
				eyJson("删除任务信息成功！","success");
			}else{
				eyJson("ERROR：删除任务信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));

		$info = Db("Task")
				->alias("t")
				->join("__TASK_TYPE__ tt","tt.tt_id  = t.tt_id","left")
				->join("__TASK_PLATFORM__ tp","tp.tpf_id = t.tpf_id","left")
				->join("__USERS__ u","u.u_id = t.u_id","left")
				->where("t.t_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$tpf_id  = safe_string(trim(input('tpf_id')));
			$tt_id   = safe_string(trim(input('tt_id')));
			$dpname  = safe_string(trim(input('dpname')));
			$url     = safe_string(trim(input('url')));
			$editor  = trim(input('editor'));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'tpf_id'    => $tpf_id,
				'tt_id'     => $tt_id,
				't_url'     => $url,
				't_desc'    => $editor,
				't_status'  => $status,
				't_dp_name' => $dpname,
			);
			
			$res = Db("task")->where("t_id = {$id}")->update($MODEL_DATA);
			if($res){
				eyJson("修改任务信息成功!","success");
			}else{
				eyJson("ERROR:修改任务信息失败！");
			}
		}
		$task_type = Db("task_type")->where("tt_status = 1")->select();
		$task_platform = Db("task_platform")->where("tpf_status = 1")->select();
		$this->assign("task_type",$task_type);
		$this->assign("task_platform",$task_platform);
		$this->assign("info",$info);
		return view();
	}	
	
}