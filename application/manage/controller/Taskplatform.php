<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Taskplatform extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("taskplatform");
		//增删改权限显示处理
		$task_platform = Db("Task_platform")->where("1 = 1")->order("tpf_id desc")->select();
		$this->assign("task_platform",$task_platform);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("task_platform")->where("tpf_id = {$id}")->delete();
			if($res){
				eyJson("删除任务平台信息成功！","success");
			}else{
				eyJson("ERROR：删除任务平台信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("task_platform")->where("tpf_id = {$id}")->find();
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'tpf_title'   => $title,
				'tpf_status'  => $status,
			);
			
			$res = Db("task_platform")->where("tpf_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改任务平台信息成功!","success");
			}else{
				eyJson("ERROR:修改任务平台信息失败！");
			}
		}
	
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array( 
				'tpf_title'   => $title,
				'tpf_addtime' => date("Y-m-d H:i:s"),
				'tpf_status'  => $status,
			);
			
			$res = Db("task_platform")->insert($MODEL_DATA);
			if($res){
				eyJson("添加任务平台信息成功！","success");
			}else{
				eyJson("ERROR:添加任务平台信息失败！");
			}
		}
		return view();
	}

}