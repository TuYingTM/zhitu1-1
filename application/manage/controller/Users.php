<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Users extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("users");
		//增删改权限显示处理
		$users = Db("users")
			->alias("u")
			->join("__USER_GROUP__ ug","ug.ug_id = u.ug_id","left")
			->join("__USERS__ su","su.u_id = u.su_id","left")
			->field("u.*,ug.*,su.u_phone as su_phone")
			->where("1 = 1")->order("u_id desc")->paginate(20);
		$page = $users->render();
		$this->assign('page', $page);
		$this->assign("users",$users);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			
			$users = Db("users")->where("u_id = '{$id}'")->find();
			if(empty($users)){
				eyJson("ERROR:会员不存在，无法删除！");
			}
			
			if($users['u_sub']==2){
				eyJson("ERROR:会员是分站管理员，请先删除会员站点！");
			}

			$res = Db("users")->where("u_id = {$id}")->delete();
			if($res){
				Db("tixian")->where("u_id = {$id}")->delete();//删除提现信息
				Db("credit_logs")->where("u_id = {$id}")->delete();//删除积分明细信息
				Db("capital_logs")->where("u_id = {$id}")->delete();//删除资金明细信息
				Db("recharge")->where("u_id = {$id}")->delete(); //删除充值记录信息
				Db("order")->where("u_id = {$id}")->delete(); //删除订单信息
				eyJson("删除用户信息成功！","success");
			}else{
				eyJson("ERROR：删除用户信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("users")->where("u_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$password = safe_string(trim(input('password')));
			$status     = safe_string(trim(input('status')));
			
			$credit     = safe_string(trim(input('credit')));
			$balance     = safe_string(trim(input('balance')));
			$commission     = safe_string(trim(input('commission')));
			$substation     = safe_string(trim(input('substation')));
			
			
			$balance_freeze     = safe_string(trim(input('balance_freeze')));
			$commission_freeze     = safe_string(trim(input('commission_freeze')));
			$substation_freeze     = safe_string(trim(input('substation_freeze')));
			
			
			
			
			$ug_id     = safe_string(trim(input('ug_id')));
			
			$send_phone     = safe_string(trim(input('send_phone')));
			$send_name     = safe_string(trim(input('send_name')));
			
			$bank_title     = safe_string(trim(input('bank_title')));
			$bank_content     = safe_string(trim(input('bank_content')));
			$bank_card     = safe_string(trim(input('bank_card')));
			$bank_name     = safe_string(trim(input('bank_name')));
			$zfb_title     = safe_string(trim(input('zfb_title')));
			$zfb_name     = safe_string(trim(input('zfb_name')));
			

			
			$MODEL_DATA = array(
				'u_status'  => $status,
				'u_credit' => $credit,
				'u_balance' => $balance,
				'u_commission' => $commission,
				'u_substation' => $substation,
				'u_balance_freeze' => $balance_freeze,
				'u_commission_freeze' => $commission_freeze,
				'u_substation_freeze' => $substation_freeze,
				'ug_id' =>$ug_id,
				'u_send_phone' =>$send_phone,
				'u_send_name' =>$send_name,
				
				'u_bank_title' =>$bank_title,
				'u_bank_content' =>$bank_content,
				'u_bank_card' =>$bank_card,
				'u_bank_name' =>$bank_name,
				'u_zfb_title' =>$zfb_title,
				'u_zfb_name' =>$zfb_name,
			);
			
			if(!empty($password)){
				$MODEL_DATA['u_password'] = md5($password);
			}
			
			$res = Db("users")->where("u_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改用户成功!","success");
			}else{
				eyJson("ERROR:修改用户失败！");
			}
		}
		$list = Db("user_group")->where("ug_status = 1")->select();
		$this->assign("list",$list);
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$phone      = safe_string(trim(input('phone')));
			$password = md5(safe_string(trim(input('password'))));
			$status     = safe_string(trim(input('status')));
			
			$promo = getRandNumber(0,999999,8);
			
			if($this->__CONFIG__['ug_id'] == 0){
				eyJson("ERROR:未设置默认会员群组，无法添加用户！");
			}
			
			$users_info = Db("users")->where("u_phone = '{$phone}'")->find();
			if(!empty($users_info)){
				eyJson("ERROR:此用户已存在，无法重复添加！");
			}
			
			$MODEL_DATA = array(
				'u_phone'   => $phone,
				'u_password' => $password,
				'u_addtime' => date("Y-m-d H:i:s"),
				'u_regip' => '127.0.0.1', //后台创建
				'u_status'  => $status,
				'ug_id' => $this->__CONFIG__['ug_id'],
				'u_promo' => $promo,
			);
		
			$res = Db("users")->insert($MODEL_DATA);
			if($res){
				eyJson("添加用户成功！","success");
			}else{
				eyJson("ERROR:添加用户失败！");
			}
		}
		return view();
	}

}