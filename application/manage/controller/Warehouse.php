<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Warehouse extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("warehouse");
		//增删改权限显示处理
		$warehouse = Db("warehouse")->where("1 = 1")->order("w_sort asc,w_id desc")->select();
		$this->assign("warehouse",$warehouse);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}


			$res = Db("warehouse")->where("w_id = {$id}")->delete();
			if($res){
				eyJson("删除仓库信息成功！","success");
			}else{
				eyJson("ERROR：删除仓库信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("warehouse")->where("w_id = {$id}")->find();
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$content     = safe_string(trim(input('content')));
			$address     = safe_string(trim(input('address')));
			$pf_id     = safe_string(trim(input('pf_id')));
			
			
			$w_fahuo     = safe_string(trim(input('w_fahuo')));
			$w_cp_code     = safe_string(trim(input('w_cp_code')));
			$w_agent_id     = safe_string(trim(input('w_agent_id')));
			$w_sender_name     = safe_string(trim(input('w_sender_name')));
			$w_sender_phone     = safe_string(trim(input('w_sender_phone')));
			$w_sender_province     = safe_string(trim(input('w_sender_province')));
			$w_sender_city     = safe_string(trim(input('w_sender_city')));
			$w_sender_district     = safe_string(trim(input('w_sender_district')));
			
			
			$w_api_name     = safe_string(trim(input('w_api_name')));
			$w_api_appid     = safe_string(trim(input('w_api_appid')));
			$w_api_appuser     = safe_string(trim(input('w_api_appuser')));
			$w_api_appkey     = safe_string(trim(input('w_api_appkey')));

			
			$MODEL_DATA = array(
				'pf_id'  =>$pf_id,
				'w_title'   => $title,
				'w_sort'    => $sort,
				'w_status'  => $status,
				'w_content'  => $content,
				'w_address'  => $address,
				'w_fahuo'  =>$w_fahuo,
				'w_cp_code'  =>$w_cp_code,
				'w_agent_id'  =>$w_agent_id,
				'w_sender_name'  =>$w_sender_name,
				'w_sender_phone'  =>$w_sender_phone,
				'w_sender_province'  =>$w_sender_province,
				'w_sender_city'  =>$w_sender_city,
				'w_sender_district'  =>$w_sender_district,
				
				'w_api_name'  =>$w_api_name,
				'w_api_appid'  =>$w_api_appid,
				'w_api_appuser'  =>$w_api_appuser,
				'w_api_appkey'  =>$w_api_appkey,
			);
			
			$res = Db("warehouse")->where("w_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改仓库信息成功!","success");
			}else{
				eyJson("ERROR:修改仓库信息失败！");
			}
		}
		$platform = Db("platform")->where("pf_status = 1")->order("pf_sort asc,pf_id desc")->select();
		$this->assign("platform",$platform);
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$content     = safe_string(trim(input('content')));
			$address     = safe_string(trim(input('address')));
			$pf_id     = safe_string(trim(input('pf_id')));
			
			$w_fahuo     = safe_string(trim(input('w_fahuo')));
			$w_cp_code     = safe_string(trim(input('w_cp_code')));
			$w_agent_id     = safe_string(trim(input('w_agent_id')));
			$w_sender_name     = safe_string(trim(input('w_sender_name')));
			$w_sender_phone     = safe_string(trim(input('w_sender_phone')));
			$w_sender_province     = safe_string(trim(input('w_sender_province')));
			$w_sender_city     = safe_string(trim(input('w_sender_city')));
			$w_sender_district     = safe_string(trim(input('w_sender_district')));
			
			$w_api_name     = safe_string(trim(input('w_api_name')));
			$w_api_appid     = safe_string(trim(input('w_api_appid')));
			$w_api_appuser     = safe_string(trim(input('w_api_appuser')));
			$w_api_appkey     = safe_string(trim(input('w_api_appkey')));

			$MODEL_DATA = array(
				'pf_id'  =>$pf_id,
				'w_title'   => $title,
				'w_sort'    => $sort,
				'w_status'  => $status,
				'w_content'  => $content,
				'w_address'  => $address,
				'w_addtime' => date("Y-m-d H:i:s"),
				'w_fahuo'  =>$w_fahuo,
				'w_cp_code'  =>$w_cp_code,
				'w_agent_id'  =>$w_agent_id,
				'w_sender_name'  =>$w_sender_name,
				'w_sender_phone'  =>$w_sender_phone,
				'w_sender_province'  =>$w_sender_province,
				'w_sender_city'  =>$w_sender_city,
				'w_sender_district'  =>$w_sender_district,
				
				'w_api_name'  =>$w_api_name,
				'w_api_appid'  =>$w_api_appid,
				'w_api_appuser'  =>$w_api_appuser,
				'w_api_appkey'  =>$w_api_appkey,
			);
			
			$res = Db("warehouse")->insert($MODEL_DATA);
			if($res){
				eyJson("添加仓库信息成功！","success");
			}else{
				eyJson("ERROR:添加仓库信息失败！");
			}
		}
		$platform = Db("platform")->where("pf_status = 1")->order("pf_sort asc,pf_id desc")->select();
		$this->assign("platform",$platform);
		return view();
	}

}