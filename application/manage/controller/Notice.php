<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Notice extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("notice");
		//增删改权限显示处理
		$notice = Db("notice")->alias("n")->join("__NOTICE_TYPE__ nt","n.nt_id = nt.nt_id","left")->where("1 = 1")->order("n_id desc")->paginate(20);
		$page = $notice->render();
		$this->assign('page', $page);
		$this->assign("notice",$notice);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}


			$res = Db("notice")->where("n_id = {$id}")->delete();
			if($res){
				eyJson("删除公告信息成功！","success");
			}else{
				eyJson("ERROR：删除公告信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("notice")->where("n_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$nt_id      = safe_string(trim(input('nt_id')));
			$title = safe_string(trim(input('title')));
			$status     = safe_string(trim(input('status')));
			$editor     = trim(input('editor'));
			$read     = safe_string(trim(input('read')));
			
			$MODEL_DATA = array(
				'nt_id'   => $nt_id,
				'n_title'    => $title,
				'n_status'  => $status,
				'n_content' => $editor,
				'n_read' => $read,
			);
			
			$res = Db("notice")->where("n_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改公告信息成功!","success");
			}else{
				eyJson("ERROR:修改公告信息失败！");
			}
		}
		$noticetype = Db("notice_type")->where("nt_status = 1")->order("nt_sort asc,nt_id desc")->select();
		$this->assign("noticetype",$noticetype);
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){

			$nt_id      = safe_string(trim(input('nt_id')));
			$title = safe_string(trim(input('title')));
			$status     = safe_string(trim(input('status')));
			$editor     = trim(input('editor'));
			$read     = safe_string(trim(input('read')));
			
			$MODEL_DATA = array(
				'nt_id'   => $nt_id,
				'n_addtime' => date("Y-m-d H:i:s"),
				'n_title'    => $title,
				'n_status'  => $status,
				'n_content' => $editor,
				'n_read' => $read,
			);
			
			$res = Db("notice")->insert($MODEL_DATA);
			if($res){
				eyJson("添加公告信息成功！","success");
			}else{
				eyJson("ERROR:添加公告信息失败！");
			}
		}
		$noticetype = Db("notice_type")->where("nt_status = 1")->order("nt_sort asc,nt_id desc")->select();
		$this->assign("noticetype",$noticetype);
		return view();
	}

}