<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Manage extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("manage");
		//增删改权限显示处理
		$list = Db("manage")
			->alias("m")
			->join("__MANAGE_GROUP__ g","g.g_id = m.g_id","left")
			->order("m_manage asc,m_status asc,m_id asc")->select();
		$this->assign("list",$list);
		return view();
	}
	
	public function manageedit()
	{
		$id = safe_string(trim(input('id')));
		$uinfo = Db("manage")->where("m_id = '{$id}'")->find();
		
		if(Request::instance()->isAjax()){
			$password = safe_string(trim(input('password')));
			$status   = safe_string(trim(input('status')));
			$group    = safe_string(trim(input('group')));

			if(empty($group)){
				eyJson($this->__LANGCONFIG__['manage_user_empty_group']);
			}
			
			if(empty($uinfo)){
				eyJson($this->__LANGCONFIG__['manage_user_empty']);
			}
			
			$USER_DATA = array(
				'm_status'   => $status,
				'g_id'       => $group,
			);
			
			if(!empty($password)){
				$USER_DATA['m_password'] = md5($password);
			}
			
			$res = Db("manage")->where("m_id = '{$id}'")->update($USER_DATA);
			
			if($res){
				eyJson($this->__LANGCONFIG__['manage_user_edit_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['manage_user_edit_error']);
			}
			
			
		}
		
		
		$this->assign("info",$uinfo);
		$list = Db("manage_group")->where("g_status = 1")->select();
		$this->assign("list",$list);
		return view();
	}
	
	public function managedel()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			
			$uinfo = Db("manage")->where("m_id = '{$id}'")->find();
			if(empty($uinfo)){
				eyJson($this->__LANGCONFIG__['manage_user_empty']);
			}
			
			if($uinfo['m_manage']==1){
				eyJson($this->__LANGCONFIG__['manage_user_yes_manage']);
			}

			$res = Db("manage")->where("m_id = {$id}")->delete();
			
			if($res){
				eyJson($this->__LANGCONFIG__['manage_user_del_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['manage_user_del_error']);
			}
			
		}
	}
	
	public function manageadd()
	{
		if(Request::instance()->isAjax()){
			$username = safe_string(trim(input('username')));
			$password = safe_string(trim(input('password')));
			$status   = safe_string(trim(input('status')));
			$group    = safe_string(trim(input('group')));
			
			if(empty($username)){
				eyJson($this->__LANGCONFIG__['manage_user_empty_username']);
			}
			
			if(empty($password)){
				eyJson($this->__LANGCONFIG__['manage_user_empty_password']);
			}

			if(empty($group)){
				eyJson($this->__LANGCONFIG__['manage_user_empty_group']);
			}
			
			
			$uinfo = Db("manage")->where("m_username = '{$username}'")->find();
			if(!empty($uinfo)){
				eyJson($this->__LANGCONFIG__['manage_user_repeat_username']);
			}
			
			$USER_DATA = array(
				'm_username' => $username,
				'm_password' => md5($password),
				'm_status'   => $status,
				'g_id'       => $group,
				'm_addtime'  => date("Y-m-d H:i:s"),
			);
			
			$res = Db("manage")->insert($USER_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['manage_user_add_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['manage_user_add_error']);
			}
		}
		$list = Db("manage_group")->where("g_status = 1")->select();
		$this->assign("list",$list);
		return view();
	}
	
	public function group()
	{
		//增删改权限显示处理
		$this->__POWER__ = array(
			'groupadd' => array(
				'action'   => "groupadd",
				'power' => false,
			),
			'groupedit' => array(
				'action'   => "groupedit",
				'power' => false,
			),
			'groupdel' => array(
				'action'   => "groupdel",
				'power' => false,
			),
		);
		$this->Is_PowerShow("manage");
		//增删改权限显示处理		
		
		$manage_table = config('database.prefix')."manage";
		$list = Db("manage_group")->alias("g")->field("g.*,(select count(m_id) from {$manage_table} m where m.g_id = g.g_id) as g_sumcount")->select();
		$this->assign("list",$list);
		return view();
	}
	
	public function groupdel()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			
			$ginfo = Db("manage_group")->where("g_id = '{$id}'")->find();
			if(empty($ginfo)){
				eyJson($this->__LANGCONFIG__['manage_group_empty']);
			}
			
			if($ginfo['g_manage']==1){
				eyJson($this->__LANGCONFIG__['manage_group_yes_manage']);
			}
			
			//查找此群下面是否存在用户如果存在无法删除
			$uinfo = Db("manage")->where("g_id = {$id}")->find();
			if(!empty($uinfo)){
				eyJson($this->__LANGCONFIG__['manage_group_noempty_user']);
			}
	
			$res = Db("manage_group")->where("g_id = {$id}")->delete();
			
			if($res){
				eyJson($this->__LANGCONFIG__['manage_group_del_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['manage_group_del_error']);
			}
			
		}
	}
	
	public function groupadd()
	{
		if(Request::instance()->isAjax()){
			$title  = safe_string(trim(input('title')));
			$status = safe_string(trim(input('status')));
			$power  = safe_string(trim(input('power')));
			
			
			if(empty($title)){
				eyJson($this->__LANGCONFIG__['manage_group_empty_title']);
			}
			
			$ginfo = Db("manage_group")->where("g_title = '{$title}'")->find();
			if(!empty($ginfo)){
				eyJson($this->__LANGCONFIG__['manage_group_repeat_title']);
			}
			
			$GROUP_DATA = array(
				'g_title'   => $title,
				'g_status'  => $status,
				'g_power'   => $power,
				'g_addtime' => date("Y-m-d H:i:s"),
			);
			
			$res = Db("manage_group")->insert($GROUP_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['manage_group_add_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['manage_group_add_error']);
			}
		}
		$list = Db("model")->where("1 = 1")->select();
		$this->assign("list",$list);
		return view();
	}
	
	public function groupedit()
	{
		
		$id = safe_string(trim(input('id')));
		$uinfo = Db("manage_group")->where("g_id = '{$id}'")->find();
		if(Request::instance()->isAjax()){
			$title  = safe_string(trim(input('title')));
			$status = safe_string(trim(input('status')));
			$power  = safe_string(trim(input('power')));
			
			if(empty($title)){
				eyJson($this->__LANGCONFIG__['manage_group_empty_title']);
			}
			
			$ginfo = Db("manage_group")->where("g_title = '{$title}'")->find();
			if(!empty($ginfo)){
				if($uinfo['g_id'] != $ginfo['g_id']){
					eyJson($this->__LANGCONFIG__['manage_group_repeat_title']);
				}
			}
			
			$GROUP_DATA = array(
				'g_title'   => $title,
				'g_power'   => $power,
				'g_status'  => $status,
			);
			
			$res = Db("manage_group")->where("g_id = '{$id}'")->update($GROUP_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['manage_group_edit_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['manage_group_edit_error']);
			}
		}
		$list = Db("model")->where("1 = 1")->select();
		$this->assign("list",$list);
		$this->assign("info",$uinfo);
		return view();
	}

}