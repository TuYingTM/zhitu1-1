<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Substation extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("substation");
		//增删改权限显示处理
		$substation = Db("substation")
			->alias("sub")
			->field("sub.*,u.u_phone")
			->join("__USERS__ u","u.u_id = sub.u_id","left")
			->where("1 = 1")
			->order("sub.sub_id desc")
			->select();
		$this->assign("substation",$substation);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			
			$substation = Db("substation")->where("sub_id = {$id}")->find();
			
			$res = Db("substation")->where("sub_id = {$id}")->delete();
			if($res){
				//更换用户相关信息
				$USER_DATA = array(
					'u_sub' => 1,
				);
				Db("users")->where("u_id = '{$substation['u_id']}'")->update($USER_DATA);
				Db("tixian")->where("sub_id = {$id}")->delete();//删除提现信息
				Db("credit_logs")->where("sub_id = {$id}")->delete();//删除积分明细信息
				Db("capital_logs")->where("sub_id = {$id}")->delete();//删除资金明细信息
				Db("users")->where("sub_id = {$id}")->delete();//删除分站会员
				Db("notice")->where("sub_id = {$id}")->delete();//删除分站公告
				Db("freight")->where("sub_id = {$id}")->delete();//删除分站运费
				Db("recharge")->where("sub_id = {$id}")->delete(); //删除充值记录信息
				Db("order")->where("sub_id = {$id}")->delete(); //删除订单信息
				eyJson("删除分站信息成功！","success");
			}else{
				eyJson("ERROR：删除分站信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("substation")->where("sub_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$status     = safe_string(trim(input('status')));
			$title     = safe_string(trim(input('title')));
			$domain     = safe_string(trim(input('domain')));
			
			$credit     = safe_string(trim(input('credit')));
			$fastmoney     = safe_string(trim(input('fastmoney')));
			$commission_one     = safe_string(trim(input('commission_one')));
			$commission_two     = safe_string(trim(input('commission_two')));
			$commission_three     = safe_string(trim(input('commission_three')));
			
			$sub_qq1     = safe_string(trim(input('sub_qq1')));
			$sub_qq2     = safe_string(trim(input('sub_qq2')));
			
			
			$MODEL_DATA = array(
				'sub_qq1' => $sub_qq1,
				'sub_qq2' => $sub_qq2,
				'sub_domain' => $domain,
				'sub_webtitle'   => $title,
				'sub_credit' => $credit,
				'sub_fastmoney' => $fastmoney,
				'sub_commission_one' => $commission_one,
				'sub_commission_two' => $commission_two,
				'sub_commission_three' => $commission_three,
				'sub_status'  => $status,
			);
			
			$res = Db("substation")->where("sub_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改分站信息成功!","success");
			}else{
				eyJson("ERROR:修改分站信息失败！");
			}
		}

		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$phone      = safe_string(trim(input('phone')));
			$status     = safe_string(trim(input('status')));
			$title     = safe_string(trim(input('title')));
			$domain     = safe_string(trim(input('domain')));
			
			//查询会员是否存在
			//判断会员是否禁用
			//判断会员是否已经有了分站
			$users = Db("users")->where("u_phone = '{$phone}'")->find();
			if(empty($users)){
				eyJson("ERROR:会员不存在，请输入正确的号码！");
			}
			
			if($users['u_status']==2){
				eyJson("ERROR:会员已被禁用，无法成为分站管理员！");
			}
			
			if($users['u_sub']==2){
				eyJson("ERROR:会员已是分站管理员，无法重新添加！");
			}
			
			
			$MODEL_DATA = array(
				'u_id' => $users['u_id'],
				'sub_domain' => $domain,
				'sub_webtitle'   => $title,
				'sub_addtime' => date("Y-m-d H:i:s"),
				'sub_credit' => $this->__CONFIG__['s_credit'],
				'sub_fastmoney' => $this->__CONFIG__['s_fastmoney'],
				'sub_commission_one' => $this->__CONFIG__['s_commission_one'],
				'sub_commission_two' => $this->__CONFIG__['s_commission_two'],
				'sub_commission_three' => $this->__CONFIG__['s_commission_three'],
				'sub_status'  => $status,
			);
			
			$res = Db("substation")->insert($MODEL_DATA);
			if($res){
				//$subid = Db('substation')->getLastInsID();
				//更换用户相关信息
				$USER_DATA = array(
					'u_sub' => 2,
				);
				Db("users")->where("u_id = '{$users['u_id']}'")->update($USER_DATA);
				eyJson("添加分站信息成功！","success");
			}else{
				eyJson("ERROR:添加分站信息失败！");
			}
		}

		return view();
	}

}