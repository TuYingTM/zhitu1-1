<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Model extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("model");
		//增删改权限显示处理
		$NavContent = Db("model")->where("mo_sid = 0")->order("mo_show asc,mo_sort asc")->select();
		if(!empty($NavContent)){
			$NavCount = count($NavContent);
			for($i=0;$i<$NavCount;$i++){
				$_NavContent_ = Db("model")->where("mo_sid = {$NavContent[$i]['mo_id']}")->order("mo_sort asc")->select();
				$NavContent[$i]['offline'] = $_NavContent_;
			}
		}
		
		

		$this->assign("model",$NavContent);
		return view();
	}
	
	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}
			
			$uinfo = Db("model")->where("mo_id = '{$id}'")->find();
			if(empty($uinfo)){
				eyJson($this->__LANGCONFIG__['model_empty']);
			}
			

			$res = Db("model")->where("mo_id = {$id}")->delete();
			
			if($res){
				eyJson($this->__LANGCONFIG__['model_del_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['model_del_error']);
			}
			
		}
	}
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("model")->where("mo_id = {$id}")->find();
		
		
		if(Request::instance()->isAjax()){
			$sid        = safe_string(trim(input('sid')));
			$title      = safe_string(trim(input('title')));
			$shorttitle = safe_string(trim(input('shorttitle')));
			$controller = safe_string(trim(input('controller')));
			$action     = safe_string(trim(input('action')));
			$sort       = safe_string(trim(input('sort')));
			$show       = safe_string(trim(input('show')));
			$navplace   = safe_string(trim(input('navplace')));
			
			if(empty($title)){
				eyJson($this->__LANGCONFIG__['model_empty_title']);
			}
			
			if(empty($shorttitle)){
				eyJson($this->__LANGCONFIG__['model_empty_shorttitle']);
			}

			if(empty($controller)){
				eyJson($this->__LANGCONFIG__['model_empty_controller']);
			}
			
			if(empty($action)){
				eyJson($this->__LANGCONFIG__['model_empty_action']);
			}
			
			
			$minfo = Db("model")->where("mo_title = '{$title}' or (mo_controller = '{$action}' and mo_action = '{$controller}')")->find();
			if(!empty($minfo)){
				if($info['mo_id'] != $minfo['mo_id']){
					eyJson($this->__LANGCONFIG__['model_repeat']);
				}
			}
			
			$MODEL_DATA = array(
				'mo_sid'        => $sid,
				'mo_title'      => $title,
				'mo_shorttitle' => $shorttitle,
				'mo_controller' => $controller,
				'mo_action'     => $action,
				'mo_show'       => $show,
				'mo_navplace'   => $navplace,
				'mo_sort'       => $sort,
			);
			
			$res = Db("model")->where("mo_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['model_edit_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['model_edit_error']);
			}
		}
		

		$list = Db("model")->where("mo_sid = 0")->select();
		$this->assign("list",$list);
		$this->assign("info",$info);
		return view();
	}
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$sid        = safe_string(trim(input('sid')));
			$title      = safe_string(trim(input('title')));
			$shorttitle = safe_string(trim(input('shorttitle')));
			$controller = safe_string(trim(input('controller')));
			$action     = safe_string(trim(input('action')));
			$sort       = safe_string(trim(input('sort')));
			$show       = safe_string(trim(input('show')));
			$navplace   = safe_string(trim(input('navplace')));
			
			if(empty($title)){
				eyJson($this->__LANGCONFIG__['model_empty_title']);
			}
			
			if(empty($shorttitle)){
				eyJson($this->__LANGCONFIG__['model_empty_shorttitle']);
			}

			if(empty($controller)){
				eyJson($this->__LANGCONFIG__['model_empty_controller']);
			}
			
			if(empty($action)){
				eyJson($this->__LANGCONFIG__['model_empty_action']);
			}
			
			
			$minfo = Db("model")->where("mo_title = '{$title}' or (mo_controller = '{$action}' and mo_action = '{$controller}')")->find();
			if(!empty($minfo)){
				eyJson($this->__LANGCONFIG__['model_repeat']);
			}
			
			$MODEL_DATA = array(
				'mo_sid'        => $sid,
				'mo_title'      => $title,
				'mo_shorttitle' => $shorttitle,
				'mo_controller' => $controller,
				'mo_action'     => $action,
				'mo_show'       => $show,
				'mo_navplace'   => $navplace,
				'mo_sort'       => $sort,
			);
			
			$res = Db("model")->insert($MODEL_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['model_add_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['model_add_error']);
			}
		}
		$list = Db("model")->where("mo_sid = 0")->select();
		$this->assign("list",$list);
		return view();
	}


}