<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Order extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("order");
		//增删改权限显示处理
		$order = Db("order")
			->alias("o")
			->join("__SUBSTATION__ s","o.sub_id = s.sub_id","left") //所属分站
			->join("__USERS__ u","o.u_id = u.u_id","left") //所属用户
			->join("__GIFT__ g","o.gift_id = g.gift_id ","left") //所属礼物
			->join("__WAREHOUSE__ w","o.w_id = w.w_id","left") //所属仓库
			->join("__PLATFORM__ pf","o.pf_id = pf.pf_id","left") //所属电商平台
			->where("1 = 1")->order("o.o_status asc,o.o_id desc")->paginate(20);
		$page = $order->render();
		$this->assign('page', $page);
		$this->assign("order",$order);
		return view();
	}
	
	//导入
	public function import()
	{
		if(Request::instance()->isAjax()){
			$avatar = input("avatar");
			$xlsPath = _PATH_ . $avatar; //指定要读取的exls路径
			import('PHPExcel.PHPExcel');
			import('PHPExcel.PHPExcel/IOFactory');
			$inputFileType = \PHPExcel_IOFactory::identify($xlsPath);
			$xlsReader = \PHPExcel_IOFactory::createReader($inputFileType);
			$xlsReader->setReadDataOnly(true);
			$xlsReader->setLoadSheetsOnly(true);
			$Sheets = $xlsReader->load($xlsPath);
			//开始读取
			$Sheet = $Sheets->getSheet(0)->toArray();
			$count = count($Sheet)-1;
			if($count > 0){
				for($i=1;$i<=$count;$i++){
					if(!empty($Sheet[$i][7])){
						$DATA = array(
							'o_status'      => 2,
							'o_kddh_sncode' => $Sheet[$i][7],
						);
						Db("order")->where("o_sncode = '".$Sheet[$i][0]."'")->update($DATA);
					}
				}
				eyJson("批量发货成功！","success");
			}else{
				eyJson("此订单表格中不存在内容！");
			}				
			
		}
		return view();
	}
	
	//导出
	public function export()
	{
		$order = Db("order")
			->alias("o")
			->join("__SUBSTATION__ s","o.sub_id = s.sub_id","left") //所属分站
			->join("__USERS__ u","o.u_id = u.u_id","left") //所属用户
			->join("__GIFT__ g","o.gift_id = g.gift_id ","left") //所属礼物
			->join("__WAREHOUSE__ w","o.w_id = w.w_id","left") //所属仓库
			->join("__PLATFORM__ pf","o.pf_id = pf.pf_id","left") //所属电商平台
			->where("o.o_status = 1")->order("o.o_status asc,o.o_id desc")->select();
		
		if(empty($order)){
			echo "没有未发的订单，无法导出";
			exit;
		}
			
		import('PHPExcel.PHPExcel/IOFactory');
		import('PHPExcel.PHPExcel');
		import('PHPExcel.PHPExcel/Writer/Excel2007');
		$header_arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M', 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$template = _PATH_.'/template.xls';          //使用模板  
		$objPHPExcel = \PHPExcel_IOFactory::load($template);     //加载excel文件,设置模板  
		$objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);  //设置保存版本格式  
		if(empty($filename)) $filename = time();
		//接下来就是写数据到表格里面去  
		$objActSheet = $objPHPExcel->getActiveSheet();  
		
		$count = count($order);
		$for_i = 2;
		for($i=0;$i<$count;$i++){
			$objActSheet->setCellValue('A'.$for_i,$order[$i]['o_sncode']);
			$objActSheet->setCellValue('B'.$for_i,$order[$i]['o_sho_name']);
			$objActSheet->setCellValue('C'.$for_i,$order[$i]['o_sho_address']);
			$objActSheet->setCellValue('D'.$for_i,$order[$i]['o_sho_phone']);
			$objActSheet->setCellValue('E'.$for_i,$order[$i]['pf_title']);
			$objActSheet->setCellValue('F'.$for_i,$order[$i]['gift_title']);
			$objActSheet->setCellValue('G'.$for_i,$order[$i]['w_title']);
			$for_i = $for_i + 1;
		}
			
		
		header("Pragma: public");  
		header("Expires: 0");  
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");  
		header("Content-Type:application/force-download");  
		header("Content-Type:application/vnd.ms-execl");  
		header("Content-Type:application/octet-stream");  
		header("Content-Type:application/download");;  
		header('Content-Disposition:attachment;filename="'.$filename.'.xls"');  
		header("Content-Transfer-Encoding:binary");  
		$objWriter->save('php://output');  
	}
	
	public function fadan()
	{
		
		if(Request::instance()->isAjax()){

			$id = safe_string(trim(input('id')));
			$warehouse = Db("order")
			->alias("o")
			->join("__SUBSTATION__ s","o.sub_id = s.sub_id","left") //所属分站
			->join("__USERS__ u","o.u_id = u.u_id","left") //所属用户
			->join("__GIFT__ g","o.gift_id = g.gift_id ","left") //所属礼物
			->join("__WAREHOUSE__ w","o.w_id = w.w_id","left") //所属仓库
			->join("__PLATFORM__ pf","o.pf_id = pf.pf_id","left") //所属电商平台
			->where("o.o_id = {$id}")->order("o_id desc")->find();
			
			
			$DATA = array();
			
			if($warehouse['w_fahuo'] == 2){ //快宝发货
				import('Kuaidihelp.Kuaidihelp');
				$ss = new \Kuaidihelp;
				$ss->appid           = $this->__CONFIG__['s_kuaidihelp_appid'];
				$ss->appkey          = $this->__CONFIG__['s_kuaidihelp_appkey'];
				$ss->agent_id        = $warehouse['w_agent_id'];
				$ss->cp_code         = $warehouse['w_cp_code'];
				$ss->goods_name      = $warehouse['gift_title'];
				$ss->sender_phone    = $warehouse['w_sender_phone'];
				$ss->sender_name     = $warehouse['w_sender_name'];
				$ss->sender_province = $warehouse['w_sender_province'];
				$ss->sender_city     = $warehouse['w_sender_city'];
				$ss->sender_district = $warehouse['w_sender_district'];
				$ss->sender_detail   = $warehouse['w_address'];	
				
				//分割地址
					$address = $warehouse['o_sho_address'];
					preg_match('/(.*?(省|自治区|北京市|天津市))/', $address, $matches);
					if (count($matches) > 1) {
						$province = $matches[count($matches) - 2];
						$address = str_replace($province, '', $address);
					}
					preg_match('/(.*?(市|自治州|地区|区划|县))/', $address, $matches);
					if (count($matches) > 1) {
						$city = $matches[count($matches) - 2];
						$address = str_replace($city, '', $address);
					}
					preg_match('/(.*?(区|县|镇|乡|街道))/', $address, $matches);
					if (count($matches) > 1) {
						$area = $matches[count($matches) - 2];
						$address = str_replace($area, '', $address);
					}		
					//分割地址
					$ss->tid                = $warehouse['o_sncode'];
					$ss->recipient_phone    = $warehouse['o_sho_phone'];
					$ss->recipient_name     = $warehouse['o_sho_name'];
					$ss->recipient_province = $province;
					$ss->recipient_city     = $city;
					$ss->recipient_district = $area;
					$ss->recipient_detail   = $address;
					$ss->GetSheet();
					$ss->GetSheetError();
					if($ss->error == "ok"){
						$DATA['o_status']      = 2;
						$DATA['o_kddh_sncode'] = $ss->error_msg;
						$DATA['o_content']     = "";
					}else{
						$DATA['o_status']      = 1;
						$DATA['o_kddh_sncode'] = "";
						$DATA['o_content'] = "快宝提示：".$ss->error_msg;
					}
			}
			

			
			$res = Db("order")->where("o_id = '{$id}'")->update($DATA);
			
			if($res){
				eyJson("补单成功","success");
			}else{
				eyJson("补单失败，请重试");
			}
			
			
		}
		
		
		
		
		$this->assign("info",$info);
		return view();
	}
	
	public function noindex()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("order");
		//增删改权限显示处理
		$order = Db("order")
			->alias("o")
			->join("__SUBSTATION__ s","o.sub_id = s.sub_id","left") //所属分站
			->join("__USERS__ u","o.u_id = u.u_id","left") //所属用户
			->join("__GIFT__ g","o.gift_id = g.gift_id ","left") //所属礼物
			->join("__WAREHOUSE__ w","o.w_id = w.w_id","left") //所属仓库
			->join("__PLATFORM__ pf","o.pf_id = pf.pf_id","left") //所属电商平台
			->where("o.o_status = 1")->order("o.o_status asc,o.o_id desc")->paginate(20);
		$page = $order->render();
		$this->assign('page', $page);
		$this->assign("order",$order);
		return view();
	}	

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("order")->where("o_id = {$id}")->delete();
			if($res){
				eyJson("删除订单信息成功！","success");
			}else{
				eyJson("ERROR：删除订单信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		
		
		if(Request::instance()->isAjax()){
			$o_kddh_sncode = safe_string(trim(input('o_kddh_sncode')));
			
			
			$DATA = array(
				'o_kddh_sncode' => $o_kddh_sncode,
				'o_status' => 2,
			);
			
			$res = Db("order")->where("o_id = '{$id}'")->update($DATA);
			
			if($res){
				eyJson("快递提交成功","success");
			}else{
				eyJson("快递提交失败");
			}
			
			
		}
		
		
		
		$info = Db("order")
			->alias("o")
			->join("__SUBSTATION__ s","o.sub_id = s.sub_id","left") //所属分站
			->join("__USERS__ u","o.u_id = u.u_id","left") //所属用户
			->join("__GIFT__ g","o.gift_id = g.gift_id ","left") //所属礼物
			->join("__WAREHOUSE__ w","o.w_id = w.w_id","left") //所属仓库
			->join("__PLATFORM__ pf","o.pf_id = pf.pf_id","left") //所属电商平台
			->where("o.o_id = {$id}")->order("o_id desc")->find();
		$this->assign("info",$info);
		return view();
	}
}