<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Platform extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("platform");
		//增删改权限显示处理
		$platform = Db("platform")->where("1 = 1")->order("pf_sort asc,pf_id desc")->select();
		$this->assign("platform",$platform);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}


			$res = Db("platform")->where("pf_id = {$id}")->delete();
			if($res){
				eyJson("删除电商平台信息成功！","success");
			}else{
				eyJson("ERROR：删除电商平台信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("platform")->where("pf_id = {$id}")->find();
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$content     = safe_string(trim(input('content')));
			
			$MODEL_DATA = array(
				'pf_title'   => $title,
				'pf_sort'    => $sort,
				'pf_status'  => $status,
				'pf_content'  => $content,
			);
			
			$res = Db("platform")->where("pf_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改电商平台信息成功!","success");
			}else{
				eyJson("ERROR:修改电商平台信息失败！");
			}
		}
	
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$content     = safe_string(trim(input('content')));
			
			$MODEL_DATA = array(
				'pf_title'   => $title,
				'pf_sort'    => $sort,
				'pf_status'  => $status,
				'pf_content'  => $content,
				'pf_addtime' => date("Y-m-d H:i:s"),
			);
			
			$res = Db("platform")->insert($MODEL_DATA);
			if($res){
				eyJson("添加电商平台信息成功！","success");
			}else{
				eyJson("ERROR:添加电商平台信息失败！");
			}
		}
		return view();
	}

}