<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Capitallogs extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("capitallogs");
		//增删改权限显示处理
		$capitallogs = Db("capital_logs")
				->alias("cl")
				->join("__USERS__ u","cl.u_id = u.u_id","left")
				->where("1 = 1")->order("cl_id desc")->paginate(20);
		$page = $capitallogs->render();
		$this->assign('page', $page);
		$this->assign("capitallogs",$capitallogs);
		return view();
	}
	
	public function users()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("capitallogs");
		//增删改权限显示处理
		$id = safe_string(trim(input('id')));
		$capitallogs = Db("capital_logs")
				->alias("cl")
				->join("__USERS__ u","cl.u_id = u.u_id","left")
				->where("cl.u_id = {$id}")->order("cl.cl_id desc")->paginate(20);
		$page = $capitallogs->render();
		$this->assign('page', $page);
		$this->assign("capitallogs",$capitallogs);
		return view();
	}	

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("capital_logs")->where("cl_id = {$id}")->delete();
			if($res){
				eyJson("删除资金明细信息成功！","success");
			}else{
				eyJson("ERROR：删除资金明细失败！");
			}
			
		}
	}	

}