<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Setup extends Base{
	
	public function system()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("setup");
		//增删改权限显示处理
		if(Request::instance()->isAjax()){
			$s_webtitle         = safe_string(trim(input('s_webtitle')));
			$s_domain    = domain_Http_Clear(safe_string(trim(input('s_domain'))));
			$s_copyright      = safe_string(trim(input('s_copyright')));
			$s_credit     = safe_string(trim(input('s_credit')));
			$s_smsusername = safe_string(trim(input('s_smsusername')));
			$s_smspassword   = safe_string(trim(input('s_smspassword')));
			$s_fastmoney         = safe_string(trim(input('s_fastmoney')));
			$s_commission_one    = safe_string(trim(input('s_commission_one')));
			$s_commission_two      = safe_string(trim(input('s_commission_two')));
			$s_commission_three     = safe_string(trim(input('s_commission_three')));
			$s_tixian_min = safe_string(trim(input('s_tixian_min')));
			$s_smsswitch   = safe_string(trim(input('s_smsswitch')));
			$bank         = safe_string(trim(input('bank')));
			$wx    = safe_string(trim(input('wx')));
			$zfb      = safe_string(trim(input('zfb')));
			$ugid      = safe_string(trim(input('ugid')));
			
			$s_qq1      = safe_string(trim(input('s_qq1')));
			$s_qq2      = safe_string(trim(input('s_qq2')));
			
			$s_kuaidihelp_appid      = safe_string(trim(input('s_kuaidihelp_appid')));
			$s_kuaidihelp_appkey      = safe_string(trim(input('s_kuaidihelp_appkey')));
			
			
			$s_qrcode_title      = safe_string(trim(input('s_qrcode_title')));
			$avatar      = safe_string(trim(input('avatar')));
			
			$s_bank_open      = safe_string(trim(input('s_bank_open')));
			$s_bank_title      = safe_string(trim(input('s_bank_title')));
			$s_bank_content      = safe_string(trim(input('s_bank_content')));
			$s_bank_username      = safe_string(trim(input('s_bank_username')));
			$s_bank_card      = safe_string(trim(input('s_bank_card')));

			$CONFIG_DATA = array(
				's_qrcode_title' => $s_qrcode_title,
				's_qrcode' => $avatar,
				'ug_id' => $ugid,
				's_webtitle'         => $s_webtitle,
				's_domain'    => $s_domain,
				's_copyright'      => $s_copyright,
				's_credit'     => $s_credit,
				's_smsusername' => $s_smsusername,
				's_smspassword'   => $s_smspassword,
				's_fastmoney'   => $s_fastmoney,
				's_commission_one'   => $s_commission_one,
				's_commission_two'   => $s_commission_two,
				's_commission_three'   => $s_commission_three,
				's_tixian_min'   => $s_tixian_min,
				's_smsswitch'   => $s_smsswitch,
				's_tongdao_bank'   => $bank,
				's_tongdao_wx'   => $wx,
				's_tongdao_zfb'   => $zfb,
				's_qq1'   => $s_qq1,
				's_qq2'   => $s_qq2,
				's_kuaidihelp_appid'   => $s_kuaidihelp_appid,
				's_kuaidihelp_appkey'   => $s_kuaidihelp_appkey,
				
				's_bank_open'   => $s_bank_open,
				's_bank_title'   => $s_bank_title,
				's_bank_content'   => $s_bank_content,
				's_bank_username'   => $s_bank_username,
				's_bank_card'   => $s_bank_card,
			);
			
			$res = Db("systems")->where("1=1")->update($CONFIG_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['system_config_update_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['system_config_update_error']);
			}
			
		}
		$user_group = Db("user_group")->where("ug_status = 1")->order("ug_sort desc,ug_id desc")->select();
		$this->assign("group",$user_group);
		return view();
	}
	

}