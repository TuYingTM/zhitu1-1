<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Index extends Base{
	
	public function index()
	{
		return view();
	}

	public function home()
	{
		echo "首页位置";
		exit;
	}

	public function login()
	{
		if(Request::instance()->isAjax()){
			
			$username = safe_string(trim(input('username')));
			$password = safe_string(trim(input('password')));
			
			if(empty($username)){
				eyJson($this->__LANGCONFIG__['username_empty']);
			}
			
			if(empty($password)){
				eyJson($this->__LANGCONFIG__['password_empty']);
			}
				
			$password = md5($password);
			$userinfo = Db("manage")->alias("m")->where("m.m_username = '{$username}' and m.m_password = '{$password}'")
									->join("__MANAGE_GROUP__ g","g.g_id = m.g_id")
									->find();
		
			if(empty($userinfo)){
				eyJson($this->__LANGCONFIG__['username_nuil']);
			}else{
				if($userinfo['m_status']==2){
					eyJson($this->__LANGCONFIG__['username_status_no']);
				}else{
					
					$_broswer    = GetBroswer();
					$_os         = GetOS();
					$_ip         = Request::instance()->ip();
					$_time       = date('Y-m-d H:i:s');
					$_session_id = session_id();
	
					//对相关信息进行更新操作
					$USER_DATA = array(
						'm_count'        => $userinfo['m_count'] + 1,
						'm_last_ip'      => $userinfo['m_this_ip'],
						'm_last_time'    => $userinfo['m_this_time'],
						'm_this_ip'      => $_ip,
						'm_this_time'    => $_time,
						'm_session_id'   => $_session_id, //本次登录的session_id值
						'm_this_broswer' => $_broswer,
						'm_this_os'      => $_os,
					);

					Db("manage")->where("m_id = {$userinfo['m_id']}")->update($USER_DATA);

					//对写入登录成功日志表
					$USER_LOG = array(
						'ml_addtime'    => $_time,
						'm_id'          => $userinfo['m_id'],
						'ml_ip'         => $_ip,
						'ml_session_id' => $_session_id,
						'ml_broswer'    => $_broswer,
						'ml_os'         => $_os,
						'ml_type'       => 1,
					);
					Db("manage_log")->insert($USER_LOG);
					//对SESSION进入记录操作
					session("m_group",$userinfo['g_title']);
					session("m_username",$username);
					session("m_id",$userinfo['m_id']);
					session("m_broswer",$userinfo['m_this_broswer']);
					session("m_os",$userinfo['m_this_os']);
					session("m_manage",$userinfo['m_manage']);
					eyJson($this->__LANGCONFIG__['username_login_ok'],"success");
				}
			}
		}
		
		return view();
	}
	
	public function outlogin()
	{
		//记录日志操作
		//对写入登录成功日志表
		$_broswer    = GetBroswer();
		$_os         = GetOS();
		$_ip         = Request::instance()->ip();
		$_time       = date('Y-m-d H:i:s');
		$_session_id = session_id();
		$USER_LOG = array(
			'ml_addtime'    => $_time,
			'm_id'          => session("m_id"),
			'ml_ip'         => $_ip,
			'ml_session_id' => $_session_id,
			'ml_broswer'    => $_broswer,
			'ml_os'         => $_os,
			'ml_type'       => 2,
		);
		Db("manage_log")->insert($USER_LOG);
		//记录日志操作
		session("m_group",null);
		session("m_username",null);
		session("m_id",null);
		session("m_broswer",null);
		session("m_os",null);
		session("m_manage",null);
		eyJson("退出成功！","success",url("index/login"));
	}
}