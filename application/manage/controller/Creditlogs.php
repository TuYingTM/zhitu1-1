<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Creditlogs extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("creditlogs");
		//增删改权限显示处理
		$creditlogs = Db("credit_logs")
				->alias("crl")
				->join("__USERS__ u","crl.u_id = u.u_id","left")
				->where("1 = 1")->order("crl_id desc")->paginate(20);
		$page = $creditlogs->render();
		$this->assign('page', $page);
		$this->assign("creditlogs",$creditlogs);
		return view();
	}
	
	public function users()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("creditlogs");
		//增删改权限显示处理
		$id = safe_string(trim(input('id')));
		$creditlogs = Db("credit_logs")
				->alias("crl")
				->join("__USERS__ u","crl.u_id = u.u_id","left")
				->where("crl.u_id = {$id}")->order("crl.crl_id desc")->paginate(20);
		$page = $creditlogs->render();
		$this->assign('page', $page);
		$this->assign("creditlogs",$creditlogs);
		return view();
	}	

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("credit_logs")->where("crl_id = {$id}")->delete();
			if($res){
				eyJson("删除积分明细信息成功！","success");
			}else{
				eyJson("ERROR：删除积分明细失败！");
			}
			
		}
	}	

}