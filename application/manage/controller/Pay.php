<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Pay extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("pay");
		//增删改权限显示处理
		$pay = Db("pay")->where("1 = 1")->order("pay_sort asc,pay_id desc")->select();
		$this->assign("pay",$pay);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}


			$res = Db("pay")->where("pay_id = {$id}")->delete();
			if($res){
				eyJson("删除充值通道信息成功！","success");
			}else{
				eyJson("ERROR：删除充值通道信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("pay")->where("pay_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$appid     = safe_string(trim(input('appid')));
			$key     = safe_string(trim(input('key')));
			$content     = safe_string(trim(input('content')));
			$tag     = safe_string(trim(input('tag')));
			
			$MODEL_DATA = array(
				'pay_title'   => $title,
				'pay_sort'    => $sort,
				'pay_status'  => $status,
				'pay_tag' => $tag,
				'pay_appid'  => $appid,
				'pay_key'  => $key,
				'pay_content'  => $content,
			);
			
			$res = Db("pay")->where("pay_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改充值通道信息成功!","success");
			}else{
				eyJson("ERROR:修改充值通道信息失败！");
			}
		}
	
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$sort = safe_string(trim(input('sort')));
			$status     = safe_string(trim(input('status')));
			$appid     = safe_string(trim(input('appid')));
			$key     = safe_string(trim(input('key')));
			$content     = safe_string(trim(input('content')));
			$tag     = safe_string(trim(input('tag')));
			
			$MODEL_DATA = array(
				'pay_title'   => $title,
				'pay_sort'    => $sort,
				'pay_status'  => $status,
				'pay_tag' => $tag,
				'pay_appid'  => $appid,
				'pay_key'  => $key,
				'pay_content'  => $content,
			);
			
			$res = Db("pay")->insert($MODEL_DATA);
			if($res){
				eyJson("添加充值通道信息成功！","success");
			}else{
				eyJson("ERROR:添加充值通道信息失败！");
			}
		}
		return view();
	}

}