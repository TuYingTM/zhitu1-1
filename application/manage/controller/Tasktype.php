<?php
namespace app\manage\controller;
use app\manage\controller\Base;
use think\Session;
use think\Request;
class Tasktype extends Base{
	
	public function index()
	{
		//增删改权限显示处理
		$this->Is_PowerShow("tasktype");
		//增删改权限显示处理
		$task_type = Db("task_type")->where("1 = 1")->order("tt_id desc")->select();
		$this->assign("task_type",$task_type);
		return view();
	}

	public function del()
	{
		if(Request::instance()->isAjax()){
			$id = safe_string(trim(input('id')));
			if(empty($id)){
				eyJson($this->__LANGCONFIG__['common_error_id']);
			}

			$res = Db("task_type")->where("tt_id = {$id}")->delete();
			if($res){
				eyJson("删除任务分类信息成功！","success");
			}else{
				eyJson("ERROR：删除任务分类信息失败！");
			}
			
		}
	}	
	
	public function edit()
	{
		$id = safe_string(trim(input('id')));
		$info = Db("task_type")->where("tt_id = {$id}")->find();
		
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array(
				'tt_title'   => $title,
				'tt_status'  => $status,
			);
			
			$res = Db("task_type")->where("tt_id = '{$id}'")->update($MODEL_DATA);
			if($res){
				eyJson("修改任务分类信息成功!","success");
			}else{
				eyJson("ERROR:修改任务分类信息失败！");
			}
		}
	
		$this->assign("info",$info);
		return view();
	}	
	
	
	public function add()
	{
		if(Request::instance()->isAjax()){
			$title      = safe_string(trim(input('title')));
			$status     = safe_string(trim(input('status')));
			
			$MODEL_DATA = array( 
				'tt_title'   => $title,
				'tt_addtime' => date("Y-m-d H:i:s"),
				'tt_status'  => $status,
			);
			
			$res = Db("task_type")->insert($MODEL_DATA);
			if($res){
				eyJson("添加任务分类信息成功！","success");
			}else{
				eyJson("ERROR:添加任务分类信息失败！");
			}
		}
		return view();
	}

}