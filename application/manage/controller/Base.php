<?php
namespace app\manage\controller;
use think\Controller;
use think\Db;
use think\Session;
use think\Request;

class Base extends Controller{
	
	public $__CONFIG__;
	public $__MODELFILE__;
	public $__LANGUAGE__ = "Zh-cn";
	public $__LANGCONFIG__;
	public $__NAV__;
	public $__POWER__ = array(
			'add' => array(
				'action'   => "add",
				'power' => false,
			),
			'edit' => array(
				'action'   => "edit",
				'power' => false,
			),
			'del' => array(
				'action'   => "del",
				'power' => false,
			),
		); //用于增删改权限判断使用
	public $__USERINFO__ = array(); //用户信息保存，用于权限时使用不用多次调用
	public $__PAGECOUNT__ = 15; //分页数
	
	
	
	public function _initialize(){
		require_once _PATH_ . "/lang/lang_".$this->__LANGUAGE__.".php"; //语言库
		$this->__LANGCONFIG__ = $LANG; //语言包
		$request = Request::instance();
		$this->__CONFIG__ = GetConfig(); //读取配置信息
		//$read_domain   = domain_Http_Clear($request->domain()); //当前域名
		//$config_domain = domain_Http_Clear($this->__CONFIG__['c_domain_manage']); //后台管理域名
		//if($read_domain != $config_domain){
		//	echo $this->__LANGCONFIG__['domain_error'];
		//	exit;
		//}
		$this->__MODELFILE__ = strtolower($request->controller()."/".$request->action());

		//http://127.0.0.1/ey_manage/manage/group.html
		if($this->__MODELFILE__ != "index/login" and $this->__MODELFILE__ != "index/outlogin" and $this->__MODELFILE__ != "user/logs" and $this->__MODELFILE__ != "user/password"){
			if(empty(session("m_id"))){
				eyAlert($this->__LANGCONFIG__['username_no_login']);
			}else{
				$POWER_ID = $this->Is_Manage($request->controller(),$request->action());
				if($POWER_ID == 2){
					eyAlert($this->__LANGCONFIG__['username_power_no']);
				}elseif($POWER_ID == 3){
					eyAlert($this->__LANGCONFIG__['username_status_no']);
				}elseif($POWER_ID == 4){
					session("m_group",null);
					session("m_username",null);
					session("m_id",null);
					session("m_broswer",null);
					session("m_os",null);
					session("m_manage",null);
					eyAlert($this->__LANGCONFIG__['username_session_id']);
				}
			}
		}elseif($this->__MODELFILE__ == "user/logs" or $this->__MODELFILE__ == "user/password"){
			//个人中心，所有人都可以进入，无权限判断，只限判断是否登录
			if(empty(session("m_id"))){
				eyAlert($this->__LANGCONFIG__['username_no_login']);
			}
			$POWER_ID = $this->Is_Manage($request->controller(),$request->action());
			if($POWER_ID == 4){
				session("m_group",null);
				session("m_username",null);
				session("m_id",null);
				session("m_broswer",null);
				session("m_os",null);
				session("m_manage",null);
				eyAlert($this->__LANGCONFIG__['username_session_id']);
			}
		}elseif($this->__MODELFILE__ == "index/login"){
			if(!empty(session("m_id"))){
				eyAlert($this->__LANGCONFIG__['username_login_ok'],"success",url("index/index"));
			}
		}
		
		$this->assign("language",$this->__LANGCONFIG__);
		$this->assign("config",$this->__CONFIG__); 
	}
	/*
	用于处理增删改显示按键权限
	*/
	
	public function Is_PowerShow($controller)
	{
		$controller = strtolower($controller);
		if($this->__USERINFO__['m_manage']==1){
			//超级管理员，跳过权限处理
			foreach($this->__POWER__ as $name){
				$this->__POWER__[$name['action']]['power'] = true;
			};
		}else{
			$__power_info__ = Db("model")->where("mo_controller = '{$controller}'")->select(); //查出此控制器所有方法
			if(empty($__power_info__)){
				//表示库中没有模块，全部权限不通过
				foreach($this->__POWER__ as $name){
					$this->__POWER__[$name['action']]['power'] = false;
				};
			}else{
				//库中有权限进行对比
				foreach($this->__POWER__ as $action){
					foreach($__power_info__ as $model){
						if(strtolower($model['mo_action']) == strtolower($action['action'])){
							$this->__POWER__[$action['action']]['power'] = true;
						}
					};
				};
			}
		}
		$this->assign("ps",$this->__POWER__);
	}
	
	/*
	判断是否已经登录系统
	*/
	public function Is_Manage($controller,$action)
	{
		/*
		1表示通过，2表示权限不通过，3表示用户已被禁用，4表示用户在别处登录
		*/
		$g_user = Db("manage")
			->field("m.m_manage,m.m_session_id,m.m_status,g.g_power")
			->alias("m")
			->join("__MANAGE_GROUP__ g","g.g_id = m.g_id")
			//->join("__MODEL__ mo","mo.mo_id in(g.g_power)")
			->where("m.m_id = ".session("m_id"))
			->find();
		$this->__USERINFO__ = $g_user;
		
		
		
		if($g_user['m_session_id']!=session_id()){
			return 4;
		}
		
		if($this->__MODELFILE__ == "user/logs" or $this->__MODELFILE__ == "user/password"){
			$this->NavPlace(1,$g_user['g_power'],$g_user['m_manage']); //调用导航
			return 1;
		}

		if($g_user['m_manage']==1){
			$this->NavPlace(1,$g_user['g_power'],$g_user['m_manage']); //调用导航
			return 1;
		}elseif($g_user['m_status']==2){
			return 3;
		}else{
			$g_model = Db("model")->field("mo_id")->where("mo_controller = '".strtolower($controller)."' and mo_action = '".strtolower($action)."'")->find();
			if(empty($g_model)){
				return 2;
			}else{
				
				$power = explode(",",$g_user['g_power']);
				
				if(in_array($g_model['mo_id'],$power)){
					$this->NavPlace(1,$g_user['g_power'],$g_user['m_manage']); //调用导航
					return 1;
				}else{
					return 2;
				}
			}
		}
	}
	
	/*
	输出顶部导航内容
	$place 为导航位置标识，可自定义与数据库中一至调用
	$power 为权限标识，判断是否有此类权限，有的话就显示出来
	$manage 为超级管理员标识，1为超级管理员不受权限控制
	*/
	public function NavPlace($place=1,$power="",$manage=2){
		$NavContent = Db("model")->where("mo_sid = 0 and mo_show = 1 and mo_navplace = {$place}")->order("mo_sort asc")->select();
		if(!empty($NavContent)){
			$power = explode(",",$power);
			$NavCount = count($NavContent);
			for($i=0;$i<$NavCount;$i++){
				if($manage==1){
					$_NavContent_ = Db("model")->where("mo_sid = {$NavContent[$i]['mo_id']} and mo_show = 1")->order("mo_sort asc")->select();
					$NavContent[$i]['offline'] = $_NavContent_;
				}else{
					if(in_array($NavContent[$i]['mo_id'],$power)){
						$_NavContent_ = Db("model")->where("mo_sid = {$NavContent[$i]['mo_id']} and mo_show = 1")->order("mo_sort asc")->select();
						$NavContent[$i]['offline'] = $_NavContent_;
					}else{
						unset($NavContent[$i]);
						continue;
					}
				}
				
				//处理action标识显示
				if($this->__MODELFILE__ == strtolower($NavContent[$i]['mo_controller']."/".$NavContent[$i]['mo_action'])){
					$NavContent[$i]['action'] = 1;
				}else{
					$NavContent[$i]['action'] = 2;
				}
				
				//处理二级分类显示
				if(empty($NavContent[$i]['offline'])){
					$NavContent[$i]['offline_status'] = 1;
				}else{
					$NavContent[$i]['offline_status'] = 2;
				}
			}
		}
		$this->__NAV__ = $NavContent;
		$this->assign("navplace_".$place,$this->__NAV__); 
	}
	

	
}
