<?php
namespace app\substation\controller;
use app\substation\controller\Base;
use think\Session;
use think\Request;
class User extends Base{
	
	public function password()
	{
		if(Request::instance()->isAjax()){
			$oldpassword = safe_string(trim(input('oldpassword')));
			$password    = safe_string(trim(input('password')));
			$endpassword = safe_string(trim(input('endpassword')));

			if(empty($oldpassword)){
				eyJson($this->__LANGCONFIG__['password_old_empty']);
			}
			
			if(empty($password)){
				eyJson($this->__LANGCONFIG__['password_new_empty']);
			}
			
			if(empty($endpassword)){
				eyJson($this->__LANGCONFIG__['password_end_empty']);
			}
			
			
			if($oldpassword == $password){
				eyJson($this->__LANGCONFIG__['password_old_and_new']);
			}
			
			
			if($endpassword != $password){
				eyJson($this->__LANGCONFIG__['password_new_no_end']);
			}
			
			$oldpassword = md5($oldpassword);
			$userinfo = Db("users")->where("u_id = ".session("sub_u_id")." and u_password = '{$oldpassword}'")->find();
			if(empty($userinfo)){
				eyJson($this->__LANGCONFIG__['password_error_old']);
			}
			
			
			$USER_DATA = array(
				'u_password' => md5($password),
			);
			
			$res = Db("users")->where("u_id = ".session("sub_u_id"))->update($USER_DATA);
			if($res){
				eyJson($this->__LANGCONFIG__['password_update_success'],"success");
			}else{
				eyJson($this->__LANGCONFIG__['password_update_error']);
			}
			
		}
		return view();
	}
	
}