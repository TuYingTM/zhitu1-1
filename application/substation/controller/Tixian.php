<?php
namespace app\substation\controller;
use app\substation\controller\Base;
use think\Session;
use think\Request;
class Tixian extends Base{
	
	public function index()
	{
		$tixian = Db("tixian")
			->alias("tx")
			->join("__USERS__ u","u.u_id = tx.u_id","left")
			//->field("u.*,ug.*,su.u_phone as su_phone")
			->where("tx.".$this->__SUBID_WHERE__)->order("tx_id desc")->select();
		$this->assign("tixian",$tixian);
		return view();
	}
}