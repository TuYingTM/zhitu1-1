<?php
namespace app\substation\controller;
use think\Controller;
use think\Db;
use think\Session;
use think\Request;

class Base extends Controller{
	
	public $__CONFIG__;
	public $__MODELFILE__;
	public $__LANGUAGE__ = "Zh-cn";
	public $__LANGCONFIG__;
	public $__PAGECOUNT__ = 15; //分页数
	public $__SUBID__;
	public $__SUBID_WHERE__;
	
	public function _initialize(){
		require_once _PATH_ . "/lang/lang_".$this->__LANGUAGE__.".php"; //语言库
		$this->__LANGCONFIG__ = $LANG; //语言包
		$request = Request::instance();
		$this->__CONFIG__ = GetConfig(); //读取配置信息
		$this->__MODELFILE__ = strtolower($request->controller()."/".$request->action());
		if($this->__MODELFILE__ != "index/login"){
			if(empty(session("sub_u_id"))){
				eyAlert($this->__LANGCONFIG__['username_no_login']);
			}else{
				$this->__SUBID__ = session("sub_id");
				$this->__SUBID_WHERE__ = "sub_id = ".$this->__SUBID__;
				$this->assign("subid",$this->__SUBID__);
			}
		}
		
		$this->assign("language",$this->__LANGCONFIG__);
		$this->assign("config",$this->__CONFIG__); 
	}
}
