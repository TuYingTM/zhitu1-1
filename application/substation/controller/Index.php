<?php
namespace app\substation\controller;
use app\substation\controller\Base;
use think\Session;
use think\Request;
class Index extends Base{
	
	public function index()
	{
		return view();
	}

	public function login()
	{
		if(Request::instance()->isAjax()){
			
			$username = safe_string(trim(input('username')));
			$password = safe_string(trim(input('password')));
			
			if(empty($username)){
				eyJson($this->__LANGCONFIG__['username_empty']);
			}
			
			if(empty($password)){
				eyJson($this->__LANGCONFIG__['password_empty']);
			}
			
			$password = md5($password);
			$userinfo = Db("users")->where("u_sub = 2 and u_phone = '{$username}' and u_password = '{$password}'")->find();
			
			if(empty($userinfo)){
				eyJson($this->__LANGCONFIG__['username_nuil']);
			}else{
				if($userinfo['u_status']==2){
					eyJson($this->__LANGCONFIG__['username_status_no']);
				}else{
					$substation = DB("substation")->where("u_id = {$userinfo['u_id']}")->find();
					if($substation['sub_status']==2){
						eyJson("您的分站已被禁用，请与管理员联系！");
					}else{
						$_ip         = Request::instance()->ip();
						$_time       = date('Y-m-d H:i:s');
						//对相关信息进行更新操作
						$USER_DATA = array(
							'u_count'        => $userinfo['u_count'] + 1,
							'u_last_time'    => $userinfo['u_this_time'],
							'u_last_ip'      => $userinfo['u_this_ip'],
							'u_this_ip'      => $_ip,
							'u_this_time'    => $_time,
						);
						Db("users")->where("u_id = {$userinfo['u_id']} and u_sub = 2")->update($USER_DATA);
						//对SESSION进入记录操作
						//查出站点ID值
						
						session("sub_username",$username);
						session("sub_u_id",$userinfo['u_id']);
						session("sub_id",$substation['sub_id']);
						eyJson($this->__LANGCONFIG__['username_login_ok'],"success");
					}
				}
			}
		}
		
		return view();
	}
	
	public function outlogin()
	{
		//记录日志操作
		session("sub_username",null);
		session("sub_u_id",null);
		session("sub_id",null);
		eyJson("退出成功！","success",url("index/login"));
	}
}