<?php
/*
易用软件
easily use soft
============================
微信：s67754457
Q  Q：1270597112
============================
快宝打印接口类 v1.0.0
============================

18625272562
yangyang520

*/
//if (!defined('Kuaidihelp_ROOT')) {
//   define('Kuaidihelp_ROOT', dirname(__FILE__) . '/');
//}

class Kuaidihelp
{
	public $hostUrl = "https://kop.kuaidihelp.com/api";
	public $Method = array(
		'GetSheet' => "cloud.print.do", //获取电子面单号
	);
	public $appid = ""; 
	public $appkey = "";
	public $agent_id = ""; //目标云打印机的访问密钥，在电脑端快宝云打印程序的“绑定”->“访问密钥”菜单项下查看；快宝盒子的访问密钥查看方法参考产品说明
	public $cp_code = ""; //快递品牌
	public $tid = ""; //任务ID，建议用订单号之类的唯一标识，提交和回调时将返回该字段, 后台申请单号时也将使用该字段作订单号
	public $goods_name = ""; //物品名(可选)
	
	//发件人信息
	public $sender_phone = "";
	public $sender_name = "";
	public $sender_province = ""; //省
	public $sender_city = ""; //市
	public $sender_district = ""; //县
	public $sender_detail = ""; //详细地址
	
	//收件人信息
	public $recipient_phone = "";
	public $recipient_name = "";
	public $recipient_province = ""; //省
	public $recipient_city = ""; //市
	public $recipient_district = ""; //县
	public $recipient_detail = ""; //详细地址
	
	public $resets = "";
	
	public $error = "ok";
	public $error_code = "";
	public $error_msg = "";

	//发送打印任务(电子面单)
	public function GetSheet()
	{
		$ts = time();
		$bodys = array(
			"app_id" => $this->appid,
			"method" => $this->Method['GetSheet'],
			"sign" => md5($this->appid.$this->Method['GetSheet'].$ts.$this->appkey), 
			"ts" => $ts,
			"data" => '{
				"action":"print.json.cn", 
				"agent_id":"'.$this->agent_id.'", 
				"print_type":"2",
				"batch":"bool",
				"print_data":{
					"sequence": "1/1",
					"cp_code": "'.$this->cp_code.'",
					"tid": "'.$this->tid.'",
					"goods_name":"'.$this->goods_name.'",
					"sender": {
						"address": {
							"province": "'.$this->sender_province.'",
							"city": "'.$this->sender_city.'",
							"district": "'.$this->sender_district.'",
							"detail": "'.$this->sender_detail.'"
						},
						"mobile": "'.$this->sender_phone.'",
						"phone": "",
						"name": "'.$this->sender_name.'"
					},
					"recipient": {
						"address": {
							"province": "'.$this->recipient_province.'",
							"city": "'.$this->recipient_city.'",
							"district": "'.$this->recipient_district.'",
							"detail": "'.$this->recipient_detail.'"
						},
						"mobile": "'.$this->recipient_phone.'",
						"phone": "",
						"name": "'.$this->recipient_name.'"
					},
					"routing_info": {},
					"waybill_code": "" 
				}
			}',
		);
		$this->resets = $this->GetUrl($bodys);
	}
	
	//处理返回的数据编码
	public function GetSheetError()
	{
		$json_array = json_decode($this->resets,true);
		if($json_array['code'] == 0){
			$this->error      = "ok";
			$this->error_msg  = $json_array['data'][$this->tid]['task_info']['waybill_code'];
			$this->error_code = 0;
		}else{
			$this->error      = "error";
			$this->error_msg  = $json_array['msg'];
			$this->error_code = $json_array['code'];
		}
	}
	

	//用户发送POST的API请求
	public function GetUrl($bodys)
	{
		$headers = array();
		array_push($headers, "Content-Type".":"."application/x-www-form-urlencoded; charset=UTF-8");  //根据API的要求，定义相对应的Content-Type
		$bodys = http_build_query($bodys);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_URL, $this->hostUrl);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_FAILONERROR, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		if (1 == strpos("$".$this->hostUrl, "https://"))
		{
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		}
		curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
		return curl_exec($curl);
	}
}

?>