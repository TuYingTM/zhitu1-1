<?php
class Send{
	
	public $Send_db = array();
	public $Fa_Send_db = array();
	public $Shou_Send_db = array();
	public $error = "error";
	public $error_msg;
	public $fileurl = "";
	/**
	 * 获取所有快递
	 */
	function get_express_list(){
		
		$url = "http://lipin.cqlunwen.cn/apiv1/get_express_list.html";
		// 签名信息
		$postdata = $this->get_sign();
		// 发送请求
    	echo $this->send_post($url, $postdata);
    	/**
    	 * 示例说明：
	    	{
			    "success": true,
			    "datalist": [
			        {
			            "kdid": "55",							// 快递ID
			            "platform": "1",						// 1淘宝天猫 2拼多多 3阿里 4京东 5其他
			            "express_type": "3",					// 1普通 2电子 3菜鸟 4无界
			            "type": "2",							// 1快递单号 2寄信封单 3礼品代发
			            "name": "圆通快递",						// 快递名称
			            "text": "支持【淘宝】【天猫】【阿里】",	// 支持平台说明
			            "remarks": "",							// 备注说明
			            "state": "1"							// 1正常 2暂停 3下架
			        }
			    ]
			}
    	 */
	}
	
	/**
	 * 获取仓库列表
	 */
	function get_warehouse_list(){
		
		$url = "http://lipin.cqlunwen.cn/apiv1/get_warehouse_list.html";
		// 签名信息
		$postdata = $this->get_sign();
		// 整理数据
    	$postdata['kdid'] = "48";
		// 发送请求
    	echo $this->send_post($url, $postdata);
    	/**
    	 * 示例说明：
	    	{
			    "success": true,
			    "datalist": [
			        {
			            "warehouse": "13",	// 仓库ID
			            "kdid": "55",		// 快递ID
			            "type": "2",		// 1系统单号 2寄信封单 3礼品代发
			            "state": "1",		// 1正常 2暂停 3下架
			            "name": "广州黄埔仓",
			            "province": "广东省",
			            "city": "广州市",
			            "district": "黄埔区",
			            "address": "九佛镇九佛西路1号",
			            "zipcode": "000000"
			        }
			    ]
			}
    	 */
	}
	
	/**
	 * 获取礼品列表
	 */
	function get_gift_list(){
		$url = "http://lipin.cqlunwen.cn/apiv1/get_gift_list.html";
		// 签名信息
		$postdata = $this->get_sign();
		// 整理数据
    	$postdata['warehouse'] = "9";	// 仓库ID
		// 发送请求
    	return $this->send_post($url, $postdata);
    	/**
    	 * 示例说明：
	    	{
			    "success": true,
			    "datalist": [
			        {
			            "gift": "7",		// 礼品ID
			            "kdid": "47",		// 快递ID
			            "warehouse": "3",	// 仓库ID
			            "image": "/image/101/346-1591858023.jpg",
			            "name": "纸巾",
			            "state": "1"		// 1正常 2暂停 3下架
			        }
			    ]
			}
		 */
	}
	
	/**
	 * 通过pid获取单号
	 */
	function get_waybill_no(){
		$url = "http://lipin.cqlunwen.cn/apiv1/get_waybill_no.html";
		
		// 签名信息
		$postdata = $this->get_sign();
		
		// 整理数据
			$pid_list = array();
    		$pid_list[] = '200515-556993905670144';
    	$postdata['pid_list'] = json_encode($pid_list);
		
    	// 发送请求
    	echo $this->send_post($url, $postdata);
	}
	
	/**
	 * 删除已购买的单号
	 */
	function del_waybill_no(){
		$url = "http://lipin.cqlunwen.cn/apiv1/del_waybill_no.html";
		
		// 签名信息
    	$postdata = $this->get_sign();
    	
    	// 整理数据
    	$postdata['type'] = 2;				// 传入的ID类型（1、购买时返回的订单编号oid，2、快递单号）
    		$id_list = array();
    		$id_list[] = 'YS100099';
    		$id_list[] = 'YS100100';
    	$postdata['id_list'] = json_encode($id_list);	// 支持批量（最多支持20个单号）
    	
    	// 发送请求
    	echo $this->send_post($url, $postdata);
	}
	
	/**
	 * 购买
	 */
    public function buy_waybill_no(){
		include $this->fileurl."config.php";
    	$url = "http://lipin.cqlunwen.cn/apiv1/buy_waybill_no.html";
    	
    	// 签名信息
    	$postdata = $this->get_sign();
    	// 基本信息
		$wid = $this->Send_db['wid'];
		$pfid = $this->Send_db['pfid'];
		$gift = $this->Send_db['giftid'];
    	$postdata['type']      = 3;				// 快递类型（必传）
    	$postdata['kdid']      = $config[$wid][$pfid]['kdid'];				// 快递ID（必传）
    	$postdata['warehouse'] = $config[$wid][$pfid]['wid'];			// 仓库ID（礼品代发和寄信封件必传，其他不用传）
    	$postdata['gift']      = $config[$wid][$pfid]['gift'][$gift];				// 礼品ID（礼品代发必传，其他不用传）
    	//$postdata['goods']     = "日用品";		// 物品（可不传）
    	//$postdata['weight']    = "1.0";		// 重量（可不传）
    	//$postdata['number']    = "1";			// 包裹内包含的数量（可不传）
    	$postdata['repeat']    = 1;			// 是否允许重复下单（1禁止重复下单，0允许重复下单）
    	
    	// 发件人信息
    	$postdata['s_name'] = $this->Fa_Send_db['name'];				// 发件人 - 姓名
    	$postdata['s_mobile'] = $this->Fa_Send_db['phone'];		// 发件人 - 手机
    	$postdata['s_province'] = "浙江省";			// 发件人 - 省
    	$postdata['s_city'] = "杭州市";				// 发件人 - 市
    	$postdata['s_district'] = "西湖区";			// 发件人 - 区
    	$postdata['s_address'] = "健康路88号";		// 发件人 - 街道
    	$postdata['s_zipcode'] = "258258";			// 发件人 - 邮编
    	
    	// 收件人列表
			$receiver_list = array();
				$receiver_item = array();
				$receiver_item['pid'] = microtime();						// 唯一PID，返回单号时，通过pid获取对应的快递单号。重复的pid会显示已购买的单号，不会重复扣款，但可能会提示错误。
				$receiver_item['r_name']     = $this->Shou_Send_db['name'];						// 收件人
				$receiver_item['r_mobile']   = $this->Shou_Send_db['phone'];				// 电话号码（或手机号码）
				$receiver_item['r_province'] = $this->Shou_Send_db['province'];					// 省
				$receiver_item['r_city']     = $this->Shou_Send_db['city'];						// 市
				$receiver_item['r_district'] = $this->Shou_Send_db['district'];					// 区
				$receiver_item['r_address']  = $this->Shou_Send_db['detail'];		// 街
				$receiver_item['r_zipcode']  = "000000";					// 邮编
				$receiver_item['r_order']    = $this->Shou_Send_db['sncode'];	// 订单号（电商平台的订单号）
			$receiver_list[] = $receiver_item;
    	$postdata['receiver_list'] = json_encode($receiver_list);		// 收件人列表，json字符串格式，最高支持6000条
		$dbinfo = $this->send_post($url, $postdata);
    	$dbinfo = json_decode($dbinfo,true);
		if($dbinfo['success'] == 1){
			$this->error = "ok";
			$this->error_msg = $dbinfo['datalist'][0]['waybill_no'];
		}else{
			$this->error = "error";
			$this->error_msg = $dbinfo['message'];
		}
    	// 发送请求
    }
    
    /**
     * 签名
     */
    function get_sign(){
    	$uid = $this->Send_db['appid'];			// 在个人资料里查UID
    	$user = $this->Send_db['appuser'];	// 登录帐号
    	$api_key = $this->Send_db['appkey'];			// 问客服要
    	$time = time();
    	return array(
			"u"=>$uid,
			"m"=>$user,
			"t"=>$time,
			"sign"=>md5($api_key."8b795683fb9485430ce84ceb78cda9786d564150".$uid.$user.$time)
		);
    }
    
    /**
     * 发送post请求
     */
    function send_post($url, $data){
    	$timeout = 30;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		if(strpos($url, "https://") !== false){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_POSTFIELDS, is_array($data) ? http_build_query($data) : $data);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
		$content = curl_exec($ch);
		curl_close($ch);
		return $content;
    }
    
}